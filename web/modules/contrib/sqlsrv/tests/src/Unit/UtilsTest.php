<?php

namespace Drupal\Tests\sqlsrv\Unit;

use Drupal\sqlsrv\Driver\Database\sqlsrv\Utils;
use Drupal\Tests\UnitTestCase;

/**
 * Tests the Utils class.
 *
 * @group Database
 */
class UtilsTest extends UnitTestCase {

  /**
   * Test the findParenMatch method.
   *
   * @covers \Drupal\sqlsrv\Driver\Database\sqlsrv\Utils::findParenMatch
   * @dataProvider dataProviderForTestFindParen
   */
  public function testFindParen(string $string, int $start, $expected) {
    $actual = Utils::findParenMatch($string, $start);

    $this->assertEquals($expected, $actual);
  }

  public function dataProviderForTestFindParen() {
    return [
      ['', 0, FALSE],
      ['', 1, FALSE],
      ['a', 1, FALSE],
      ['a', -1, FALSE],
      ['ab', -1, FALSE],
      ['ab', -10, FALSE],
      ['()', 0, 1],
      ['()', 1, FALSE],
      ['(()', 0, FALSE],
      ['(()', 1, 2],
      ['Lorem(ipsum(dolor)sit(amet)consectetur)', 5, 38],
      ['Lorem(ipsum(dolor)sit(amet)consectetur)', 11, 17],
      ['Lorem(ipsum(dolor)sit(amet)consectetur)', 21, 26],
    ];
  }

}
