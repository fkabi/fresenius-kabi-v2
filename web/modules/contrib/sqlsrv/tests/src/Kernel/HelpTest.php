<?php

namespace Drupal\Tests\sqlsrv\Kernel;

use Drupal\Core\Routing\RouteMatch;
use Drupal\KernelTests\KernelTestBase;

/**
 * Tests the module help page.
 *
 * @group help
 */
class HelpTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['system', 'sqlsrv', 'user', 'path_alias'];

  /**
   * Ensures that the module's hook_help() is called.
   */
  public function testHookHelp() {
    // I'm sure this can be improved. It's a copy/paste/alter. I'm not sure what the point of $route is.
    $route = \Drupal::service('router.route_provider')->getRouteByName('<front>');
    $result = \Drupal::service('module_handler')->invoke('sqlsrv', 'help', ['help.page.sqlsrv', new RouteMatch('<front>', $route)]);
    $this->assertStringContainsString('The SQL Server module provides the connection', $result);
  }

}
