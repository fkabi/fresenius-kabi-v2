<?php

namespace Drupal\Tests\sqlsrv\Kernel;

use Drupal\KernelTests\KernelTestBase;

/**
 * Tests the module help page.
 *
 * @group help
 */
class RequirementsTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['system', 'sqlsrv', 'user'];

  /**
   * Ensures that the module's hook_requirements() is called.
   */
  public function testHookRequirements() {
    module_load_install('sqlsrv');
    \Drupal::service('module_handler')->loadInclude('sqlsrv', 'install');
    $result = \Drupal::service('module_handler')->invoke('sqlsrv', 'requirements', ['runtime']);
    $this->assertIsArray($result);
  }

}
