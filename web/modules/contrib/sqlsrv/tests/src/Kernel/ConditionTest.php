<?php

namespace Drupal\Tests\sqlsrv\Kernel;

use Drupal\sqlsrv\Driver\Database\sqlsrv\Select;

/**
 * Test the functions of the custom Condition class.
 *
 * @group Database
 */
class ConditionTest extends SqlsrvTestBase {

  // Testing of custom Condition class in select->where already happens in core.
  // Test custom Condition class in select->having.

  /**
   * Confirms that we can properly nest custom conditional clauses.
   */
  public function testNestedConditions() {
    // This query should translate to:
    // SELECT job FROM {test} WHERE name='Paul' AND (name REGEX '^P' OR age=27)
    // That should find only one record. Yes it's a non-optimal way of writing
    // that query but that's not the point!
    $query = $this->connection->select('test');
    $query->addField('test', 'job');
    $query->condition('name', 'Paul');
    $query->condition(($this->connection->condition('OR'))->condition('name', '^P', 'REGEXP')->condition('age', '27'));

    $job = $query->execute()->fetchField();
    $this->assertEquals($job, 'Songwriter', 'Correct data retrieved.');
  }

  /**
   * Ensure that the sqlsrv driver can execute queries with multiple escapes.
   *
   * Core tests already do this, but good to double check.
   */
  public function testPdoBugFix() {
    $connection = $this->connection;

    $query = new Select($connection, 'test', 't');

    // Create and execute buggy query.
    $query->addField('t', 'job');
    $query->condition('job', '%i%', 'LIKE');
    $query->condition('name', '%o%', 'LIKE');
    $result = $query->execute();

    // Asserting that no exception is thrown. Is there a better way?
    // Should actually review results.
    $this->assertTrue(TRUE);
  }

  /**
   * Test that brackets are escaped correctly.
   */
  public function testLikeWithBrackets() {
    $this->connection->insert('test_people')
      ->fields([
        'job' => '[Rutles] - Guitar',
        'name' => 'Dirk',
      ])
      ->execute();
    $name = $this->connection->select('test_people', 't')
      ->fields('t', ['name'])
      ->condition('job', '%[Rutles%', 'LIKE')
      ->execute()
      ->fetchField();
    $this->assertEquals('Dirk', $name);
    $this->connection->insert('test_people')
      ->fields([
        'job' => '[Rutles] - Drummer [Original]',
        'name' => 'Kevin',
      ])
      ->execute();
    $names = $this->connection->select('test_people', 't')
      ->fields('t', ['name', 'job'])
      ->condition('job', '%[Rutles]%', 'LIKE')
      ->execute()
      ->fetchAllAssoc('job');
    $this->assertCount(2, $names);
  }

  /**
   * Test REGEXP in WHERE clause.
   */
  public function testRegexpWhere() {
    $name = $this->connection->select('test', 't')
      ->fields('t', ['name'])
      ->where('name REGEXP :is_p', [':is_p' => '^P'])
      ->execute()
      ->fetchField();
    $this->assertEquals('Paul', $name);
  }

  /**
   * Test NOT REGEXP in WHERE clause.
   */
  public function testNotRegexpWhere() {
    $name = $this->connection->select('test', 't')
      ->fields('t', ['name'])
      ->where('name NOT REGEXP :no_o', [':no_o' => 'o'])
      ->execute()
      ->fetchField();
    $this->assertEquals('Paul', $name);
  }

}
