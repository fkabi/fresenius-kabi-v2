<?php

namespace Drupal\Tests\sqlsrv\Kernel;

use Drupal\migrate\Row;
use Drupal\migrate_drupal\Tests\StubTestTrait as CoreStubTestTrait;

/**
 * Provides common functionality for testing stubbing.
 */
trait StubTestTrait {

  use CoreStubTestTrait;

  /**
   * Create a stub of the given entity type.
   *
   * @param string $entity_type_id
   *   The entity type we are stubbing.
   *
   * @return int
   *   ID of the created entity.
   */
  protected function createEntityStub($entity_type_id) {
    // Create a dummy migration to pass to the destination plugin.
    $definition = [
      'migration_tags' => ['Stub test'],
      'source' => ['plugin' => 'empty'],
      'process' => [],
      'destination' => ['plugin' => 'entity:' . $entity_type_id],
    ];
    $migration = \Drupal::service('plugin.manager.migration')->createStubMigration($definition);
    $destination_plugin = $migration->getDestinationPlugin(TRUE);
    $stub_row = new Row([], [], TRUE);
    $feed_values = [
      'uuid' => '13064741-350c-4271-a3c1-991431be0e63',
      'langcode' => 'en',
      'title' => 'trushesticrasehicrelifreuivodesucligubrabusitodroduwadutheswicluwosaswiprestispe',
      'url' => str_repeat('a', 1701),
      'refresh' => 3600,
      'checked' => 0,
      'queued' => 0,
    ];
    if ($entity_type_id == 'aggregator_feed') {
      foreach ($feed_values as $key => $value) {
        $stub_row->setDestinationProperty($key, $value);
      }
    }
    elseif ($entity_type_id == 'aggregator_item') {
      $entity_storage = \Drupal::entityTypeManager()->getStorage('aggregator_feed');
      $entity = $entity_storage->createWithSampleValues(NULL, ['in_preview' => TRUE]);
      $entity->setUrl($feed_values['url']);
      $stub_row->setDestinationProperty('fid', $entity);
    }
    $destination_ids = $destination_plugin->import($stub_row);
    return reset($destination_ids);
  }

}
