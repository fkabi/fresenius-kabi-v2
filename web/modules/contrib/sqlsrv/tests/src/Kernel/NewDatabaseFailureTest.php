<?php

namespace Drupal\Tests\sqlsrv\Kernel;

use Drupal\Core\Database\Database;

class NewDatabaseFailureTest extends SqlsrvTestBase {

  /**
   * {@inheritdoc}
   */
  protected function tearDown() {
    Database::renameConnection('sqlsrv_original_default', 'default');
    parent::tearDown();
  }

  /**
   * Test that an incorrect password reurns $errors.
   *
   * @covers \Drupal\sqlsrv\Driver\Database\sqlsrv\Install\Tasks::connect
   */
  public function testBadPassword() {
    Database::renameConnection('default', 'sqlsrv_original_default');
    $db_url = 'sqlsrv://sa:Password12@localhost/newdrupalsite?schema=dbo&cache_schema=true&module=sqlsrv';
    $database = Database::convertDbUrlToConnectionInfo($db_url, $this->root);
    Database::addConnectionInfo('default', 'default', $database);
    $driver = 'sqlsrv';
    $namespace = 'Drupal\\sqlsrv\\Driver\\Database\\sqlsrv';

    $errors = db_installer_object($driver, $namespace)->runTasks();
    $this->assertNotEmpty($errors);
  }

  /**
   * Test that an unpriviledged account reurns $errors.
   *
   * @covers \Drupal\sqlsrv\Driver\Database\sqlsrv\Install\Tasks::connect
   */
  public function testBadPermissions() {
    $sql = "CREATE LOGIN nonadmin WITH PASSWORD = 'Password12!'";
    $this->connection->queryDirect($sql);
    Database::renameConnection('default', 'sqlsrv_original_default');
    $db_url = 'sqlsrv://nonadmin:Password12!@localhost/newdrupalsite?schema=dbo&cache_schema=true&module=sqlsrv';
    $database = Database::convertDbUrlToConnectionInfo($db_url, $this->root);
    Database::addConnectionInfo('default', 'default', $database);
    $driver = 'sqlsrv';
    $namespace = 'Drupal\\sqlsrv\\Driver\\Database\\sqlsrv';
    $errors = db_installer_object($driver, $namespace)->runTasks();
    $this->assertNotEmpty($errors);
  }

}
