<?php

namespace Drupal\page_content_rest_api\Plugin\rest\resource;

use Drupal\Core\Menu\MenuTreeParameters;
use Drupal\user\Entity\User;
use Drupal\node\Entity\Node;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\file\Entity\File;
use Drupal\rest\ModifiedResourceResponse;
use Drupal\rest\Plugin\ResourceBase;
use Symfony\Component\HttpFoundation\Request;
use Drupal\vimeo_module\Controller\VimeoController;
use Drupal\page_content_rest_api\Helper\ImageHelper;

/**
 * Provides Resource for the Page Content functionality.
 *
 * @RestResource(
 *   id = "page_content_get",
 *   label = @Translation("PAGE_CONTENT_GET"),
 *   uri_paths = {
 *     "canonical" = "/page-content-rest-api/get-page-content"
 *   }
 * )
 */
class PageContentGet extends ResourceBase {
  const HTTP_OK = 200;
  const HTTP_UNPROC = 422;

  /**
   * Responds to entity GET requests.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *
   * @return \Drupal\rest\ModifiedResourceResponse
   */
  public function get(Request $request) {
    $page = $request->query->get('page');

    if (!isset($page) || $page === NULL) {
      $response = [
        'success' => FALSE,
        'status_code' => self::HTTP_UNPROC,
        'message' => $this->t("INTERNAL_ERROR"),
        'data' => [
          'error_title' => $this->t('PAGE_DOES_NOT_EXIST'),
        ],
      ];

      return new ModifiedResourceResponse($response);
    }

    try {
      $user = \Drupal::currentUser();
      $userLangId = $user->getPreferredLangcode();

      switch ($page) {
        case 'login':
          $term = $this->getData($page);
          $menu = $this->getMenu();

          $content = [
            'menu' => $menu,
            'breadcrumbs' => $this->getBreadcrumbs($menu, ($term->field_login_section_title->value ? $term->field_login_section_title->value : $this->t('LOG_IN')), $page),
            'login_page_login_section_title' => ($term->field_login_section_title->value ? $term->field_login_section_title->value : $this->t('LOG_IN')),
            'login_page_login_section_patient_restricted_message' => ($term->field_login_sec_patient_rstr_msg->value ? $term->field_login_sec_patient_rstr_msg->value : $this->t('ACCESS_TO_THIS_CONTENT_IS_RESTRICTED')),
            'login_page_login_section_hcp_restricted_message' => ($term->field_login_sec_hcp_rstr_msg->value ? $term->field_login_sec_hcp_rstr_msg->value : $this->t('ACCESS_TO_THIS_CONTENT_IS_RESTRICTED')),
            'login_page_login_section_email_placeholder' => ($term->field_login_section_email->value ? $term->field_login_section_email->value : $this->t('EMAIL')),
            'login_page_login_section_password_placeholder' => ($term->field_login_section_password->value ? $term->field_login_section_password->value : $this->t('PASSWORD')),
            'login_page_login_section_forgot_email_label' => ($term->field_login_section_forgot_email->value ? $term->field_login_section_forgot_email->value : $this->t('FORGOT_EMAIL')),
            'login_page_login_section_forgot_password_label' => ($term->field_login_section_forgot_pass->value ? $term->field_login_section_forgot_pass->value : $this->t('FORGOT_PASSWORD')),
            'login_page_login_section_submit_button_label' => ($term->field_login_section_sbmt_btn_lab->value ? $term->field_login_section_sbmt_btn_lab->value : $this->t('LOG_IN')),
            'login_page_register_section_title' => ($term->field_login_reg_sec_title->value ? $term->field_login_reg_sec_title->value : $this->t('REGISTER_NOW_FOR_ADDITIONAL_BENEFITS')),
            'login_page_register_section_content' => ($term->field_login_reg_sec_content->value ? $term->field_login_reg_sec_content->value : ''),
            'login_page_register_section_patient_register_button_label' => ($term->field_login_reg_sec_patient_reg->value ? $term->field_login_reg_sec_patient_reg->value : $this->t('REGISTER_AS_A_PATIENT')),
            'login_page_register_section_hcp_register_button_label' => ($term->field_login_reg_sec_hcp_reg->value ? $term->field_login_reg_sec_hcp_reg->value : $this->t('REGISTER_AS_AN_HCP')),
            'login_page_register_section_description' => ($term->field_login_reg_sec_desc->value ? $term->field_login_reg_sec_desc->value : $this->t('YOU_WILL_NEED_THE_UNIQUE_CODE_PROVIDED_ON_THE_ACCESS_CARD_IN_YOUR_STARTED_PACK')),
            'login_page_patient_restricted_register_section_content' => ($term->field_login_reg_patient_rstr_con->value ? $term->field_login_reg_patient_rstr_con->value : ''),
            'login_page_hcp_restricted_register_section_content' => ($term->field_login_reg_hcp_rstr_con->value ? $term->field_login_reg_hcp_rstr_con->value : ''),
            'footer' => $this->getFooter(),
          ];
          break;

        case 'register-patient':
          $term = $this->getData($page);
          $menu = $this->getMenu();

          $content = [
            'menu' => $menu,
            'breadcrumbs' => $this->getBreadcrumbs($menu, ($term->field_register_patient_title->value ? $term->field_register_patient_title->value : $this->t('REGISTER')), $page),
            'register_patient_page_title' => ($term->field_register_patient_title->value ? $term->field_register_patient_title->value : $this->t('REGISTER')),
            'register_patient_page_email' => ($term->field_register_patient_email->value ? $term->field_register_patient_email->value : $this->t('EMAIL')),
            'register_patient_page_condition' => ($term->field_register_patient_condition->value ? $term->field_register_patient_condition->value : $this->t('DIAGNOSED_CONDITION')),
            'register_patient_page_access_code' => ($term->field_register_patient_acc_code->value ? $term->field_register_patient_acc_code->value : $this->t('ACCESS_CODE')),
            'register_patient_page_password' => ($term->field_register_patient_password->value ? $term->field_register_patient_password->value : $this->t('PASSWORD')),
            'register_patient_page_confirm_password' => ($term->field_register_patient_conf_pass->value ? $term->field_register_patient_conf_pass->value : $this->t('CONFIRM_PASSWORD')),
            'register_patient_page_checkbox_label' => ($term->field_register_patient_checkbox->value ? $term->field_register_patient_checkbox->value : $this->t('AGREE_TO_RECEIVE_EMAILS')),
            'register_patient_page_submit_button_label' => ($term->field_register_patient_submit_bt->value ? $term->field_register_patient_submit_bt->value : $this->t('REGISTER')),
            'footer' => $this->getFooter(),
          ];
          break;

        case 'register-hcp':
          $term = $this->getData($page);

          $titles = [];
          $field_allowed_values = FieldStorageConfig::loadByName('user', 'field_user_title')->getSettings()['allowed_values'];

          foreach ($field_allowed_values as $label => $value) {
            $titles[$value] = [
              'value' => $value,
              'label' => $label,
            ];
          }

          $menu = $this->getMenu();

          $content = [
            'menu' => $menu,
            'breadcrumbs' => $this->getBreadcrumbs($menu, ($term->field_register_hcp_title->value ? $term->field_register_hcp_title->value : $this->t('REGISTER')), $page),
            'register_hcp_page_select_field_titles' => $titles,
            'register_hcp_page_title' => ($term->field_register_hcp_title->value ? $term->field_register_hcp_title->value : $this->t('REGISTER')),
            'register_hcp_page_user_title' => ($term->field_register_hcp_user_title->value ? $term->field_register_hcp_user_title->value : $this->t('TITLE')),
            'register_hcp_page_first_name' => ($term->field_register_hcp_fname->value ? $term->field_register_hcp_fname->value : $this->t('FIRST_NAME')),
            'register_hcp_page_last_name' => ($term->field_register_hcp_lname->value ? $term->field_register_hcp_lname->value : $this->t('LAST_NAME')),
            'register_hcp_page_country' => ($term->field_register_hcp_country->value ? $term->field_register_hcp_country->value : $this->t('COUNTRY')),
            'register_hcp_page_healthcare_organization' => ($term->field_register_hcp_org->value ? $term->field_register_hcp_org->value : $this->t('HEALTHCARE_ORGANIZATION')),
            'register_hcp_page_email' => ($term->field_register_hcp_email->value ? $term->field_register_hcp_email->value : $this->t('EMAIL')),
            'register_hcp_page_password' => ($term->field_register_hcp_password->value ? $term->field_register_hcp_password->value : $this->t('PASSWORD')),
            'register_hcp_page_confirm_password' => ($term->field_register_hcp_conf_pass->value ? $term->field_register_hcp_conf_pass->value : $this->t('CONFIRM_PASSWORD')),
            'register_hcp_page_registration_number_label' => ($term->field_register_hcp_reg_num->value ? $term->field_register_hcp_reg_num->value : $this->t('REGISTRATION_NUMBER')),
            'register_hcp_page_checkbox_label' => ($term->field_register_hcp_checkbox->value ? $term->field_register_hcp_checkbox->value : $this->t('I_HAVE_READ_THE_STATEMENT')),
            'register_hcp_page_submit_button_label' => ($term->field_register_hcp_submit_bt->value ? $term->field_register_hcp_submit_bt->value : $this->t('REGISTER')),
            'footer' => $this->getFooter(),
          ];
          break;

        case 'homepage':
        case 'main-page':
          $term = $this->getData($page);

          $carousel_items = NULL;
          if ($term->field_homepage_carousel_ref->target_id) {
            $homepage_carousel = Node::load($term->field_homepage_carousel_ref->target_id);

            $homepage_carousel_items = $homepage_carousel->field_homepage_carousel_items;

            if ($homepage_carousel_items) {
              foreach ($homepage_carousel_items as $item) {
                $entityReference = $item->get('entity');
                $entityAdapter = $entityReference->getTarget();
                $referencedEntity = $entityAdapter->getValue();

                $field_link = $referencedEntity->field_hp_carousel_item_link->first();
                $url = '';
                $image = NULL;
                $image_small = NULL;
                $hero_image = NULL;

                if ($field_link) {
                  $url = $field_link->getUrl()->toString();
                }

                if ($referencedEntity->field_homepage_carousel_item_img->target_id) {
                  $image = ImageHelper::exportImagePathData($referencedEntity, 'field_homepage_carousel_item_img');
                }

                if ($referencedEntity->field_hp_carousel_item_img_small->target_id) {
                  $image_small = ImageHelper::exportImagePathData($referencedEntity, 'field_hp_carousel_item_img_small');
                }
                if($referencedEntity->field_hero_image->target_id) {
                  $hero_image = ImageHelper::exportImagePathData($referencedEntity, 'field_hero_image');
                }

                $carousel_items[$referencedEntity->id()] = [
                  'id' => $referencedEntity->id(),
                  'link' => $url,
                  'image' => $image,
                  'image_small' => $image_small,
                  'body' => $referencedEntity->body->value,
                  'title' => $referencedEntity->title->value,
                  'hero_image' => $hero_image,
                ];
              }
            }
          }

          $footer = $this->getFooter();
          $footer["footer_iso_number"] = $term->field_hp_page_iso->value;

          $content = [
            'menu' => $this->getMenu(),
            'homepage_title' => ($term->field_hp_title->value ? $term->field_hp_title->value : $this->t('MAIN_PAGE_TITLE')),
            'carousel' => $carousel_items,
            'footer' => $footer,
          ];
          break;

        case 'restricted-popup':
          $term = $this->getData($page);

          $content = [
            'restricted_popup_title' => ($term->field_rsrt_popup_title->value ? $term->field_rsrt_popup_title->value : $this->t('ACCESS_ALL_AREAS')),
            'restricted_popup_content' => ($term->field_rstr_popup_content->value ? $term->field_rstr_popup_content->value : ''),
            'restricted_popup_cancel_cta_label' => ($term->field_rsrt_popup_cancel_label->value ? $term->field_rsrt_popup_cancel_label->value : $this->t('NOT_NOW')),
            'restricted_popup_submit_cta_label' => ($term->field_rsrt_popup_submit_label->value ? $term->field_rsrt_popup_submit_label->value : $this->t('LOGIN_SLASH_REGISTER')),
          ];
          break;

        case 'getting-started':
          $cache = \Drupal::cache();
          $cached_result = $cache->get('getting-started-page');
          if ($cached_result) {
            $content = $cached_result->data;
            break;
          }

          $term = $this->getData($page);

          $carousel_items = NULL;
          if ($term->field_getting_start_carousel_ref->target_id) {
            $homepage_carousel = Node::load($term->field_getting_start_carousel_ref->target_id);

            $homepage_carousel_items = $homepage_carousel->field_homepage_carousel_items;

            if ($homepage_carousel_items) {
              foreach ($homepage_carousel_items as $key => $item) {
                $entityReference = $item->get('entity');
                $entityAdapter = $entityReference->getTarget();
                $referencedEntity = $entityAdapter->getValue();

                $field_link = $referencedEntity->field_hp_carousel_item_link->first();
                $url = '';
                $image = NULL;
                $image_small = NULL;

                if ($field_link) {
                  $url = $field_link->getUrl()->toString();
                }

                if ($referencedEntity->field_homepage_carousel_item_img->target_id) {
                  $image = ImageHelper::exportImagePathData($referencedEntity, 'field_homepage_carousel_item_img');
                }

                if ($referencedEntity->field_hp_carousel_item_img_small->target_id) {
                  $image_small = ImageHelper::exportImagePathData($referencedEntity, 'field_hp_carousel_item_img_small');
                }

                $carousel_items[$key] = [
                  'id' => $referencedEntity->id(),
                  'link' => $url,
                  'image' => $image,
                  'image_small' => $image_small,
                  'body' => $referencedEntity->body->value,
                  'title' => $referencedEntity->title->value,
                ];
              }
            }
          }

          $carousel_items = array_slice($carousel_items, 0, 3);

          $menu = $this->getMenu();

          $roles = $this->getMenuRoles($menu, $page);

          $footer_param = FALSE;

          if ($roles !== NULL) {
            $footer_param = $this->shouldDisplayFooterSituational($roles);
          }

          $footer = $this->getFooter($footer_param);
          $footer["footer_iso_number"] = $term->field_getting_started_page_iso->value;

          $content = [
            'menu' => $menu,
            'breadcrumbs' => $this->getBreadcrumbs($menu, ($term->field_getting_started_title->value ? $term->field_getting_started_title->value : $this->t('GETTING_STARTED_PAGE_TITLE')), $page),
            'getting_started_page_title' => ($term->field_getting_started_title->value ? $term->field_getting_started_title->value : $this->t('GETTING_STARTED_PAGE_TITLE')),
            'getting_started_page_description' => ($term->field_getting_started_desc->value ? $term->field_getting_started_desc->value : $this->t('GETTING_STARTED_PAGE_DESCRIPTION')),
            'carousel' => $carousel_items,
            'footer' => $footer,
          ];

          $expire = date('Y-m-d H:i:s', strtotime(date('Y-m-d H:i:s', time()) . ' + 1 days'));
          $cache->set('getting-started-page', $content, strtotime($expire));

          break;

        case 'profile':
          $term = $this->getData($page);
          $menu = $this->getMenu();

          $footer = $this->getFooter(TRUE);
          $footer["footer_iso_number"] = $term->field_profile_page_iso->value;

          $content = [
            'menu' => $menu,
            'breadcrumbs' => $this->getBreadcrumbs($menu, ($term->field_profile_page_title->value ? $term->field_profile_page_title->value : $this->t('PROFILE_PAGE_TITLE')), $page),
            'profile_page_title' => ($term->field_profile_page_title->value ? $term->field_profile_page_title->value : $this->t('PROFILE_PAGE_TITLE')),
            'profile_page_email_label' => ($term->field_profile_page_email_label->value ? $term->field_profile_page_email_label->value : $this->t('EMAIL')),
            'profile_page_condition_label' => ($term->field_profile_page_cond_label->value ? $term->field_profile_page_cond_label->value : $this->t('DIAGNOSED_CONDITION')),
            'profile_page_checkbox_label' => ($term->field_profile_page_checkbox_labe->value ? $term->field_profile_page_checkbox_labe->value : ''),
            'profile_page_save_button_label' => ($term->field_profile_page_save_btn_labe->value ? $term->field_profile_page_save_btn_labe->value : $this->t('SAVE_SETTINGS')),
            'profile_page_logout_button_label' => ($term->field_profile_page_logout_btn_la->value ? $term->field_profile_page_logout_btn_la->value : $this->t('LOGOUT')),
            'footer' => $footer,
          ];

          $user = \Drupal::currentUser();
          if ($user) {
            $uid = $user->id();
            if ($uid) {
              $account = User::load($uid);
              if ($account) {
                $condition_id = [];
                $condition = '';

                $mail = $account->mail[0]->value;
                $email_sending = $account->field_user_email_sending[0]->value;

                foreach ($account->get('field_diagnosed_condition_ref') as $condition) {
                  if (method_exists($condition, 'getvalue')) {
                    $condition_id[] = $condition->getValue()['target_id'];
                  }
                }

                if (!empty($condition_id)) {
                  $condition = $condition_id[0];
                }

                if ($condition !== '') {
                  $nodes = \Drupal::entityTypeManager()
                    ->getListBuilder('node')
                    ->getStorage()
                    ->loadByProperties([
                      'type' => 'diagnosed_condition',
                      'nid' => $condition_id,
                    ]);

                  $condition = [
                    'id' => $condition,
                    'title' => $nodes[array_key_first($nodes)]->getTitle(),
                  ];
                }

                $roles = [];

                foreach ($account->getRoles() as $role) {
                  if ($role === 'patient' || $role === 'hcp' || $role === 'general_admin') {
                    $roles[] = $role;
                  }
                }

                $content['user'] = [
                  'email' => $mail,
                  'email_sending' => $email_sending,
                  'condition' => $condition,
                  'roles' => $roles,
                ];
              }
            }
          }

          break;

        case 'nutrition':
          $cache = \Drupal::cache();
          $cached_result = $cache->get('nutrition-page');
          if ($cached_result) {
            $content = $cached_result->data;
            break;
          }

          $term = $this->getData($page);

          $carousel_items = NULL;
          if ($term->field_nutrition_carousel_ref->target_id) {
            $homepage_carousel = Node::load($term->field_nutrition_carousel_ref->target_id);

            $homepage_carousel_items = $homepage_carousel->field_homepage_carousel_items;

            if ($homepage_carousel_items) {
              foreach ($homepage_carousel_items as $key => $item) {
                $entityReference = $item->get('entity');
                $entityAdapter = $entityReference->getTarget();
                $referencedEntity = $entityAdapter->getValue();

                $field_link = $referencedEntity->field_hp_carousel_item_link->first();
                $url = '';
                $image = NULL;
                $image_small = NULL;

                if ($field_link) {
                  $url = $field_link->getUrl()->toString();
                }

                if ($referencedEntity->field_homepage_carousel_item_img->target_id) {
                  $image = ImageHelper::exportImagePathData($referencedEntity, 'field_homepage_carousel_item_img');
                }

                if ($referencedEntity->field_hp_carousel_item_img_small->target_id) {
                  $image_small = ImageHelper::exportImagePathData($referencedEntity, 'field_hp_carousel_item_img_small');
                }

                $carousel_items[$key] = [
                  'id' => $referencedEntity->id(),
                  'link' => $url,
                  'image' => $image,
                  'image_small' => $image_small,
                  'body' => $referencedEntity->body->value,
                  'title' => $referencedEntity->title->value,
                ];
              }
            }
          }

          $menu = $this->getMenu();

          $roles = $this->getMenuRoles($menu, $page);

          $footer_param = FALSE;

          if ($roles !== NULL) {
            $footer_param = $this->shouldDisplayFooterSituational($roles);
          }

          $footer = $this->getFooter($footer_param);
          $footer["footer_iso_number"] = $term->field_nutrition_page_iso->value;

          $content = [
            'menu' => $menu,
            'breadcrumbs' => $this->getBreadcrumbs($menu, ($term->field_nutrition_title->value ? $term->field_nutrition_title->value : $this->t('NUTRITION_PAGE_TITLE')), $page),
            'nutrition_page_title' => ($term->field_nutrition_title->value ? $term->field_nutrition_title->value : $this->t('NUTRITION_PAGE_TITLE')),
            'nutrition_page_description' => ($term->field_nutrition_desc->value ? $term->field_nutrition_desc->value : $this->t('NUTRITION_PAGE_DESCRIPTION')),
            'carousel' => $carousel_items,
            'footer' => $footer,
          ];

          $expire = date('Y-m-d H:i:s', strtotime(date('Y-m-d H:i:s', time()) . ' + 1 days'));
          $cache->set('nutrition-page', $content, strtotime($expire));

          break;

        case 'policies':
          $term = $this->getData($page);

          $footer = $this->getFooter();
          $footer["footer_iso_number"] = $term->field_pols_priv_page_iso->value;

          $content = [
            'menu' => $this->getMenu(),
            'breadcrumbs' => [
              [
                'label' => $this->t('HOME'),
                'url' => '/',
              ],
              [
                'label' => ($term->field_pols_priv_title->value ? $term->field_pols_priv_title->value : $this->t('PRIVACY_POLICY')),
                'url' => NULL,
              ],
            ],
            'privacy_policy' => [
              'title' => ($term->field_pols_priv_title->value ? $term->field_pols_priv_title->value : $this->t('PRIVACY_POLICY')),
              'content' => ($term->field_pols_priv_cont->value ? $term->field_pols_priv_cont->value : ''),
              'footer' => [
                'footer_iso_number' => $term->field_pols_priv_page_iso->value,
              ],
              'footer_iso_number' => $term->field_pols_priv_page_iso->value,
            ],
            'cache_policy' => [
              'content' => ($term->field_pols_cach_cont->value ? $term->field_pols_cach_cont->value : ''),
              'cancel_button' => ($term->field_pols_cach_cb->value ? $term->field_pols_cach_cb->value : $this->t('CANCEL')),
              'submit_button' => ($term->field_pols_cach_sb->value ? $term->field_pols_cach_sb->value : $this->t('SUBMIT')),
            ],
            'footer' => $footer,
          ];
          break;

        case 'eating-well':
          $term = $this->getData($page);

          $carousel_items = NULL;
          if ($term->field_eating_well_carousel_ref->target_id) {
            $homepage_carousel = Node::load($term->field_eating_well_carousel_ref->target_id);

            $homepage_carousel_items = $homepage_carousel->field_homepage_carousel_items;

            if ($homepage_carousel_items) {
              foreach ($homepage_carousel_items as $key => $item) {
                $entityReference = $item->get('entity');
                $entityAdapter = $entityReference->getTarget();
                $referencedEntity = $entityAdapter->getValue();

                $field_link = $referencedEntity->field_hp_carousel_item_link->first();
                $url = '';
                $image = NULL;
                $image_small = NULL;

                if ($field_link) {
                  $url = $field_link->getUrl()->toString();
                }

                if ($referencedEntity->field_homepage_carousel_item_img->target_id) {
                  $image = ImageHelper::exportImagePathData($referencedEntity, 'field_homepage_carousel_item_img');
                }

                if ($referencedEntity->field_hp_carousel_item_img_small->target_id) {
                  $image_small = ImageHelper::exportImagePathData($referencedEntity, 'field_hp_carousel_item_img_small');
                }

                $carousel_items[$key] = [
                  'id' => $referencedEntity->id(),
                  'link' => $url,
                  'image' => $image,
                  'image_small' => $image_small,
                  'body' => $referencedEntity->body->value,
                  'title' => $referencedEntity->title->value,
                ];
              }
            }
          }

          $carousel_items = array_slice($carousel_items, 0, 3);

          $gallery_items = NULL;
          if ($term->field_ew_symptom_gallery_ref->target_id) {
            $symptom_gallery = Node::load($term->field_ew_symptom_gallery_ref->target_id);

            $symptom_gallery_items = $symptom_gallery->field_symptom_gallery_items;

            if ($symptom_gallery_items) {
              foreach ($symptom_gallery_items as $key => $item) {
                $entityReference = $item->get('entity');
                $entityAdapter = $entityReference->getTarget();
                $referencedEntity = $entityAdapter->getValue();

                $image = NULL;

                if ($referencedEntity->field_homepage_carousel_item_img->target_id) {
                  $image = ImageHelper::exportImagePathData($referencedEntity, 'field_symptom_gallery_item_image');
                }

                $gallery_items[$key] = [
                  'id' => $referencedEntity->id(),
                  'image' => $image,
                  'title' => $referencedEntity->title->value,
                ];
              }
            }
          }

          $menu = $this->getMenu();

          $roles = $this->getMenuRoles($menu, $page);

          $footer_param = FALSE;

          if ($roles !== NULL) {
            $footer_param = $this->shouldDisplayFooterSituational($roles);
          }

          $current_card_image_id = $term->field_eating_well_card_image[0]->target_id;
          $current_card_image = File::load($current_card_image_id);
          $uri = $current_card_image->getFileUri();
          $stream_wrapper_manager = \Drupal::service('stream_wrapper_manager')->getViaUri($uri);
          $file_path = $stream_wrapper_manager->realpath();
          $ext_path = $stream_wrapper_manager->getExternalUrl();

          if (file_exists($file_path) && strpos($file_path, '/web/web/') && strpos($ext_path, '/web/web/') === FALSE) {
            $ext_path = str_replace('/web/', '/web/web/', $ext_path);
          }

          $vimeo_client = new VimeoController();
          $video_id = $term->field_ew_video->value;
          $video = $vimeo_client->getVideo($video_id);

          $footer = $this->getFooter($footer_param);
          $footer["footer_iso_number"] = $term->field_ew_page_iso->value;

          $content = [
            'menu' => $menu,
            'breadcrumbs' => $this->getBreadcrumbs($menu, ($term->field_eating_well_page_title->value ? $term->field_eating_well_page_title->value : $this->t('EATING_WELL_PAGE_TITLE')), $page),
            'eating_well_page_title' => ($term->field_eating_well_page_title->value ? $term->field_eating_well_page_title->value : $this->t('EATING_WELL_PAGE_TITLE')),
            'eating_well_page_section_1' => [
              'string_1' => $term->field_ew_sec1_str1->value,
              'string_2' => $term->field_ew_sec1_str2->value,
              'desc' => $term->field_ew_sec1_desc->processed,
            ],
            'eating_well_page_section_2' => [
              'string_1' => $term->field_ew_sec2_str1->value,
              'string_2' => $term->field_ew_sec2_str2->value,
              'desc' => $term->field_ew_sec2_desc->processed,
            ],
            'eating_well_page_section_3' => [
              'string_1' => $term->field_ew_sec3_str1->value,
              'string_2' => $term->field_ew_sec3_str2->value,
              'desc' => $term->field_ew_sec3_desc->processed,
            ],
            'carousel' => $carousel_items,
            'symptom_gallery' => $gallery_items,
            'card' => [
              'title' => $term->field_eating_well_card_title->value,
              'description' => $term->field_eating_well_card_desc->processed,
              'img_url' => $ext_path,
            ],
            'cta' => [
              'title' => $term->field_ew_cta_title->value,
              'description' => $term->field_ew_cta_desc->processed,
              'button_label' => $term->field_ew_cta_button_label->value,
              'button_url' => $term->field_ew_cta_button_url->value,
            ],
            'references' => $term->field_eating_well_references->processed,
            'video' => $video,
            'footer' => $footer,
          ];

          break;

        case 'your-condition':
          $term = $this->getData($page);

          $menu = $this->getMenu();

          $roles = $this->getMenuRoles($menu, $page);

          $footer_param = FALSE;

          if ($roles !== NULL) {
            $footer_param = $this->shouldDisplayFooterSituational($roles);
          }

          $result = \Drupal::entityQuery('node')
            ->condition('type', 'diagnosed_condition')
            ->execute();

          $conditions = [];
          foreach ($result as $id) {
            $condition = Node::load($id);
            $conditions[$id] = $condition->getTitle();
          }

          $content = [
            'menu' => $menu,
            'breadcrumbs' => $this->getBreadcrumbs($menu, ($term->field_ur_cond_page_title->value ? $term->field_ur_cond_page_title->value : $this->t('YOUR_CONDITION_PAGE_TITLE')), $page),
            'your_condition_page_title' => ($term->field_ur_cond_page_title->value ? $term->field_ur_cond_page_title->value : $this->t('YOUR_CONDITION_PAGE_TITLE')),
            'your_condition_select_items' => $conditions,
            'your_condition_default_select_option' => $term->field_ur_cond_page_default_cond->value,
            'footer' => $this->getFooter($footer_param),
          ];

          $param = $request->query->get('param');

          if ($param) {
            $user = \Drupal::currentUser();
            $user_id = $user->id();

            switch ($param) {
              case 'default-condition':
                $default_condition_id = $this->getDefaultConditionId($user_id);
                $default_condition_data = $this->getDefaultConditionData($default_condition_id);

                $vimeo_client = new VimeoController();
                $video_id = $default_condition_data->field_idb_dis_video->value;
                $video = $vimeo_client->getVideo($video_id);

                $gallery_items = NULL;
                if ($default_condition_data->field_ibd_dis_symptom_galler_ref->target_id) {
                  $symptom_gallery = Node::load($default_condition_data->field_ibd_dis_symptom_galler_ref->target_id);

                  $symptom_gallery_items = $symptom_gallery->field_symptom_gallery_items;

                  if ($symptom_gallery_items) {
                    foreach ($symptom_gallery_items as $key => $item) {
                      $entityReference = $item->get('entity');
                      $entityAdapter = $entityReference->getTarget();
                      $referencedEntity = $entityAdapter->getValue();

                      $image = NULL;

                      if ($referencedEntity->field_symptom_gallery_item_image->target_id) {
                        $image = ImageHelper::exportImagePathData($referencedEntity, 'field_symptom_gallery_item_image');
                      }

                      $gallery_items[$key] = [
                        'id' => $referencedEntity->id(),
                        'image' => $image,
                        'title' => $referencedEntity->title->value,
                      ];
                    }
                  }
                }

                if ($default_condition_data !== NULL) {
                  $content['conditions'][] = [
                    'condition' => [
                      'id' => $default_condition_id,
                      'name' => $this->getConditionNameById($default_condition_id),
                    ],
                    'title' => $default_condition_data->field_ibd_dis_title->value,
                    'section_article_string_1' => $default_condition_data->field_ibd_dis_section_str1->value,
                    'section_article_string_2' => $default_condition_data->field_ibd_dis_section_str2->value,
                    'section_article_description' => $default_condition_data->field_ibd_dis_section_desc->value,
                    'cta' => [
                      'title' => $default_condition_data->field_ibd_dis_cta_title->value,
                      'description' => $default_condition_data->field_ibd_dis_cta_desc->value,
                      'button_label' => $default_condition_data->field_ibd_dis_cta_button_label->value,
                      'button_url' => $default_condition_data->field_ibd_dis_cta_button_url->value,
                    ],
                    'video' => $video,
                    'symptom_gallery' => $gallery_items,
                    'footer' => [
                      'footer_iso_number' => $default_condition_data->field_ibd_dis_page_iso->value,
                    ],
                    'footer_iso_number' => $default_condition_data->field_ibd_dis_page_iso->value,
                  ];
                }
                break;

              case 'all-except-default':
                $default_condition_id = $this->getDefaultConditionId($user_id);
                $terms = $this->getAllConditionDataExceptOne($default_condition_id);

                foreach ($terms as $term) {
                  $vimeo_client = new VimeoController();
                  $video_id = $term->field_idb_dis_video->value;
                  $video = $vimeo_client->getVideo($video_id);

                  $gallery_items = NULL;
                  if ($term->field_ibd_dis_symptom_galler_ref->target_id) {
                    $symptom_gallery = Node::load($term->field_ibd_dis_symptom_galler_ref->target_id);

                    $symptom_gallery_items = $symptom_gallery->field_symptom_gallery_items;

                    if ($symptom_gallery_items) {
                      foreach ($symptom_gallery_items as $key => $item) {
                        $entityReference = $item->get('entity');
                        $entityAdapter = $entityReference->getTarget();
                        $referencedEntity = $entityAdapter->getValue();

                        $image = NULL;

                        if ($referencedEntity->field_symptom_gallery_item_image->target_id) {
                          $image = ImageHelper::exportImagePathData($referencedEntity, 'field_symptom_gallery_item_image');
                        }

                        $gallery_items[$key] = [
                          'id' => $referencedEntity->id(),
                          'image' => $image,
                          'title' => $referencedEntity->title->value,
                        ];
                      }
                    }
                  }

                  if ($term !== NULL) {
                    $content['conditions'][] = [
                      'condition' => [
                        'id' => $default_condition_id,
                        'name' => $this->getConditionNameById($default_condition_id),
                      ],
                      'title' => $term->field_ibd_dis_title->value,
                      'section_article_string_1' => $term->field_ibd_dis_section_str1->value,
                      'section_article_string_2' => $term->field_ibd_dis_section_str2->value,
                      'section_article_description' => $term->field_ibd_dis_section_desc->value,
                      'cta' => [
                        'title' => $term->field_ibd_dis_cta_title->value,
                        'description' => $term->field_ibd_dis_cta_desc->value,
                        'button_label' => $term->field_ibd_dis_cta_button_label->value,
                        'button_url' => $term->field_ibd_dis_cta_button_url->value,
                      ],
                      'video' => $video,
                      'symptom_gallery' => $gallery_items,
                      'footer' => [
                        'footer_iso_number' => $term->field_ibd_dis_page_iso->value,
                      ],
                      'footer_iso_number' => $term->field_ibd_dis_page_iso->value,
                    ];
                  }
                }
                break;

              case 'all':
                $terms = $this->getAllConditionDataExceptOne(NULL);

                foreach ($terms as $term) {
                  $vimeo_client = new VimeoController();
                  $video_id = $term->field_idb_dis_video->value;
                  $video = $vimeo_client->getVideo($video_id);

                  $gallery_items = NULL;
                  if ($term->field_ibd_dis_symptom_galler_ref->target_id) {
                    $symptom_gallery = Node::load($term->field_ibd_dis_symptom_galler_ref->target_id);

                    $symptom_gallery_items = $symptom_gallery->field_symptom_gallery_items;

                    if ($symptom_gallery_items) {
                      foreach ($symptom_gallery_items as $key => $item) {
                        $entityReference = $item->get('entity');
                        $entityAdapter = $entityReference->getTarget();
                        $referencedEntity = $entityAdapter->getValue();

                        $image = NULL;

                        if ($referencedEntity->field_symptom_gallery_item_image->target_id) {
                          $image = ImageHelper::exportImagePathData($referencedEntity, 'field_symptom_gallery_item_image');
                        }

                        $gallery_items[$key] = [
                          'id' => $referencedEntity->id(),
                          'image' => $image,
                          'title' => $referencedEntity->title->value,
                        ];
                      }
                    }
                  }

                  if ($term !== NULL) {
                    $content['conditions'][] = [
                      'condition' => [
                        'id' => $term->id(),
                        'name' => $this->getConditionNameById($term->id()),
                      ],
                      'title' => $term->field_ibd_dis_title->value,
                      'section_article_string_1' => $term->field_ibd_dis_section_str1->value,
                      'section_article_string_2' => $term->field_ibd_dis_section_str2->value,
                      'section_article_description' => $term->field_ibd_dis_section_desc->value,
                      'cta' => [
                        'title' => $term->field_ibd_dis_cta_title->value,
                        'description' => $term->field_ibd_dis_cta_desc->value,
                        'button_label' => $term->field_ibd_dis_cta_button_label->value,
                        'button_url' => $term->field_ibd_dis_cta_button_url->value,
                      ],
                      'video' => $video,
                      'symptom_gallery' => $gallery_items,
                      'footer' => [
                        'footer_iso_number' => $term->field_ibd_dis_page_iso->value,
                      ],
                      'footer_iso_number' => $term->field_ibd_dis_page_iso->value,
                    ];
                  }
                }
              default:
                break;
            }
          }
          break;

        case 'toolbox':
          $param = $request->query->get('param');
          $term = $this->getData($param);
          $menu = $this->getMenu();
          $roles = $this->getMenuRoles($menu, $page);
          $footer_param = FALSE;
          if ($roles !== NULL) {
            $footer_param = $this->shouldDisplayFooterSituational($roles);
          }
          $content = [
            'menu' => $menu,
            'breadcrumbs' => $this->getBreadcrumbs($menu, ($term->field_title->value ? $term->field_title->value : $this->t('PAGE_TITLE')), $param),
            'title' => isset($term->field_title->value) ? $term->field_title->value : strtoupper($param) . '_TITLE',
            'subtitle' => isset($term->field_subtitle->value) ? $term->field_subtitle->value : strtoupper($param) . '_SUBTITLE',
            'footer' => $this->getFooter($footer_param),
          ];
          break;

        case 'coaching-tool':
          $term = $this->getData($page);
          $menu = $this->getMenu();
          $roles = $this->getMenuRoles($menu, $page);
          $footer_param = FALSE;
          if ($roles !== NULL) {
            $footer_param = $this->shouldDisplayFooterSituational($roles);
          }
          $content = [
            'menu' => $menu,
            'breadcrumbs' => $this->getBreadcrumbs($menu, ($term->field_title->value ? $term->field_title->value : $this->t('PAGE_TITLE')), $homepage_carousel),
            'title' => isset($term->field_title->value) ? $term->field_title->value : strtoupper($page) . '_TITLE',
            'subtitle' => isset($term->field_subtitle->value) ? $term->field_subtitle->value : strtoupper($page) . '_SUBTITLE',
            'footer' => $this->getFooter($footer_param),
          ];
          break;

        case 'hcp-contact-form-question-types':
        case 'hcp-contact-form-name-titles':
          $terms = $this->getData($page, TRUE);
          $content = [];
          foreach ($terms as $term) {
            $content[] = ["id" => $term->id(), "label" => $term->name->value];
          }
          break;

        default:
          $response = [
            'success' => FALSE,
            'status_code' => self::HTTP_UNPROC,
            'message' => $this->t("INTERNAL_ERROR"),
            'data' => [
              'error_title' => $this->t('PAGE_DOES_NOT_EXIST'),
            ],
          ];

          return new ModifiedResourceResponse($response);
      }

      $payload = [
        'success' => TRUE,
        'status_code' => self::HTTP_OK,
        'message' => $this->t("SUCCESSFUL"),
        'data' => [
          'content' => $content,
        ],
      ];
      return new ModifiedResourceResponse($payload);
    }
    catch (\Exception $e) {
      $response = [
        'success' => FALSE,
        'status_code' => self::HTTP_UNPROC,
        'message' => $e->getMessage(),
        'data' => [],
      ];

      return new ModifiedResourceResponse($response);
    }
  }

  /**
   *
   */
  private function getDefaultConditionId($user_id) {
    $page = 'your-condition';
    $data = $this->getData($page);
    if ($user_id === 0) {
      return $data->field_ur_cond_page_default_cond->value;
    }

    $condition = $this->getUsersCondition($user_id);
    if (!$condition) {
      $condition = $data->field_ur_cond_page_default_cond->value;
    }

    return $condition;
  }

  /**
   *
   */
  private function getConditionNameById($condition_id) {
    $condition = Node::load($condition_id);

    if (!$condition) {
      return NULL;
    }

    return $condition->getTitle();
  }

  /**
   *
   */
  private function getDefaultConditionData($condition_id) {
    $condition_taxonomies = [
      'frontend_ibd_disease_taxonomy' => 'field_ibd_dis_cond_ref',
      'frontend_juve_disease_taxonomy' => 'field_juv_assigned_condition_ref',
    ];

    foreach ($condition_taxonomies as $tax_name => $reference) {
      $properties['vid'] = $tax_name;

      $terms = \Drupal::entityTypeManager()
        ->getStorage('taxonomy_term')
        ->loadByProperties($properties);

      $term = reset($terms);

      $assigned_condition_id = $term->$reference[0]->target_id;

      if ($condition_id === $assigned_condition_id) {
        return $term;
      }

      return NULL;
    }
  }

  /**
   *
   */
  private function getAllConditionDataExceptOne($condition_id) {
    $condition_taxonomies = [
      'frontend_ibd_disease_taxonomy' => 'field_ibd_dis_cond_ref',
      'frontend_juve_disease_taxonomy' => 'field_juv_assigned_condition_ref',
    ];

    $terms_array = [];

    foreach ($condition_taxonomies as $tax_name => $reference) {
      $properties['vid'] = $tax_name;

      $terms = \Drupal::entityTypeManager()
        ->getStorage('taxonomy_term')
        ->loadByProperties($properties);

      $term = reset($terms);

      // Minden taxban más a neve a field_assigned_condition_ref-nek, ez a baj.
      $assigned_condition_id = $term->$reference[0]->target_id;

      if ($condition_id === $assigned_condition_id) {
        continue;
      }

      $terms_array[] = $term;
    }

    return $terms_array;
  }

  /**
   *
   */
  private function getUsersCondition($user_id) {
    $condition = NULL;
    $account = User::load($user_id);
    foreach ($account->get('field_diagnosed_condition_ref') as $condition) {
      if (method_exists($condition, 'getvalue')) {
        $condition_id[] = $condition->getValue()['target_id'];
      }
    }

    if (!empty($condition_id)) {
      $condition = $condition_id[0];
    }

    return $condition;
  }

  /**
   *
   */
  private function getBreadcrumbs($menu, $leaves_label, $page) {
    $result = [];
    $this->buildBreadcrumbTreeWithUrl($menu, $result);

    $node_path_storage = [];
    $this->getPathFromTree($result, '', $node_path_storage);

    foreach ($node_path_storage as $key => $element) {
      $node_path_storage[$key] = str_replace('//', '/', $element);
      if ($element === '' || $element === '/') {
        unset($node_path_storage[$key]);
      }

      if (strpos($element, $page) === FALSE) {
        unset($node_path_storage[$key]);
      }
    }

    $tmp = explode('/', current($node_path_storage));

    foreach ($tmp as $key => $item) {
      if ($item === $page || $item === '') {
        unset($tmp[$key]);
      }
    }

    $breads = [];

    foreach ($tmp as $url) {
      $this->getMenuNameForUrl($menu, $breads, $url);
    }

    $breadcrumbs[] = [
      'label' => $this->t('HOME'),
      'url' => '/',
    ];

    foreach ($breads as $url => $label) {
      $breadcrumbs[] = [
        'label' => $this->t($label),
        'url' => $url,
      ];
    }

    $breadcrumbs[] = [
      'label' => $leaves_label,
      'url' => NULL,
    ];

    return $breadcrumbs;
  }

  /**
   *
   */
  private function buildBreadcrumbTreeWithUrl($array, &$result) {
    foreach ($array as $key => $item) {
      $result[$key] = [
        'url' => $item['url'],
        'children' => $item['child'],
      ];
    }
  }

  /**
   *
   */
  private function getMenuNameForUrl($array, &$result, $search) {
    foreach ($array as $key => $item) {
      if ($item['url'] === '/' . $search) {
        $result[$item['url']] = $item['label'];
      }
      else {
        $this->getMenuNameForUrl($item['child'], $result, $search);
      }
    }
  }

  /**
   *
   */
  private function getPathFromTree($children, $node_path, &$node_path_storage) {
    $original_node_path = $node_path;
    foreach ($children as $child) {
      $node_path = $original_node_path . '/' . $child['url'];
      array_push($node_path_storage, $node_path);
      if (!empty($child['children'])) {
        $this->getPathFromTree($child['children'], $node_path, $node_path_storage);
      }
      elseif (!empty($child['child'])) {
        $this->getPathFromTree($child['child'], $node_path, $node_path_storage);
      }
    }
  }

  /**
   *
   */
  private function getData($page, $returnAll = FALSE) {
    switch ($page) {
      case 'login':
        $vid = 'login_test_taxonomy';
        break;

      case 'register-patient':
        $vid = 'register_patient_taxonomy';
        break;

      case 'register-hcp':
        $vid = 'register_hcp_taxonomy';
        break;

      case 'footer':
        $vid = 'frontend_footer_taxonomy';
        break;

      case 'homepage':
      case 'main-page':
        $vid = 'frontend_main_page_taxonomy';
        break;

      case 'restricted-popup':
        $vid = 'frontend_rstr_popup_taxonomy';
        break;

      case 'getting-started':
        $vid = 'frontend_getting_start_taxonomy';
        break;

      case 'profile':
        $vid = 'frontend_profile_taxonomy';
        break;

      case 'nutrition':
        $vid = 'frontend_nutrition_taxonomy';
        break;

      case 'policies':
        $vid = 'frontend_policies_taxonomy';
        break;

      case 'eating-well':
        $vid = 'frontend_eating_well_taxonomy';
        break;

      case 'your-condition':
        $vid = 'frontend_your_condition_taxonomy';
        break;

      case 'hcp-contact-form-question-types':
        $vid = 'hcp_contact_form';
        break;

      case 'hcp-contact-form-name-titles':
        $vid = 'name_titles';
        break;
    }

    $refactored_pages = [
      'track-symptoms',
      'symptom-tracker-settings',
      'log-medication',
      'goals',
      'appointments',
      'notes',
      'reminders',
      'log-blood-test-results',
      'coaching-tool',
    ];
    if (in_array($page, $refactored_pages)) {
      $vid = 'frontend_' . str_replace('-', '_', $page);
    }

    $properties['vid'] = $vid;

    $terms = \Drupal::entityTypeManager()
      ->getStorage('taxonomy_term')
      ->loadByProperties($properties);

    if ($returnAll) {
      return $terms;
    }
    return reset($terms);
  }

  /**
   *
   */
  private function generateSubMenuTree(&$output, &$input, $parent = FALSE) {
    $input = array_values($input);

    // Zero means root.
    $counter = 0;
    foreach ($input as $key => $item) {
      if ($item->link->isEnabled()) {
        $name = $item->link->getTitle();
        $url = $item->link->getUrlObject();
        $url_string = $url->toString();
        $tmp = explode('/', $url_string);
        $language = \Drupal::languageManager()->getCurrentLanguage()->getId();
        $weight = $item->link->getWeight();
        $key = (int) $weight;

        $roles = [];

        if (get_class($item->link) === 'Drupal\menu_link_content\Plugin\Menu\MenuLinkContent') {
          foreach ($item->link->_getEntity()->field_menu_role_list as $list_item) {
            $roles[] = $list_item->value;
          }
        }
        else {
          $options = $item->link->getPluginDefinition()['options'];
          if (!empty($options)) {
            $roles = $options['field_menu_role_list'];
          }
          else {
            $roles = [''];
          }
        }

        if ($tmp[0] === 'https:' || $tmp[0] === 'http:') {
        }
        elseif (count($tmp) === 2) {
          if ($tmp[0] === '' && $tmp[1] === $language) {
            $url_string = '/';

            if ($counter === 0) {
              $key = $url_string;
            }
          }
          else {
            $url_string = "/" . end($tmp);
          }
        }
        else {
          end($tmp);
          $string = prev($tmp);
          $url_string = "/{$string}";
        }

        if ($parent === FALSE) {
          $output[$key] = [
            'label' => $name,
            'url' => $url_string,
            'roles' => $roles,
          ];
        }
        else {
          $parent = 'submenu-' . $parent;
          $output['child'][$key] = [
            'label' => $name,
            'url' => $url_string,
            'roles' => $roles,
          ];
        }

        if ($item->hasChildren) {
          if ($item->depth == 1) {
            $this->generateSubMenuTree($output[$key], $item->subtree, $key);
          }
          else {
            $this->generateSubMenuTree($output['child'][$key], $item->subtree, $key);
          }
        }
      }

      $counter++;
    }
  }

  /**
   *
   */
  private function getMenu() {
    $sub_nav = \Drupal::menuTree()->load('frontend', new MenuTreeParameters());
    $menu_tree2 = NULL;
    $this->generateSubMenuTree($menu_tree2, $sub_nav);

    $menu = $menu_tree2['/']['child'];

    ksort($menu);

    $counter = 0;
    $result = [];
    foreach ($menu as $key => $item) {
      $result[$counter++] = $item;
    }

    return $result;
  }

  /**
   *
   */
  private function getFooter($situational_visible = FALSE) {
    $term = $this->getData('footer');

    $sub_nav = \Drupal::menuTree()->load('disclaimer', new MenuTreeParameters());
    $menu_tree2 = NULL;
    $this->generateSubMenuTree($menu_tree2, $sub_nav);
    $footer_menu = $menu_tree2['/']['child'];

    ksort($footer_menu);

    $counter = 0;
    $result = [];
    foreach ($footer_menu as $key => $item) {
      $result[$counter++] = $item;
    }

    return [
      'footer_disclaimer_situational' => $term->field_footer_disclaimer_situatio[0]->value,
      'footer_disclaimer_situational_visible' => $situational_visible,
      'footer_disclaimer_normal' => $term->field_footer_disclaimer_normal[0]->value,
      'footer_date_of_preparation_label' => $term->field_footer_date_label[0]->value,
      'footer_menu' => $result,
    ];
  }

  /**
   *
   */
  private function getMenuRoles($menu, $page) {
    foreach ($menu as $item) {
      $url = $item['url'];
      $arr_url = explode('/', $url);
      $end = end($arr_url);

      if ($end === $page) {
        return $item['roles'];
      }

      if (isset($item['child'])) {
        return $this->getMenuRoles($item['child'], $page);
      }
    }
  }

  /**
   *
   */
  private function shouldDisplayFooterSituational($roles) {
    if (count($roles) === 1 && $roles[0] === '') {
      return FALSE;
    }

    return TRUE;
  }

}
