<?php

namespace Drupal\page_content_rest_api\Helper;

use Drupal\file\Entity\File;

class ImageHelper
{
  public static function exportImagePathData($referencedEntity, $image_field_name)
  {
    $image = $referencedEntity->$image_field_name->target_id;
    $current_logo_id = $image;
    $current_Logo = File::load($current_logo_id);
    $uri = $current_Logo->getFileUri();
    $stream_wrapper_manager = \Drupal::service('stream_wrapper_manager')->getViaUri($uri);
    $file_path = $stream_wrapper_manager->realpath();
    $ext_path = $stream_wrapper_manager->getExternalUrl();

    if (file_exists($file_path) && strpos($file_path, '/web/web/') && strpos($ext_path, '/web/web/') === false) {
      $ext_path = str_replace('/web/', '/web/web/', $ext_path);
    }

    $ext_path = str_replace('http://', 'https://', $ext_path);

    $image = [
      'url' => $ext_path,
      'file_path' => $file_path,
    ];

    $path = pathinfo($file_path);
    $path_webp = $path['filename'] . '.webp';

    $full_path_webp = $path['dirname'] . DIRECTORY_SEPARATOR . $path_webp;

    if (file_exists($full_path_webp)) {
      $path = pathinfo($ext_path);
      $file_webp = $path['filename'] . '.webp';

      $full_url_webp = $path['dirname'] . "/" . $file_webp;

      $full_url_webp = str_replace('http://', 'https://', $full_url_webp);

      $image['webp_url'] = $full_url_webp;
    }

    return $image;
  }
}
