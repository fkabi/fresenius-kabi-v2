<?php

namespace Drupal\goal_rest_api\Plugin\rest\resource;

use Drupal\Core\Annotation\Translation;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\rest\Annotation\RestResource;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Symfony\Component\HttpFoundation\Request;
use Drupal\rest\ModifiedResourceResponse;

/**
 * Provides Resource for the Goal functionality
 *
 * @RestResource(
 *   id = "goal_get",
 *   label = @Translation("GOAL_GET"),
 *   uri_paths = {
 *     "canonical" = "/goal-rest-api/get-goals"
 *   }
 * )
 */
class GoalGet extends ResourceBase
{
  const HTTP_OK = 200;
  const HTTP_UNPROC = 422;

  /**
   * Responds to entity GET requests.
   * @return \Drupal\rest\ResourceResponse
   */
  public function get(Request $request)
  {
    $interval = $request->query->get('interval');

    try {
      $user = \Drupal::currentUser();
      $userLangId = $user->getPreferredLangcode();

      $result = \Drupal::entityQuery('node')
        ->condition('type', 'goal')
        ->condition('field_goal_user_ref', $user->id())
        ->condition('field_goal_status', [0, 1], 'IN');

      if ($interval !== null) {
        $start = new \DateTime('now');
        $start->setTime(0, 0, 0);
        $start = DrupalDateTime::createFromDateTime($start);

        $end = new \DateTime('now');

        switch ($interval) {
          case 'day':
            $end->modify('+1 days');
            $end->setTime(0, 0, 0);
            $end = DrupalDateTime::createFromDateTime($end);

            break;
          case 'week':
            $end->modify('+6 days');
            $end->setTime(0, 0, 0);
            $end = DrupalDateTime::createFromDateTime($end);

            break;
          case 'month':
            $end->modify('+30 days');
            $end->setTime(0, 0, 0);
            $end = DrupalDateTime::createFromDateTime($end);

            break;
          case 'year':
          default:
            $end->modify('+365 days');
            $end->setTime(0, 0, 0);
            $end = DrupalDateTime::createFromDateTime($end);

            break;
        }

        $result->condition('field_goal_target_date', $start, '>=');
        $result->condition('field_goal_target_date', $end, '<=');
      }

      $result = $result->execute();
      $nids = \Drupal::entityTypeManager()->getStorage('node')->loadMultiple($result);

      $goals = [];

      foreach ($nids as $id => $node) {
        $date = date('Y-m-d H:i:s', strtotime($node->field_goal_target_date[0]->value));

        $goals[$date][] = [
          'id' => $node->id(),
          'name' => $node->field_goal_name[0]->value,
          'date' => $date,
          'status' => $node->field_goal_status[0]->value,
        ];
      }

      if (empty($goals)) {
        $payload = [
          'success' => false,
          'status_code' => self::HTTP_OK,
          'message' => $this->t("USER_DOES_NOT_HAVE_ANY_GOAL"),
          'data' => [
            'content' => [],
          ],
        ];
      } else {
        $payload = [
          'success' => true,
          'status_code' => self::HTTP_OK,
          'message' => $this->t("SUCCESSFUL"),
          'data' => [
            'content' => $goals,
          ],
        ];
      }
      return new ModifiedResourceResponse($payload, 200);
    } catch (\Exception $e) {
      $response = [
        'success' => false,
        'status_code' => self::HTTP_UNPROC,
        'message' => $e->getMessage(),
        'data' => [
          'line' => $e->getLine(),
        ],

      ];

      return new ModifiedResourceResponse($response, 200);
    }
  }
}
