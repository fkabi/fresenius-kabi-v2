<?php

namespace Drupal\goal_rest_api\Plugin\rest\resource;

use Drupal\Core\Annotation\Translation;
use Drupal\kabi\Validation\ApiValidation;
use Drupal\node\Entity\Node;
use Drupal\rest\Annotation\RestResource;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Provides Resource for the Goal functionality
 *
 * @RestResource(
 *   id = "goal_create",
 *   label = @Translation("GOAL_CREATE"),
 *   uri_paths = {
 *     "canonical" = "/goal-rest-api/create-goal",
 *     "create" = "/goal-rest-api/create-goal"
 *   }
 * )
 */
class GoalCreate extends ResourceBase
{
  const HTTP_OK = 200;
  const HTTP_UNPROC = 422;

  /**
   * Responds to entity POST requests.
   * @return \Drupal\rest\ResourceResponse
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function post(Request $request)
  {
    $content = json_decode($request->getContent(), true);

    $errors = ApiValidation::validate($content, self::getRules(), 'goal');

    if ($errors) {
      $response = [
        'success' => false,
        'status_code' => self::HTTP_UNPROC,
        'message' => $this->t("INVALID_DATA"),
        'data' => $errors,
      ];

      return new ResourceResponse($response);
    }

    try {
      $user = \Drupal::currentUser();
      $user_id = $user->id();
    } catch (\Exception $e) {
      $response = [
        'success' => false,
        'status_code' => self::HTTP_UNPROC,
        'message' => $this->t("USER_NOT_FOUND"),
        'data' => [],
      ];

      return new ResourceResponse($response);
    }

    $account = \Drupal\user\Entity\User::load($user_id);
    if ($account->field_used_access_code[0]->target_id === null) {
      $response = [
        'success' => false,
        'status_code' => self::HTTP_OK,
        'message' => $this->t("USER_CANNOT_CREATE_GOAL"),
        'data' => [
          'content' => $this->t("USER_DOES_NOT_HAVE_A_VALID_ACCESS_CODE"),
        ],
      ];

      return new ResourceResponse($response);
    }

    try {
      $goal = [
        'type' => 'goal',
      ];

      $goal_time = strtotime($content["field_goal_target_date"][0]["value"]);
      $time = strtotime('today midnight');

      if (!$goal_time || $goal_time < $time) {
        $response = [
          'success' => false,
          'status_code' => self::HTTP_OK,
          'message' => $this->t("INVALID_DATE"),
          'data' => [
            'content' => $this->t("INVALID_DATE"),
          ],
        ];

        return new ResourceResponse($response);
      }

      $date = date('Y-m-d\TH:i:s', $goal_time);

      $goal["title"] = $content["field_goal_name"][0]["value"];
      $goal["field_goal_status"] = 0;
      $goal["field_goal_target_date"] = $date;
      $goal["field_goal_name"] = $content["field_goal_name"][0]["value"];
      $goal["field_goal_user_ref"] = $user_id;

      $node = Node::create($goal);
      $node->save();
    } catch (\Exception $e) {
      $response = [
        'success' => false,
        'status_code' => self::HTTP_UNPROC,
        'message' => $this->t("GOAL_CANNOT_BE_CREATED"),
        'data' => [],
      ];

      return new ResourceResponse($response);
    }

    return new ResourceResponse([
      'success' => true,
      'status_code' => self::HTTP_OK,
      'message' => $this->t("GOAL_CREATED"),
      'data' => [
        'content' => [
          $node->id() => [
            'id' => $node->id(),
            'name' => $node->field_goal_name[0]->value,
            'date' => $date,
            'status' => $node->field_goal_status[0]->value,
          ],
        ],
      ],
    ]);
  }

  public static function getRules(): array
  {
    return [
      "field_goal_name" => "required|min:1|max:128",
      "field_goal_target_date" => "required",
    ];
  }
}
