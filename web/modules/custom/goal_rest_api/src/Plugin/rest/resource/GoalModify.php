<?php

namespace Drupal\goal_rest_api\Plugin\rest\resource;

use Drupal\Core\Annotation\Translation;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\kabi\Validation\ApiValidation;
use Drupal\node\Entity\Node;
use Drupal\rest\Annotation\RestResource;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Provides Resource for the Goal functionality
 *
 * @RestResource(
 *   id = "goal_modify",
 *   label = @Translation("GOAL_MODIFY"),
 *   uri_paths = {
 *     "canonical" = "/goal-rest-api/{goal_id}/modify-goal",
 *     "create" = "/goal-rest-api/{goal_id}/modify-goal"
 *   }
 * )
 */
class GoalModify extends ResourceBase
{
  const HTTP_OK = 200;
  const HTTP_UNPROC = 422;

  /**
   * Responds to entity POST requests.
   * @return \Drupal\rest\ResourceResponse
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function post(Request $request, $goal_id)
  {
    $content = json_decode($request->getContent(), true);

    $node_original_status = null;

    if ($content === null) {
      $response = [
        'success' => false,
        'status_code' => self::HTTP_UNPROC,
        'message' => $this->t("INVALID_DATA"),
        'data' => [
          'error_title' => $this->t('EMPTY_REQUEST'),
        ],
      ];

      return new ResourceResponse($response);
    }

    $errors = ApiValidation::validate($content, self::getRules(), 'goal');

    if ($errors) {
      $response = [
        'success' => false,
        'status_code' => self::HTTP_UNPROC,
        'message' => $this->t("INVALID_DATA"),
        'data' => $errors,
      ];

      return new ResourceResponse($response);
    }

    try {
      $user = \Drupal::currentUser();
      $user_id = $user->id();
    } catch (\Exception $e) {
      $response = [
        'success' => false,
        'status_code' => self::HTTP_UNPROC,
        'message' => $this->t("USER_NOT_FOUND"),
        'data' => [],
      ];

      return new ResourceResponse($response);
    }

    $account = \Drupal\user\Entity\User::load($user_id);
    if ($account->field_used_access_code[0]->target_id === null) {
      $response = [
        'success' => false,
        'status_code' => self::HTTP_OK,
        'message' => $this->t("USER_CANNOT_MODIFY_GOAL"),
        'data' => [
          'content' => $this->t("USER_DOES_NOT_HAVE_A_VALID_ACCESS_CODE"),
        ],
      ];

      return new ResourceResponse($response);
    }

    try {
      $nids = \Drupal::entityTypeManager()
        ->getListBuilder('node')
        ->getStorage()
        ->loadByProperties([
          'type' => "goal",
          'nid' => $goal_id,
          'field_goal_user_ref' => $user_id,
        ]);
      $keys = array_keys($nids);
      $node = null;
      if (isset($keys[0])) {
        $key = $keys[0];
        $node = $nids[$key];
      }

      if ($node === null) {
        $response = [
          'success' => false,
          'status_code' => self::HTTP_OK,
          'message' => $this->t("GOAL_DOES_NOT_EXIST_OR_DOES_NOT_BELONG_TO_USER"),
          'data' => [
            'content' => $this->t("GOAL_DOES_NOT_EXIST_OR_DOES_NOT_BELONG_TO_USER"),
          ],
        ];

        return new ResourceResponse($response);
      }

      $goal_name = $content['field_goal_name'][0]['value'];
      $goal_status = $content['field_goal_status'][0]['value'];
      $goal_time = strtotime($content["field_goal_target_date"][0]["value"]);

      $time = strtotime('today midnight');

      if ($goal_status != 1 && (!$goal_time || $goal_time < $time)) {
        $response = [
          'success' => false,
          'status_code' => self::HTTP_OK,
          'message' => $this->t("INVALID_DATE"),
          'data' => [
            'content' => $this->t("INVALID_DATE"),
          ],
        ];

        return new ResourceResponse($response);
      }

      $goal_time = date('Y-m-d\TH:i:s', $goal_time);

      if ($goal_status !== null && (!is_numeric($goal_status) || ($goal_status < 0 || $goal_status > 1))) {
        $response = [
          'success' => false,
          'status_code' => self::HTTP_OK,
          'message' => $this->t("INVALID_STATUS"),
          'data' => [
            'content' => $this->t("INVALID_STATUS"),
          ],
        ];

        return new ResourceResponse($response);
      }

      $node_original_status = $node->field_goal_status[0]->value;

      if($node_original_status === '1'){
        $response = [
          'success' => false,
          'status_code' => self::HTTP_OK,
          'message' => $this->t("CANNOT_MODIFY_COMPLETED_GOAL"),
          'data' => [
            'content' => $this->t("CANNOT_MODIFY_COMPLETED_GOAL"),
          ],
        ];

        return new ResourceResponse($response);
      }

      if ($goal_name) {
        $node->set('field_goal_name', $goal_name);
      }

      if ($goal_status) {
        $node->set('field_goal_status', $goal_status);
      }

      if ($goal_time) {
        $node->set('field_goal_target_date', $goal_time);
      }

      $node->save();
    } catch (\Exception $e) {
      $response = [
        'success' => false,
        'status_code' => self::HTTP_UNPROC,
        'message' => $this->t("GOAL_CANNOT_BE_MODIFIED"),
        'data' => [],
      ];

      return new ResourceResponse($response);
    }

    return new ResourceResponse([
      'success' => true,
      'status_code' => self::HTTP_OK,
      'message' => $this->t("GOAL_MODIFIED"),
      'data' => [
        'content' => '',
      ],
    ]);
  }

  public static function getRules(): array
  {
    return [
      "field_goal_name" => "min:1|max:128",
    ];
  }
}
