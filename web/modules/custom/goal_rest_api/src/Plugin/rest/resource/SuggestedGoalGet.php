<?php

namespace Drupal\goal_rest_api\Plugin\rest\resource;

use Drupal\Core\Annotation\Translation;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\rest\Annotation\RestResource;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Provides Resource for the Goal functionality
 *
 * @RestResource(
 *   id = "suggested_goal_get",
 *   label = @Translation("SUGGESTED_GOAL_GET"),
 *   uri_paths = {
 *     "canonical" = "/goal-rest-api/get-suggested-goals"
 *   }
 * )
 */
class SuggestedGoalGet extends ResourceBase
{
  const HTTP_OK = 200;
  const HTTP_UNPROC = 422;

  /**
   * Responds to entity GET requests.
   * @return ResourceResponse
   */
  public function get(): ResourceResponse
  {
    try {
      $user = \Drupal::currentUser();
      $userLangId = $user->getPreferredLangcode();

      $site_settings = \Drupal::service('site_settings.loader');
      $settings = $site_settings->loadByFieldset('suggested_goals');

      if ($settings['suggested_goals'] === '') {
        $payload = [
          'success' => false,
          'status_code' => self::HTTP_OK,
          'message' => $this->t("EMPTY_SUGGESTED_GOALS"),
          'data' => [
            'content' => [],
          ],
        ];
      }

      $goals = [];

      foreach ($settings['suggested_goals'] as $suggested_goal) {
        $goals[] = ['title' => $suggested_goal];
      }

      $payload = [
        'success' => true,
        'status_code' => self::HTTP_OK,
        'message' => $this->t("SUCCESSFUL"),
        'data' => [
          'content' => ['suggestedGoals' => $goals],
        ],
      ];

      $response = new ResourceResponse($payload);
      $response->getCacheableMetadata()->addCacheContexts(['user']);
      $response->addCacheableDependency(CacheableMetadata::createFromRenderArray([
        '#cache' => [
          'context' => ['user'],
          'tags' => ['user:3', 'languages:' . $userLangId],
          'max-age' => 0,
        ],
      ]));

      return $response;
    } catch (\Exception $e) {
      $response = [
        'success' => false,
        'status_code' => self::HTTP_UNPROC,
        'message' => $e->getMessage(),
        'data' => [
          'line' => $e->getLine(),
        ],
      ];

      return new ResourceResponse($response);
    }
  }
}
