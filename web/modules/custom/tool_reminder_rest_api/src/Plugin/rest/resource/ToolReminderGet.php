<?php

namespace Drupal\tool_reminder_rest_api\Plugin\rest\resource;

use Drupal\Core\Annotation\Translation;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\node\Entity\Node;
use Drupal\rest\Annotation\RestResource;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;

/**
 * Provides Resource for the Tool Reminder functionality
 *
 * @RestResource(
 *   id = "tool_reminder_get",
 *   label = @Translation("TOOL_REMINDER_GET"),
 *   uri_paths = {
 *     "canonical" = "/tool-reminder-rest-api/get-tool-reminder"
 *   }
 * )
 */
class ToolReminderGet extends ResourceBase
{
  const HTTP_OK = 200;
  const HTTP_UNPROC = 422;

  /**
   * Responds to entity GET requests.
   * @return \Drupal\rest\ResourceResponse
   */
  public function get()
  {
    try {
      $user = \Drupal::currentUser();
      $userLangId = $user->getPreferredLangcode();

      $nids = \Drupal::entityTypeManager()
        ->getListBuilder('node')
        ->getStorage()
        ->loadByProperties([
          'type' => "tool_reminder",
          'field_tool_reminder_user_ref' => $user->id(),
        ]);

      if (empty($nids)) {
        $payload = [
          'success' => true,
          'status_code' => self::HTTP_OK,
          'message' => $this->t("THE_USER_HAS_NO_TOOL_REMINDER_SET"),
        ];
      } else {
        $node = $nids[array_key_first($nids)];

        $payload = [
          'success' => true,
          'status_code' => self::HTTP_OK,
          'message' => $this->t("THE_USER_HAS_A_TOOL_REMINDER_SET"),
          'data' => [
            'field_tool_reminder_frequency' => $node->field_tool_reminder_frequency[0]->value,
            'field_tool_reminder_weekday' => $node->field_tool_reminder_weekday[0]->value,
          ],
        ];
      }

      $response = new ResourceResponse($payload);
      $response->getCacheableMetadata()->addCacheContexts(['user']);
      $response->addCacheableDependency(CacheableMetadata::createFromRenderArray([
        '#cache' => [
          'context' => ['user'],
          'tags' => ['user:3', 'languages:' . $userLangId],
          'max-age' => 0,
        ],
      ]));

      return $response;
    } catch (\Exception $e) {
      $response = [
        'success' => false,
        'status_code' => self::HTTP_UNPROC,
        'message' => $e->getMessage(),
        'data' => [],
      ];

      return new ResourceResponse($response);
    }
  }
}
