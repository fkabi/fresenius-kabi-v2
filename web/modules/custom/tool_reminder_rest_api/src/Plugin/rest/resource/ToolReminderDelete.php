<?php

namespace Drupal\tool_reminder_rest_api\Plugin\rest\resource;

use Drupal\Core\Annotation\Translation;
use Drupal\node\Entity\Node;
use Drupal\rest\Annotation\RestResource;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Provides Resource for the Tool Reminder functionality
 *
 * @RestResource(
 *   id = "tool_reminder_delete",
 *   label = @Translation("TOOL_REMINDER_DELETE"),
 *   uri_paths = {
 *     "canonical" = "/tool-reminder-rest-api/delete-tool-reminder",
 *     "create" = "/tool-reminder-rest-api/delete-tool-reminder"
 *   }
 * )
 */
class ToolReminderDelete extends ResourceBase
{
  const HTTP_OK = 200;
  const HTTP_UNPROC = 422;

  /**
   * Responds to entity POST requests.
   * @return \Drupal\rest\ResourceResponse
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function post(Request $request)
  {
    try {
      $user = \Drupal::currentUser();
      $user_id = $user->id();
    } catch (\Exception $e) {
      $response = [
        'success' => false,
        'status_code' => self::HTTP_UNPROC,
        'message' => $this->t("USER_NOT_FOUND"),
        'data' => [],
      ];

      return new ResourceResponse($response);
    }

    $account = \Drupal\user\Entity\User::load($user_id);
    if ($account->field_used_access_code[0]->target_id === null) {
      $response = [
        'success' => false,
        'status_code' => self::HTTP_OK,
        'message' => $this->t("USER_CANNOT_DELETE_TOOL_REMINDER"),
        'data' => [
          'content' => $this->t("USER_DOES_NOT_HAVE_A_VALID_ACCESS_CODE"),
        ],
      ];

      return new ResourceResponse($response);
    }

    try {
      $nids = \Drupal::entityTypeManager()
        ->getListBuilder('node')
        ->getStorage()
        ->loadByProperties([
          'type' => "tool_reminder",
          'field_tool_reminder_user_ref' => $user_id,
        ]);
      $keys = array_keys($nids);
      $node = null;
      if (isset($keys[0])) {
        $key = $keys[0];
        $node = $nids[$key];
      }

      if($node === null) {
        $response = [
          'success' => false,
          'status_code' => self::HTTP_OK,
          'message' => $this->t("THE_USER_DOES_NOT_HAVE_A_TOOL_REMINDER_YET"),
          'data' => [
            'content' => $this->t("THE_USER_DOES_NOT_HAVE_A_TOOL_REMINDER_YET"),
          ],
        ];

        return new ResourceResponse($response);
      }

      $node->delete();
    } catch (\Exception $e) {
      $response = [
        'success' => false,
        'status_code' => self::HTTP_UNPROC,
        'message' => $this->t("TOOL_REMINDER_CANNOT_BE_DELETED"),
        'data' => [],
      ];

      return new ResourceResponse($response);
    }

    return new ResourceResponse([
      'success' => true,
      'status_code' => self::HTTP_OK,
      'message' => $this->t("TOOL_REMINDER_DELETED"),
      'data' => [
        'content' => '',
      ],
    ]);
  }
}
