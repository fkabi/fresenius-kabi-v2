<?php

namespace Drupal\medication_rest_api\Plugin\rest\resource;

use Drupal\Core\Annotation\Translation;
use Drupal\kabi\Validation\ApiValidation;
use Drupal\node\Entity\Node;
use Drupal\rest\Annotation\RestResource;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Provides Resource for the Medication functionality
 *
 * @RestResource(
 *   id = "medication_modify",
 *   label = @Translation("MEDICATION_MODIFY"),
 *   uri_paths = {
 *     "canonical" = "/medication-rest-api/{medication_id}/modify-medication",
 *     "create" = "/medication-rest-api/{medication_id}/modify-medication"
 *   }
 * )
 */
class MedicationModify extends ResourceBase
{
  const HTTP_OK = 200;
  const HTTP_UNPROC = 422;

  /**
   * Responds to entity POST requests.
   * @return \Drupal\rest\ResourceResponse
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function post(Request $request, $medication_id)
  {
    $content = json_decode($request->getContent(), true);

    if ($content === null) {
      $response = [
        'success' => false,
        'status_code' => self::HTTP_UNPROC,
        'message' => $this->t("INVALID_DATA"),
        'data' => [
          'error_title' => $this->t('EMPTY_REQUEST'),
        ],
      ];

      return new ResourceResponse($response);
    }

    $errors = ApiValidation::validate($content, self::getRules(), 'medication');

    if ($errors) {
      $response = [
        'success' => false,
        'status_code' => self::HTTP_UNPROC,
        'message' => $this->t("INVALID_DATA"),
        'data' => $errors,
      ];

      return new ResourceResponse($response);
    }

    try {
      $user = \Drupal::currentUser();
      $user_id = $user->id();
    } catch (\Exception $e) {
      $response = [
        'success' => false,
        'status_code' => self::HTTP_UNPROC,
        'message' => $this->t("USER_NOT_FOUND"),
        'data' => [],
      ];

      return new ResourceResponse($response);
    }

    $account = \Drupal\user\Entity\User::load($user_id);
    /*if ($account->field_used_access_code[0]->target_id === null) {
      $response = [
        'success' => false,
        'status_code' => self::HTTP_OK,
        'message' => $this->t("USER_CANNOT_MODIFY_MEDICATIONS"),
        'data' => [
          'content' => $this->t("USER_DOES_NOT_HAVE_A_VALID_ACCESS_CODE"),
        ],
      ];

      return new ResourceResponse($response);
    }*/

    try {
      $nids = \Drupal::entityTypeManager()
        ->getListBuilder('node')
        ->getStorage()
        ->loadByProperties([
          'type' => "medication",
          'nid' => $medication_id,
          'field_medication_user_ref' => $user_id,
        ]);
      $keys = array_keys($nids);
      $node = null;
      if (isset($keys[0])) {
        $key = $keys[0];
        $node = $nids[$key];
      }

      if ($node === null) {
        $response = [
          'success' => false,
          'status_code' => self::HTTP_OK,
          'message' => $this->t("MEDICATION_DOES_NOT_EXIST_OR_DOES_NOT_BELONG_TO_USER"),
          'data' => [
            'content' => $this->t("MEDICATION_DOES_NOT_EXIST_OR_DOES_NOT_BELONG_TO_USER"),
          ],
        ];

        return new ResourceResponse($response);
      }

      $medication_label = $content['field_medication_label'][0]['value'];
      $frequency = $content['field_medication_frequency'][0]['value'];
      $starting_date = $content['field_medication_starting_date'][0]['value'];
      $status = $content['field_medication_status'][0]['value'];

      if ($medication_label !== null && strlen($medication_label) < 255) {
        $node->set('field_medication_label', $medication_label);
      }

      if ($starting_date !== null) {
        $time = strtotime($starting_date);
        $today_midnight = strtotime('today midnight');

        $saved_starting_date =
          $node
            ->get('field_medication_starting_date')
            ->getValue()[0]['value'];
        $is_valid = (
          strtotime($starting_date) === strtotime($saved_starting_date)
        );
        if ($time < $today_midnight && !$is_valid) {
          $response = [
            'success' => false,
            'status_code' => self::HTTP_UNPROC,
              'message' => $this->t("INVALID_DATE"),
            'data' => '',
          ];

          return new ResourceResponse($response);
        }

        $node->set('field_medication_starting_date', date('Y-m-d\TH:i:s', $time));
      }

      if ($frequency !== null && is_numeric($frequency) && $frequency > 0 && $frequency <= 31) {
        $node->set('field_medication_frequency', $frequency);
      }

      if ($status !== null) {
        if ($status === 'FALSE') {
          $status = false;
        } else {
          $status = true;
        }

        $node->set('field_medication_status', $status);
      }

      $node->save();
    } catch (\Exception $e) {
      $response = [
        'success' => false,
        'status_code' => self::HTTP_UNPROC,
        'message' => $this->t("MEDICATION_CANNOT_BE_MODIFIED"),
        'data' => [
          'msg' => $e->getMessage(),
          'line' => $e->getLine(),
        ],
      ];

      return new ResourceResponse($response);
    }

    return new ResourceResponse([
      'success' => true,
      'status_code' => self::HTTP_OK,
      'message' => $this->t("MEDICATION_MODIFIED"),
      'data' => [
        'content' => '',
      ],
    ]);
  }

  public static function getRules(): array
  {
    return [
      "field_medication_label" => "min:1|max:128",
    ];
  }
}
