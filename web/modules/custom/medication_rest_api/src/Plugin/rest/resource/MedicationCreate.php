<?php

namespace Drupal\medication_rest_api\Plugin\rest\resource;

use Drupal\Core\Annotation\Translation;
use Drupal\kabi\Validation\ApiValidation;
use Drupal\node\Entity\Node;
use Drupal\rest\Annotation\RestResource;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Provides Resource for the Medication functionality
 *
 * @RestResource(
 *   id = "medication_create",
 *   label = @Translation("MEDICATION_CREATE"),
 *   uri_paths = {
 *     "canonical" = "/medication-rest-api/create-medication",
 *     "create" = "/medication-rest-api/create-medication"
 *   }
 * )
 */
class MedicationCreate extends ResourceBase
{
  const HTTP_OK = 200;
  const HTTP_UNPROC = 422;

  /**
   * Responds to entity POST requests.
   * @return \Drupal\rest\ResourceResponse
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function post(Request $request)
  {
    $content = json_decode($request->getContent(), true);

    if ($content === null) {
      $response = [
        'success' => false,
        'status_code' => self::HTTP_UNPROC,
        'message' => $this->t("INVALID_DATA"),
        'data' => [
          'error_title' => $this->t('EMPTY_REQUEST'),
        ],
      ];

      return new ResourceResponse($response);
    }

    $errors = ApiValidation::validate($content, self::getRules(), 'medication');

    if ($errors) {
      $response = [
        'success' => false,
        'status_code' => self::HTTP_UNPROC,
        'message' => $this->t("INVALID_DATA"),
        'data' => $errors,
      ];

      return new ResourceResponse($response);
    }

    $frequency = $content['field_medication_frequency'][0]['value'];
    if (!is_numeric($frequency) || $frequency < 1 || $frequency > 31) {
      $response = [
        'success' => false,
        'status_code' => self::HTTP_UNPROC,
        'message' => $this->t("INVALID_DATA"),
        'data' => ["field_medication_frequency" => "INVALID_DATA"],
      ];

      return new ResourceResponse($response);
    }

    try {
      $user = \Drupal::currentUser();
      $user_id = $user->id();
    } catch (\Exception $e) {
      $response = [
        'success' => false,
        'status_code' => self::HTTP_UNPROC,
        'message' => $this->t("USER_NOT_FOUND"),
        'data' => [],
      ];

      return new ResourceResponse($response);
    }

    $account = \Drupal\user\Entity\User::load($user_id);
    if ($account->field_used_access_code[0]->target_id === null) {
      $response = [
        'success' => false,
        'status_code' => self::HTTP_OK,
        'message' => $this->t("USER_CANNOT_CREATE_MEDICATION"),
        'data' => [
          'has_order' => false,
          'content' => $this->t("USER_DOES_NOT_HAVE_A_VALID_ACCESS_CODE"),
          'can_order' => false,
        ],
      ];

      return new ResourceResponse($response);
    }

    try {
      $nids = \Drupal::entityTypeManager()
        ->getListBuilder('node')
        ->getStorage()
        ->loadByProperties([
          'type' => "medication",
          'field_medication_user_ref' => $user->id(),
          'field_medication_status' => TRUE,
        ]);

      if (count($nids) >= 5) {
        throw new \Exception("LIMIT_REACHED");
      }
    } catch (\Exception $e) {
      $response = [
        'success' => false,
        'status_code' => self::HTTP_UNPROC,
        'message' => $this->t("MEDICATION_CANNOT_BE_CREATED_DUE_LIMIT_REACHED"),
        'data' => [],
      ];

      return new ResourceResponse($response);
    }

    try {
      $medication = [
        'type' => 'medication',
      ];

      $medication_name = $content['field_medication_label'][0]['value'];
      $starting_date = $content['field_medication_starting_date'][0]['value'];
      $time = strtotime($starting_date);
      $today_midnight = strtotime('today');

      if (!$time || $time < $today_midnight) {
        $response = [
          'success' => false,
          'status_code' => self::HTTP_UNPROC,
          'message' => $this->t("INVALID_DATE"),
          'data' => '',
        ];

        return new ResourceResponse($response);
      }

      $medication["title"] = $medication_name;
      $medication["field_medication_label"] = $medication_name;
      $medication["field_medication_user_ref"] = $user_id;
      $medication["field_medication_status"] = TRUE;
      $medication["field_medication_frequency"] = $frequency;
      $medication["field_medication_starting_date"] = date('Y-m-d\TH:i:s', $time);

      $node = Node::create($medication);
      $node->save();
    } catch (\Exception $e) {
      $response = [
        'success' => false,
        'status_code' => self::HTTP_UNPROC,
        'message' => $this->t("MEDICATION_CANNOT_BE_CREATED"),
        'data' => [],
      ];

      return new ResourceResponse($response);
    }

    return new ResourceResponse([
      'success' => true,
      'status_code' => self::HTTP_OK,
      'message' => $this->t("MEDICATION_CREATED"),
      'data' => [
        'content' => [
          $node->id() => [
            'id' => $node->id(),
            'name' => $node->field_medication_label->value,
            'frequency' => $node->field_medication_frequency->value,
            'startingDate' => $node->field_medication_starting_date->value,
            'status' => $node->field_medication_status->value,
            'statusName' => ($node->field_medication_status->value === '1' ? $this->t('ACTIVE') : $this->t('INACTIVE')),
          ],
        ],
      ],
    ]);
  }

  public static function getRules(): array
  {
    return [
      "field_medication_label" => "required|min:1|max:254",
      "field_medication_frequency" => "required",
      "field_medication_starting_date" => "required",
    ];
  }
}
