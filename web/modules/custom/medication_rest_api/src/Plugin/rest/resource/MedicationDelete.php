<?php

namespace Drupal\medication_rest_api\Plugin\rest\resource;

use Drupal\Core\Annotation\Translation;
use Drupal\node\Entity\Node;
use Drupal\rest\Annotation\RestResource;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Provides Resource for the Medication functionality
 *
 * @RestResource(
 *   id = "medication_delete",
 *   label = @Translation("MEDICATION_DELETE"),
 *   uri_paths = {
 *     "canonical" = "/medication-rest-api/{medication_id}/delete-medication",
 *     "create" = "/medication-rest-api/{medication_id}/delete-medication"
 *   }
 * )
 */
class MedicationDelete extends ResourceBase
{
  const HTTP_OK = 200;
  const HTTP_UNPROC = 422;

  /**
   * Responds to entity POST requests.
   * @return \Drupal\rest\ResourceResponse
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function post(Request $request, $medication_id)
  {
    try {
      $user = \Drupal::currentUser();
      $user_id = $user->id();
    } catch (\Exception $e) {
      $response = [
        'success' => false,
        'status_code' => self::HTTP_UNPROC,
        'message' => $this->t("USER_NOT_FOUND"),
        'data' => [],
      ];

      return new ResourceResponse($response);
    }

    $account = \Drupal\user\Entity\User::load($user_id);
    if ($account->field_used_access_code[0]->target_id === null) {
      $response = [
        'success' => false,
        'status_code' => self::HTTP_OK,
        'message' => $this->t("USER_CANNOT_DELETE_MEDICATIONS"),
        'data' => [
          'content' => $this->t("USER_DOES_NOT_HAVE_A_VALID_ACCESS_CODE"),
        ],
      ];

      return new ResourceResponse($response);
    }

    try {
      $nids = \Drupal::entityTypeManager()
        ->getListBuilder('node')
        ->getStorage()
        ->loadByProperties([
          'type' => "medication",
          'nid' => $medication_id,
          'field_medication_user_ref' => $user_id,
        ]);
      $keys = array_keys($nids);
      $node = null;
      if (isset($keys[0])) {
        $key = $keys[0];
        $node = $nids[$key];
      }

      if ($node === null) {
        $response = [
          'success' => false,
          'status_code' => self::HTTP_OK,
          'message' => $this->t("MEDICATION_DOES_NOT_EXIST_OR_DOES_NOT_BELONG_TO_USER"),
          'data' => [
            'content' => $this->t("MEDICATION_DOES_NOT_EXIST_OR_DOES_NOT_BELONG_TO_USER"),
          ],
        ];

        return new ResourceResponse($response);
      }
    } catch (\Exception $e) {
      $response = [
        'success' => false,
        'status_code' => self::HTTP_UNPROC,
        'message' => $this->t("MEDICATION_CANNOT_BE_DELETED"),
        'data' => [],
      ];

      return new ResourceResponse($response);
    }

    /*try { // CANNOT BE DELETED
      $nids = \Drupal::entityTypeManager()
        ->getListBuilder('node')
        ->getStorage()
        ->loadByProperties([
          'type' => "user_medication_report",
          'field_usermedreport_user_ref' => $user->id(),
          'field_usermedreport_med_ref' => $node->id(),
        ]);

      foreach ($nids as $medication_id => $report) {
        $report_node = \Drupal\node\Entity\Node::load($medication_id);

        $report_node->delete();
      }
    } catch (\Exception $e) {
      $response = [
        'success' => false,
        'status_code' => self::HTTP_UNPROC,
        'message' => $this->t("USER_MEDICATION_REPORTS_CANNOT_BE_DELETED"),
        'data' => [],
      ];

      return new ResourceResponse($response);
    }*/

    try {
      //$node->delete(); //CANNOT BE DELETED
      $node->set('field_medication_status', false);

      $node->save();
    } catch (\Exception $e) {
      $response = [
        'success' => false,
        'status_code' => self::HTTP_UNPROC,
        'message' => $this->t("MEDICATION_CANNOT_BE_DELETED"),
        'data' => [],
      ];

      return new ResourceResponse($response);
    }

    return new ResourceResponse([
      'success' => true,
      'status_code' => self::HTTP_OK,
      'message' => $this->t("MEDICATION_DELETED"),
      'data' => [
        'content' => '',
      ],
    ]);
  }
}
