<?php

namespace Drupal\medication_rest_api\Plugin\rest\resource;

use Drupal\Core\Annotation\Translation;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\rest\Annotation\RestResource;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;

/**
 * Provides Resource for the Medication functionality
 *
 * @RestResource(
 *   id = "medication_get",
 *   label = @Translation("MEDICATION_GET"),
 *   uri_paths = {
 *     "canonical" = "/medication-rest-api/get-medications"
 *   }
 * )
 */
class MedicationGet extends ResourceBase
{
  const HTTP_OK = 200;
  const HTTP_UNPROC = 422;

  /**
   * Responds to entity GET requests.
   * @return \Drupal\rest\ResourceResponse
   */
  public function get()
  {
    try {
      $user = \Drupal::currentUser();
      $userLangId = $user->getPreferredLangcode();

      $nids = \Drupal::entityTypeManager()
        ->getListBuilder('node')
        ->getStorage()
        ->loadByProperties([
          'type' => "medication",
          'field_medication_user_ref' => $user->id(),
        ]);

      $user_medications = [];

      foreach ($nids as $id => $node) {
        if ($node->field_medication_status->value === '1') {
          $user_medications[$id] = [
            'id' => $id,
            'name' => $node->field_medication_label->value,
            'frequency' => $node->field_medication_frequency->value,
            'startingDate' => $node->field_medication_starting_date->value,
            'status' => $node->field_medication_status->value,
            'statusName' => ($node->field_medication_status->value === '1' ? $this->t('ACTIVE') : $this->t('INACTIVE')),
          ];
        }
      }

      if (empty($user_medications)) {
        $payload = [
          'success' => false,
          'status_code' => self::HTTP_OK,
          'message' => $this->t("USER_DOES_NOT_HAVE_ANY_MEDICATION"),
          'data' => [
            'content' => [],
          ],
        ];
      } else {
        $payload = [
          'success' => true,
          'status_code' => self::HTTP_OK,
          'message' => $this->t("SUCCESSFUL"),
          'data' => [
            'content' => $user_medications,
          ],
        ];
      }

      $response = new ResourceResponse($payload);
      $response->getCacheableMetadata()->addCacheContexts(['user']);
      $response->addCacheableDependency(CacheableMetadata::createFromRenderArray([
        '#cache' => [
          'context' => ['user'],
          'tags' => ['user:3', 'languages:' . $userLangId],
          'max-age' => 0,
        ],
      ]));

      return $response;
    } catch (\Exception $e) {
      $response = [
        'success' => false,
        'status_code' => self::HTTP_UNPROC,
        'message' => $e->getMessage(),
        'data' => [],
      ];

      return new ResourceResponse($response);
    }
  }
}
