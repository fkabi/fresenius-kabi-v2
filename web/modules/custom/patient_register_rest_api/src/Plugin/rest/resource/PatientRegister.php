<?php

namespace Drupal\patient_register_rest_api\Plugin\rest\resource;

use Drupal\Core\Annotation\Translation;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Extension\ModuleHandler;
use Drupal\kabi\Validation\ApiValidation;
use Drupal\node\Entity\Node;
use Drupal\rest\Annotation\RestResource;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Provides Resource for the Patient Register functionality
 *
 * @RestResource(
 *   id = "patient_register",
 *   label = @Translation("PATIENT_REGISTER"),
 *   uri_paths = {
 *     "canonical" = "api/json/patient/register",
 *     "create" = "api/json/patient/register"
 *   }
 * )
 */
class PatientRegister extends ResourceBase
{
  const HTTP_OK = 200;
  const HTTP_UNPROC = 422;

  /**
   * Responds to entity POST requests.
   * @return \Drupal\rest\ResourceResponse
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function post(Request $request)
  {
    $content = json_decode($request->getContent(), true);
    $errors = ApiValidation::validate($content, self::getRules(), 'user');

    if ($errors) {
      $response = [
        'success' => false,
        'status_code' => self::HTTP_UNPROC,
        'message' => $this->t("INVALID_DATA"),
        'data' => $errors,
      ];

      return new ResourceResponse($response);
    }

    try {
      $email = $content["email"][0]["value"];
      $email = strtolower($email);
      $password = $content["password"][0]["value"];
      $name = $content["name"][0]["value"];

      $title = isset($content["title"][0]["value"]) ? $content["title"][0]["value"] : null;
      $condition = $content["field_diagnosed_condition_ref"][0]['target_id'];
      $access_code = $content["access_code"][0]["value"];

      $nids = \Drupal::entityTypeManager()
        ->getListBuilder('node')
        ->getStorage()
        ->loadByProperties([
          'type' => 'access_code',
          'field_access_code_code' => $access_code,
        ]);

      $keys = array_keys($nids);
      $key = $keys[0];
      $node = $nids[$key];

      if ($node === null) {
        $response = [
          'success' => false,
          'status_code' => self::HTTP_UNPROC,
          'message' => $this->t("PATIENT_ACCESS_CODE_IS_INVALID"),
          'data' => $errors,
        ];

        return new ResourceResponse($response);
      } else {
        $assigned_to = $node->field_access_code_assigned_to->getValue();
        $is_used = $node->field_access_code_is_used->getValue();
        if (
          !empty($assigned_to) &&
          !empty($is_used) &&
          (
            $assigned_to[0]["target_id"] !== null ||
            $is_used[0]["value"] !== '0'
          )
        ) {
          $response = [
            'success' => false,
            'status_code' => self::HTTP_UNPROC,
            'message' => $this->t("PATIENT_ACCESS_CODE_IS_ALREADY_USED"),
            'data' => $errors,
          ];

          return new ResourceResponse($response);
        }
      }

      if (!user_load_by_mail($email) && !user_load_by_name($name)) {
        $language = \Drupal::languageManager()->getCurrentLanguage()->getId();
        $user = \Drupal\user\Entity\User::create();

        $user->setPassword($password);
        $user->enforceIsNew();
        $user->setEmail($email);
        $user->setUsername($email);

        $user->set("langcode", $language);
        $user->set("preferred_langcode", $language);
        $user->set("preferred_admin_langcode", $language);

        if (isset($title)) {
          $user->set('field_user_title', $title);

        }
        $user->set('field_diagnosed_condition_ref', [$condition]);

        $user->set('field_used_access_code', $node->id());

        $timestamp = \Drupal::time()->getRequestTime();
        $date = DrupalDateTime::createFromTimestamp($timestamp, 'UTC');
        $email_subscription = isset($content["field_user_email_sending"][0]["value"]) ? $content["field_user_email_sending"][0]["value"] : false;

        $user->set('field_user_password_set', $date->format("Y-m-d\TH:i:s"));
        $user->set('field_user_email_sending', $email_subscription);

        $user->activate();

        $result = $user->save();

        $node->set('field_access_code_assigned_to', $user->id());
        $node->set('field_access_code_is_used', true);
        $node->set('field_access_code_assign_date', $date->format("Y-m-d\TH:i:s"));

        $node->save();
      } else {
        $response = [
          'success' => false,
          'status_code' => self::HTTP_UNPROC,
          'message' => $this->t("USER_WITH_EMAIL_ALREADY_EXISTS"),
          'data' => $errors,
        ];

        return new ResourceResponse($response);
      }
    } catch (\Exception $e) {
      $response = [
        'success' => false,
        'status_code' => self::HTTP_UNPROC,
        'message' => $this->t("PATIENT_USER_CANNOT_BE_CREATED"),
        'data' => [
          "exception" => $e->getMessage(),
          "line" => $e->getLine(),
          "code" => $e->getCode(),
          "trace" => $e->getTraceAsString(),
        ],
      ];

      return new ResourceResponse($response);
    }

    try {
      $user_id = $user->id();

      $site_settings = \Drupal::service('site_settings.loader');
      $settings = $site_settings->loadByFieldset('default_patient_symptoms');

      $symptom = [
        'type' => 'symptom',
      ];

      foreach ($settings['default_patient_symptoms'] as $default_symptom) {
        $symptom_name = $default_symptom;

        $symptom["title"] = $symptom_name;
        $symptom["field_symptom_name"] = $symptom_name;
        $symptom["field_symptom_user_ref"] = $user_id;

        $node = Node::create($symptom);
        $node->save();
      }
    } catch (\Exception $e) {
      $response = [
        'success' => false,
        'status_code' => self::HTTP_UNPROC,
        'message' => $this->t("DEFAULT_SYMPTOMS_CANNOT_BE_ASSIGNED_TO_THE_USER"),
        'data' => [
          "exception" => $e->getMessage(),
        ],
      ];

      return new ResourceResponse($response);
    }

    try {
      $user_id = $user->id();

      $site_settings = \Drupal::service('site_settings.loader');
      $settings = $site_settings->loadByFieldset('default_patient_medication');

      $medication = [
        'type' => 'medication',
      ];

      foreach ($settings['default_patient_medication'] as $default_medication) {
        $medication_name = $default_medication["field_default_medication_label"];
        $medication_frequency = $default_medication["field_default_medication_freq"];

        $medication["title"] = $medication_name;
        $medication["field_medication_label"] = $medication_name;
        $medication["field_medication_frequency"] = $medication_frequency;
        $medication["field_medication_starting_date"] = date('Y-m-d\TH:i:s', time());
        $medication["field_medication_status"] = TRUE;
        $medication["field_medication_user_ref"] = $user_id;

        $node = Node::create($medication);
        $node->save();
      }
    } catch (\Exception $e) {
      $response = [
        'success' => false,
        'status_code' => self::HTTP_UNPROC,
        'message' => $this->t("DEFAULT_SYMPTOMS_CANNOT_BE_ASSIGNED_TO_THE_USER"),
        'data' => [
          "exception" => $e->getMessage(),
        ],
      ];

      return new ResourceResponse($response);
    }

    _user_mail_notify('register_no_approval_required',
      $user,
      \Drupal::languageManager()->getCurrentLanguage()->getId()
    );

    return new ResourceResponse([
      'success' => true,
      'status_code' => self::HTTP_OK,
      'message' => $this->t("PATIENT_USER_CREATED"),
      'data' => [],
    ]);
  }

  public static function getRules(): array
  {
    return [
      "email" => "required|max:60|pattern:/^([a-z0-9_\-\.]+)@([a-z0-9_\-\.]+)\.([a-z]{2,5})$/",
      "password" => "required|min:10|max:60|pattern:/^.*(?=\S{10,})(?=\S*[a-z])(?=\S*[A-Z])(?=\S*[\d])[^\"'\s]*$/",
      "confirm_password" => "required|min:10|max:60|pattern:/^.*(?=\S{10,})(?=\S*[a-z])(?=\S*[A-Z])(?=\S*[\d])[^\"'\s]*$/",
      "name" => "required|min:2|max:60",
      "access_code" => "required|min:1|max:32",
    ];
  }
}
