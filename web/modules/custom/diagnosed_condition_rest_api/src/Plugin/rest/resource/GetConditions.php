<?php

namespace Drupal\diagnosed_condition_rest_api\Plugin\rest\resource;

use Drupal\Core\Annotation\Translation;
use Drupal\node\Entity\Node;
use Drupal\rest\Annotation\RestResource;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;

/**
 * Provides Resource for the Diagnosed Condition functionality
 *
 * @RestResource(
 *   id = "diagnosed_condition",
 *   label = @Translation("GET_ALL_CONDITIONS"),
 *   uri_paths = {
 *     "canonical" = "api/json/get-conditions"
 *   }
 * )
 */
class GetConditions extends ResourceBase
{
  const HTTP_OK = 200;
  const HTTP_UNPROC = 422;

  /**
   * Responds to entity GET requests.
   * @return \Drupal\rest\ResourceResponse
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function get()
  {
    $nodes = \Drupal::entityTypeManager()
      ->getListBuilder('node')
      ->getStorage()
      ->loadByProperties([
        'type' => 'diagnosed_condition',
      ]);

    $arr_conditions = [];

    foreach ($nodes as $id => $item) {
      $arr_conditions[$id] = [
        'id' => $id,
        'label' => $item->field_diagnosed_condition_label[0]->value,
        'condition' => $item->field_diagnosed_condition_cond[0]->value,
      ];
    }

    return new ResourceResponse([
      'success' => true,
      'status_code' => self::HTTP_OK,
      'message' => $this->t("SUCCESS"),
      'data' => [
        'conditions' => $arr_conditions,
      ],
    ]);
  }
}
