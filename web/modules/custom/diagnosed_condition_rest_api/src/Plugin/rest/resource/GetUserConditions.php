<?php

namespace Drupal\diagnosed_condition_rest_api\Plugin\rest\resource;

use Drupal\Core\Annotation\Translation;
use Drupal\node\Entity\Node;
use Drupal\rest\Annotation\RestResource;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;

/**
 * Provides Resource for the Diagnosed Condition functionality
 *
 * @RestResource(
 *   id = "user_diagnosed_condition",
 *   label = @Translation("GET_USER_CONDITIONS"),
 *   uri_paths = {
 *     "canonical" = "api/json/get-user-conditions/{id}"
 *   }
 * )
 */
class GetUserConditions extends ResourceBase
{
  const HTTP_OK = 200;
  const HTTP_UNPROC = 422;

  /**
   * Responds to entity GET requests.
   * @param $id
   * @return \Drupal\rest\ResourceResponse
   */
  public function get($id)
  {
    $account = \Drupal\user\Entity\User::load($id);

    if (!$account) {
      return new ResourceResponse([
        'success' => false,
        'status_code' => self::HTTP_UNPROC,
        'message' => $this->t("USER_DOES_NOT_EXIST"),
        'data' => [],
      ]);
    }

    $condition_id = [];

    foreach ($account->get('field_diagnosed_condition_ref') as $condition) {
      if (method_exists($condition, 'getvalue')) {
        $condition_id[] = $condition->getValue()['target_id'];
      }
    }

    if (!empty($condition_id)) {
      $nodes = \Drupal::entityTypeManager()
        ->getListBuilder('node')
        ->getStorage()
        ->loadByProperties([
          'type' => 'diagnosed_condition',
          'nid' => $condition_id,
        ]);

      $arr_conditions = [];

      foreach ($nodes as $id => $item) {
        $arr_conditions[$id] = [
          'id' => $id,
          'label' => $item->field_diagnosed_condition_label[0]->value,
          'condition' => $item->field_diagnosed_condition_cond[0]->value,
        ];
      }

      return new ResourceResponse([
        'success' => true,
        'status_code' => self::HTTP_OK,
        'message' => $this->t("SUCCESS"),
        'data' => [
          'conditions' => $arr_conditions,
        ],
      ]);
    }

    return new ResourceResponse([
      'success' => false,
      'status_code' => self::HTTP_OK,
      'message' => $this->t("USER_DOES_NOT_HAVE_ANY_DIAGNOSED_CONDITION"),
      'data' => [],
    ]);
  }
}
