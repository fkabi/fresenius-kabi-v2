<?php

namespace Drupal\user_medication_report_rest_api\Plugin\rest\resource;

use Drupal\Core\Annotation\Translation;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\rest\Annotation\RestResource;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Provides Resource for the User Medication Report functionality
 *
 * @RestResource(
 *   id = "user_medication_report_get",
 *   label = @Translation("USER_MEDICATION_REPORT_GET"),
 *   uri_paths = {
 *     "canonical" = "/user-medication-report-rest-api/get-user-medication-report",
 *   }
 * )
 */
class UserMedicationReportGet extends ResourceBase
{
  const HTTP_OK = 200;
  const HTTP_UNPROC = 422;

  /**
   * Responds to entity GET requests.
   * @return \Drupal\rest\ResourceResponse
   */
  public function get(Request $request)
  {
    $interval = $request->query->get('interval');

    try {
      $user = \Drupal::currentUser();
      $userLangId = $user->getPreferredLangcode();

      $result = \Drupal::entityQuery('node')
        ->condition('type', 'user_medication_report')
        ->condition('field_usermedreport_user_ref', $user->id());

      if ($interval !== null) {
        $start = new \DateTime('now');
        $end = new \DateTime('now');
        $end->modify('+1 day');
        $end->setTime(0, 0, 0);
        $end = DrupalDateTime::createFromDateTime($end);

        switch ($interval) {
          case 'day':
            $start->setTime(0, 0, 0);
            $start = DrupalDateTime::createFromDateTime($start);
            break;
          case 'week':
            $start->modify('-6 days');
            $start->setTime(0, 0, 0);
            $start = DrupalDateTime::createFromDateTime($start);

            break;
          case 'month':
          default:
            $start->modify('-30 days');
            $start->setTime(0, 0, 0);
            $start = DrupalDateTime::createFromDateTime($start);

            break;
        }

        $result->condition('field_usermedreport_date', $start, '>=');
        $result->condition('field_usermedreport_date', $end, '<=');
      }

      $result = $result->execute();
      $nids = \Drupal::entityTypeManager()->getStorage('node')->loadMultiple($result);

      $user_medication_report = [];

      foreach ($nids as $id => $node) {
        $medication = \Drupal\node\Entity\Node::load($node->field_usermedreport_med_ref[0]->target_id);

        $date = date('Y-m-d', strtotime($node->field_usermedreport_date[0]->value));

        $user_medication_report[$date][] = [
          'id' => $node->id(),
          'medication_name' => $medication->field_medication_label[0]->value,
          'medication_id' => $node->field_usermedreport_med_ref[0]->target_id,
          'date' => $date,
        ];
      }

      if (empty($user_medication_report)) {
        $payload = [
          'success' => false,
          'status_code' => self::HTTP_OK,
          'message' => $this->t("USER_DOES_NOT_HAVE_ANY_USER_MEDICATION_REPORT"),
          'data' => [
            'content' => [],
          ],
        ];
      } else {
        $payload = [
          'success' => true,
          'status_code' => self::HTTP_OK,
          'message' => $this->t("SUCCESSFUL"),
          'data' => [
            'content' => $user_medication_report,
          ],
        ];
      }

      $response = new ResourceResponse($payload);
      $response->getCacheableMetadata()->addCacheContexts(['user']);
      $response->addCacheableDependency(CacheableMetadata::createFromRenderArray([
        '#cache' => [
          'context' => ['user'],
          'tags' => ['user:3', 'languages:' . $userLangId],
          'max-age' => 0,
        ],
      ]));

      return $response;
    } catch (\Exception $e) {
      $response = [
        'success' => false,
        'status_code' => self::HTTP_UNPROC,
        'message' => $e->getMessage(),
        'data' => [
          'line' => $e->getLine()
        ],
      ];

      return new ResourceResponse($response);
    }
  }
}
