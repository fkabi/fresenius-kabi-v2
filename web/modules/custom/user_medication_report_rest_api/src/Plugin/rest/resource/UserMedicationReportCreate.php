<?php

namespace Drupal\user_medication_report_rest_api\Plugin\rest\resource;

use Drupal\Core\Annotation\Translation;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\kabi\Validation\ApiValidation;
use Drupal\node\Entity\Node;
use Drupal\rest\Annotation\RestResource;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Provides Resource for the User Medication Report functionality
 *
 * @RestResource(
 *   id = "user_medication_report_create",
 *   label = @Translation("USER_MEDICATION_REPORT_CREATE"),
 *   uri_paths = {
 *     "canonical" = "/user-medication-report-rest-api/create-user-medication-report",
 *     "create" = "/user-medication-report-rest-api/create-user-medication-report"
 *   }
 * )
 */
class UserMedicationReportCreate extends ResourceBase
{
  const HTTP_OK = 200;
  const HTTP_UNPROC = 422;

  /**
   * Responds to entity POST requests.
   * @return \Drupal\rest\ResourceResponse
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function post(Request $request)
  {
    $content = json_decode($request->getContent(), true);

    if ($content === null) {
      $response = [
        'success' => false,
        'status_code' => self::HTTP_UNPROC,
        'message' => $this->t("INVALID_DATA"),
        'data' => [
          'error_title' => $this->t('EMPTY_REQUEST'),
        ],
      ];

      return new ResourceResponse($response);
    }

    $errors = ApiValidation::validate($content, self::getRules(), 'user_medication_report');

    if ($errors) {
      $response = [
        'success' => false,
        'status_code' => self::HTTP_UNPROC,
        'message' => $this->t("INVALID_DATA"),
        'data' => $errors,
      ];

      return new ResourceResponse($response);
    }

    try {
      $user = \Drupal::currentUser();
      $user_id = $user->id();
    } catch (\Exception $e) {
      $response = [
        'success' => false,
        'status_code' => self::HTTP_UNPROC,
        'message' => $this->t("USER_NOT_FOUND"),
        'data' => [],
      ];

      return new ResourceResponse($response);
    }

    $account = \Drupal\user\Entity\User::load($user_id);

    if ($account->field_used_access_code[0]->target_id === null) {
      $response = [
        'success' => false,
        'status_code' => self::HTTP_OK,
        'message' => $this->t("USER_CANNOT_CREATE_USER_MEDICATION_REPORT"),
        'data' => [
          'content' => $this->t("USER_DOES_NOT_HAVE_A_VALID_ACCESS_CODE"),
        ],
      ];

      return new ResourceResponse($response);
    }

    try {

      $medication_id = $content['field_usermedreport_med_ref'][0]['target_id'];
      $result = \Drupal::entityQuery('node')
        ->condition('type', 'medication')
        ->condition('nid', $medication_id)
        ->condition('field_medication_user_ref', $user_id)
        ->execute();
      $nids = \Drupal::entityTypeManager()->getStorage('node')->loadMultiple($result);

      if (empty($nids)) {
        $response = [
          'success' => false,
          'status_code' => self::HTTP_OK,
          'message' => $this->t("MEDICATION_NOT_EXIST"),
          'data' => [
            'content' => $this->t("MEDICATION_NOT_EXIST"),
          ],
        ];

        return new ResourceResponse($response);
      }

      $user_medication_report = [
        'type' => 'user_medication_report',
      ];

      $keys = array_keys($nids);
      $node = $nids[$keys[0]];

      $medication_name = $node->field_medication_label[0]->value;
      $time = time();
      $date = date('Y-m-d\TH:i:s', $time);

      $user_medication_report["title"] = $medication_name . "_" . $user_id . "_" . date('Y-m-d_H:i:s', $time);
      $user_medication_report["field_usermedreport_date"] = $date;
      $user_medication_report["field_usermedreport_med_ref"] = $node->id();
      $user_medication_report["field_usermedreport_user_ref"] = $user_id;

      $node = Node::create($user_medication_report);
      $node->save();
    } catch (\Exception $e) {
      $response = [
        'success' => false,
        'status_code' => self::HTTP_UNPROC,
        'message' => $this->t("USER_MEDICATION_REPORT_CANNOT_BE_CREATED"),
        'data' => [
          'line' => $e->getLine(),
          'trace' => $e->getTraceAsString(),
        ],
      ];

      return new ResourceResponse($response);
    }

    return new ResourceResponse([
      'success' => true,
      'status_code' => self::HTTP_OK,
      'message' => $this->t("USER_MEDICATION_REPORT_CREATED"),
      'data' => [
        'content' => [
          $node->id() => [
            'id' => $node->id(),
            'medication_name' => $medication_name,
            'medication_id' => $node->field_usermedreport_med_ref[0]->target_id,
            'date' => $date,
          ],
        ],
      ],
    ]);
  }

  public static function getRules(): array
  {
    return [
      "field_usermedreport_med_ref" => "required",
    ];
  }
}
