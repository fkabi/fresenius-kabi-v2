<?php

namespace Drupal\password_renewal_rest_api\Plugin\rest\resource;

use Drupal\Core\Annotation\Translation;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\rest\Annotation\RestResource;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;

/**
 * Provides Resource for the Password renewal functionality
 *
 * @RestResource(
 *   id = "password_renewal_get",
 *   label = @Translation("PASSWORD_RENEWAL_GET"),
 *   uri_paths = {
 *     "canonical" = "/user/password-renewal"
 *   }
 * )
 */
class PasswordRenewalGet extends ResourceBase
{
  const HTTP_OK = 200;
  const HTTP_UNPROC = 422;

  /**
   * Responds to entity GET requests.
   * @return \Drupal\rest\ResourceResponse
   */
  public function get()
  {
    $payload = [
      'success' => false,
      'status_code' => self::HTTP_OK,
      'message' => $this->t("CANNOT_LOGIN"),
      'data' => [
        'content' => $this->t("PASSWORD_EXPIRED"),
        'can_login' => false,
      ],
    ];

    return new ResourceResponse($payload);
  }
}
