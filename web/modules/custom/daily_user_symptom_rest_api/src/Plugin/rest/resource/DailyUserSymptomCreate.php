<?php

namespace Drupal\daily_user_symptom_rest_api\Plugin\rest\resource;

use Drupal\Core\Annotation\Translation;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\kabi\Validation\ApiValidation;
use Drupal\node\Entity\Node;
use Drupal\rest\Annotation\RestResource;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Provides Resource for the Daily User Symptom functionality
 *
 * @RestResource(
 *   id = "daily_user_symptom_create",
 *   label = @Translation("DAILY_USER_SYMPTOM_CREATE"),
 *   uri_paths = {
 *     "canonical" = "/daily-user-symptom-rest-api/create-daily-user-symptoms",
 *     "create" = "/daily-user-symptom-rest-api/create-daily-user-symptoms"
 *   }
 * )
 */
class DailyUserSymptomCreate extends ResourceBase
{
  const HTTP_OK = 200;
  const HTTP_UNPROC = 422;

  /**
   * Responds to entity POST requests.
   * @return \Drupal\rest\ResourceResponse
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function post(Request $request)
  {
    $content = json_decode($request->getContent(), true);

    if ($content === null) {
      $response = [
        'success' => false,
        'status_code' => self::HTTP_UNPROC,
        'message' => $this->t("INVALID_DATA"),
        'data' => [
          'error_title' => $this->t('EMPTY_REQUEST'),
        ],
      ];

      return new ResourceResponse($response);
    }

    try {
      $user = \Drupal::currentUser();
      $user_id = $user->id();
    } catch (\Exception $e) {
      $response = [
        'success' => false,
        'status_code' => self::HTTP_UNPROC,
        'message' => $this->t("USER_NOT_FOUND"),
        'data' => [],
      ];

      return new ResourceResponse($response);
    }

    $account = \Drupal\user\Entity\User::load($user_id);
    if ($account->field_used_access_code[0]->target_id === null) {
      $response = [
        'success' => false,
        'status_code' => self::HTTP_OK,
        'message' => $this->t("USER_CANNOT_CREATE_DAILY_USER_SYMPTOM"),
        'data' => [
          'content' => $this->t("USER_DOES_NOT_HAVE_A_VALID_ACCESS_CODE"),
        ],
      ];

      return new ResourceResponse($response);
    }

    if (isset($content['field_daily_symptom_symptom_ref'])) {
      $errors = ApiValidation::validate($content, self::getRules(), 'user_daily_symptom');

      if ($errors) {
        $response = [
          'success' => false,
          'status_code' => self::HTTP_UNPROC,
          'message' => $this->t("INVALID_DATA"),
          'data' => $errors,
        ];

        return new ResourceResponse($response);
      }

      try {
        $symptom_id = $content['field_daily_symptom_symptom_ref'][0]['target_id'];
        $result = \Drupal::entityQuery('node')
          ->condition('type', 'symptom')
          ->condition('nid', $symptom_id)
          ->condition('field_symptom_user_ref', $user_id)
          ->execute();
        $symptom_ids = \Drupal::entityTypeManager()->getStorage('node')->loadMultiple($result);

        if (empty($symptom_ids)) {
          $response = [
            'success' => false,
            'status_code' => self::HTTP_OK,
            'message' => $this->t("SYMPTOM_NOT_EXIST"),
            'data' => [
              'content' => $this->t("SYMPTOM_NOT_EXIST"),
            ],
          ];

          return new ResourceResponse($response);
        }
      } catch (\Exception $e) {
        $response = [
          'success' => false,
          'status_code' => self::HTTP_UNPROC,
          'message' => $this->t("USER_DAILY_SYMPTOM_CANNOT_BE_CREATED"),
          'data' => [
            'line' => $e->getLine(),
            'trace' => $e->getTraceAsString(),
          ],
        ];

        return new ResourceResponse($response);
      }

      try {
        $start = new \DateTime('now');
        $start->setTime(0, 0, 0);
        $start = DrupalDateTime::createFromDateTime($start);

        $end = new \DateTime('now');
        $end->modify('+1 day');
        $end->setTime(0, 0, 0);
        $end = DrupalDateTime::createFromDateTime($end);

        $result = \Drupal::entityQuery('node')
          ->condition('type', 'user_daily_symptom')
          ->condition('field_daily_symptom_user_ref', $user->id())
          ->condition('field_daily_symptom_symptom_ref', $symptom_id)
          ->condition('field_daily_symptom_date', $start, '>=')
          ->condition('field_daily_symptom_date', $end, '<=')
          ->execute();
        $nids = \Drupal::entityTypeManager()->getStorage('node')->loadMultiple($result);

        if (!empty($nids)) {
          $response = [
            'success' => false,
            'status_code' => self::HTTP_OK,
            'message' => $this->t("USER_CANNOT_CREATE_DAILY_USER_SYMPTOM"),
            'data' => [
              'content' => $this->t("USER_ALREADY_HAS_A_DAILY_USER_SYMPTOM_FOR_THE_CURRENT_DAY"),
            ],
          ];

          return new ResourceResponse($response);
        }
      } catch (\Exception $e) {
        $response = [
          'success' => false,
          'status_code' => self::HTTP_UNPROC,
          'message' => $this->t("USER_DAILY_SYMPTOM_CANNOT_BE_CREATED"),
          'data' => [
            'line' => $e->getLine(),
            'trace' => $e->getTraceAsString(),
          ],
        ];

        return new ResourceResponse($response);
      }

      try {
        $user_daily_symptom = [
          'type' => 'user_daily_symptom',
        ];

        $keys = array_keys($symptom_ids);
        $node = $symptom_ids[$keys[0]];

        $symptom_name = $node->field_symptom_name[0]->value;
        $value = (int)$content["field_daily_symptom_value"][0]["value"];

        if ($value < 1 || $value > 5 || !is_int($value)) {
          $response = [
            'success' => false,
            'status_code' => self::HTTP_UNPROC,
            'message' => $this->t("DAILY_SYMPTOM_VALUE_IS_OUT_OF_BOUNDS"),
            'data' => [
              'message' => $this->t("DAILY_SYMPTOM_VALUE_IS_OUT_OF_BOUNDS"),
            ],
          ];

          return new ResourceResponse($response);
        }

        $date = date('Y-m-d\TH:i:s', time());

        $user_daily_symptom["title"] = $symptom_name . "_" . $value;
        $user_daily_symptom["field_daily_symptom_date"] = $date;
        $user_daily_symptom["field_daily_symptom_value"] = $value;
        $user_daily_symptom["field_daily_symptom_symptom_ref"] = $node->id();
        $user_daily_symptom["field_daily_symptom_user_ref"] = $user_id;

        $node = Node::create($user_daily_symptom);
        $node->save();
      } catch (\Exception $e) {
        $response = [
          'success' => false,
          'status_code' => self::HTTP_UNPROC,
          'message' => $this->t("USER_DAILY_SYMPTOM_CANNOT_BE_CREATED"),
          'data' => [
            'line' => $e->getLine(),
            'trace' => $e->getTraceAsString(),
          ],
        ];

        return new ResourceResponse($response);
      }

      return new ResourceResponse([
        'success' => true,
        'status_code' => self::HTTP_OK,
        'message' => $this->t("USER_DAILY_SYMPTOM_CREATED"),
        'data' => [
          'content' => [
            $node->id() => [
              'id' => $node->id(),
              'symptom_name' => $symptom_name,
              'symptom_id' => $node->field_daily_symptom_symptom_ref[0]->target_id,
              'date' => $date,
              'value' => $node->field_daily_symptom_value[0]->value,
            ],
          ],
        ],
      ]);
    } else {
      $issues = [];
      $contentArray = $content;
      foreach ($contentArray as $content) {
        $errors = ApiValidation::validate($content, self::getRules(), 'user_daily_symptom');

        if ($errors) {
          $response = [
            'success' => false,
            'status_code' => self::HTTP_UNPROC,
            'message' => $this->t("INVALID_DATA"),
            'data' => $errors,
          ];

          return new ResourceResponse($response);
        }
      }

      $keys_to_bounce = [];
      foreach ($contentArray as $key => $content) {
        try {
          $symptom_id = $content['field_daily_symptom_symptom_ref'][0]['target_id'];
          $result = \Drupal::entityQuery('node')
            ->condition('type', 'symptom')
            ->condition('nid', $symptom_id)
            ->condition('field_symptom_user_ref', $user_id)
            ->execute();
          $symptom_ids = \Drupal::entityTypeManager()->getStorage('node')->loadMultiple($result);

          if (empty($symptom_ids)) {
            $issues[] = [
              'message' => $this->t("SYMPTOM_NOT_EXIST"),
              'data' => [
                'content' => $this->t("SYMPTOM_NOT_EXIST"),
                'id' => $symptom_id,
              ],
            ];

            $keys_to_bounce[] = $key;
          }
        } catch (\Exception $e) {
          $response = [
            'success' => false,
            'status_code' => self::HTTP_UNPROC,
            'message' => $this->t("USER_DAILY_SYMPTOM_CANNOT_BE_CREATED"),
            'data' => [
              'line' => $e->getLine(),
              'trace' => $e->getTraceAsString(),
            ],
          ];

          return new ResourceResponse($response);
        }
      }

      foreach ($keys_to_bounce as $key) {
        unset($contentArray[$key]);
      }
      $keys_to_bounce = [];

      foreach ($contentArray as $key => $content) {
        try {
          $symptom_id = $content['field_daily_symptom_symptom_ref'][0]['target_id'];

          $start = new \DateTime('now');
          $start->setTime(0, 0, 0);
          $start = DrupalDateTime::createFromDateTime($start);

          $end = new \DateTime('now');
          $end->modify('+1 day');
          $end->setTime(0, 0, 0);
          $end = DrupalDateTime::createFromDateTime($end);

          $result = \Drupal::entityQuery('node')
            ->condition('type', 'user_daily_symptom')
            ->condition('field_daily_symptom_user_ref', $user->id())
            ->condition('field_daily_symptom_symptom_ref', $symptom_id)
            ->condition('field_daily_symptom_date', $start, '>=')
            ->condition('field_daily_symptom_date', $end, '<=')
            ->execute();
          $nids = \Drupal::entityTypeManager()->getStorage('node')->loadMultiple($result);

          if (!empty($nids)) {
            $issues[] = [
              'message' => $this->t("USER_CANNOT_CREATE_DAILY_USER_SYMPTOM"),
              'data' => [
                'content' => $this->t("USER_ALREADY_HAS_A_DAILY_USER_SYMPTOM_FOR_THE_CURRENT_DAY"),
                'id' => $symptom_id,
              ],
            ];

            $keys_to_bounce[] = $key;
          }
        } catch (\Exception $e) {
          $response = [
            'success' => false,
            'status_code' => self::HTTP_UNPROC,
            'message' => $this->t("USER_DAILY_SYMPTOM_CANNOT_BE_CREATED"),
            'data' => [
              'line' => $e->getLine(),
              'trace' => $e->getTraceAsString(),
            ],
          ];

          return new ResourceResponse($response);
        }
      }

      foreach ($keys_to_bounce as $key) {
        unset($contentArray[$key]);
      }
      unset($keys_to_bounce);

      $result_response = [];

      foreach ($contentArray as $content) {
        try {
          $user_daily_symptom = [
            'type' => 'user_daily_symptom',
          ];

          $symptom_id = $content['field_daily_symptom_symptom_ref'][0]['target_id'];
          $result = \Drupal::entityQuery('node')
            ->condition('type', 'symptom')
            ->condition('nid', $symptom_id)
            ->condition('field_symptom_user_ref', $user_id)
            ->execute();
          $symptom_ids = \Drupal::entityTypeManager()->getStorage('node')->loadMultiple($result);

          $keys = array_keys($symptom_ids);
          $node = $symptom_ids[$keys[0]];

          $symptom_name = $node->field_symptom_name[0]->value;
          $value = (int)$content["field_daily_symptom_value"][0]["value"];

          if ($value < 1 || $value > 5 || !is_int($value)) {
            $issues[] = [
              'message' => $this->t("DAILY_SYMPTOM_VALUE_IS_OUT_OF_BOUNDS"),
              'data' => [
                'content' => $this->t("DAILY_SYMPTOM_VALUE_IS_OUT_OF_BOUNDS"),
                'id' => $symptom_id,
              ],
            ];

            continue;
          }

          $date = date('Y-m-d\TH:i:s', time());

          $user_daily_symptom["title"] = $symptom_name . "_" . $value;
          $user_daily_symptom["field_daily_symptom_date"] = $date;
          $user_daily_symptom["field_daily_symptom_value"] = $value;
          $user_daily_symptom["field_daily_symptom_symptom_ref"] = $node->id();
          $user_daily_symptom["field_daily_symptom_user_ref"] = $user_id;

          $node = Node::create($user_daily_symptom);
          $node->save();

          $result_response[$node->id()] = [
            'id' => $node->id(),
            'symptom_name' => $symptom_name,
            'symptom_id' => $node->field_daily_symptom_symptom_ref[0]->target_id,
            'date' => $date,
            'value' => $node->field_daily_symptom_value[0]->value,
          ];
        } catch (\Exception $e) {
          $response = [
            'success' => false,
            'status_code' => self::HTTP_UNPROC,
            'message' => $this->t("USER_DAILY_SYMPTOM_CANNOT_BE_CREATED"),
            'data' => [
              'line' => $e->getLine(),
              'trace' => $e->getTraceAsString(),
            ],
          ];

          return new ResourceResponse($response);
        }
      }

      return new ResourceResponse([
        'success' => true,
        'status_code' => self::HTTP_OK,
        'message' => $this->t("USER_DAILY_SYMPTOM_CREATED"),
        'data' => [
          'content' => $result_response,
          'error' => $issues,
        ],
      ]);
    }
  }

  public static function getRules(): array
  {
    return [
      "field_daily_symptom_symptom_ref" => "required",
      "field_daily_symptom_value" => "required",
    ];
  }
}
