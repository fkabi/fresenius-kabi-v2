<?php

namespace Drupal\daily_user_symptom_rest_api\Plugin\rest\resource;

use Drupal\Core\Annotation\Translation;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\rest\Annotation\RestResource;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Provides Resource for the Daily User Symptom functionality
 *
 * @RestResource(
 *   id = "daily_user_symptom_get",
 *   label = @Translation("DAILY_USER_SYMPTOM_GET"),
 *   uri_paths = {
 *     "canonical" = "/daily-user-symptom-rest-api/get-daily-user-symptoms"
 *   }
 * )
 */
class DailyUserSymptomGet extends ResourceBase
{
  const HTTP_OK = 200;
  const HTTP_UNPROC = 422;

  /**
   * Responds to entity GET requests.
   * @return \Drupal\rest\ResourceResponse
   */
  public function get(Request $request)
  {
    $interval = $request->query->get('interval');

    try {
      $user = \Drupal::currentUser();
      $userLangId = $user->getPreferredLangcode();

      $result = \Drupal::entityQuery('node')
        ->condition('type', 'user_daily_symptom')
        ->condition('field_daily_symptom_user_ref', $user->id());

      if ($interval !== null) {
        $start = new \DateTime('now');
        $end = new \DateTime('now');
        $end->modify('+1 day');
        $end->setTime(0, 0, 0);
        $end = DrupalDateTime::createFromDateTime($end);

        switch ($interval) {
          case 'day':
            $start->setTime(0, 0, 0);
            $start = DrupalDateTime::createFromDateTime($start);
            break;
          case 'week':
            $start->modify('-6 days');
            $start->setTime(0, 0, 0);
            $start = DrupalDateTime::createFromDateTime($start);

            break;
          case 'month':
          default:
            $start->modify('-30 days');
            $start->setTime(0, 0, 0);
            $start = DrupalDateTime::createFromDateTime($start);

            break;
        }

        $result->condition('field_daily_symptom_date', $start, '>=');
        $result->condition('field_daily_symptom_date', $end, '<=');
      }

      $result = $result->execute();
      $nids = \Drupal::entityTypeManager()->getStorage('node')->loadMultiple($result);

      $user_daily_symptoms = [];

      foreach ($nids as $id => $node) {
        $symptom = \Drupal\node\Entity\Node::load($node->field_daily_symptom_symptom_ref[0]->target_id);

        $date = date('Y-m-d', strtotime($node->field_daily_symptom_date[0]->value));

        $user_daily_symptoms[$date][] = [
          'id' => $node->id(),
          'symptom_name' => $symptom->field_symptom_name[0]->value,
          'symptom_id' => $node->field_daily_symptom_symptom_ref[0]->target_id,
          'date' => $date,
          'value' => $node->field_daily_symptom_value[0]->value,
        ];
      }

      if (empty($user_daily_symptoms)) {
        $payload = [
          'success' => false,
          'status_code' => self::HTTP_OK,
          'message' => $this->t("USER_DOES_NOT_HAVE_ANY_DAILY_SYMPTOM_REGISTRATION"),
          'data' => [
            'content' => [],
          ],
        ];
      } else {
        $payload = [
          'success' => true,
          'status_code' => self::HTTP_OK,
          'message' => $this->t("SUCCESSFUL"),
          'data' => [
            'content' => $user_daily_symptoms,
          ],
        ];
      }

      $response = new ResourceResponse($payload);
      $response->getCacheableMetadata()->addCacheContexts(['user']);
      $response->addCacheableDependency(CacheableMetadata::createFromRenderArray([
        '#cache' => [
          'context' => ['user'],
          'tags' => ['user:3', 'languages:' . $userLangId],
          'max-age' => 0,
        ],
      ]));

      return $response;
    } catch (\Exception $e) {
      $response = [
        'success' => false,
        'status_code' => self::HTTP_UNPROC,
        'message' => $e->getMessage(),
        'data' => [
          'line' => $e->getLine()
        ],
      ];

      return new ResourceResponse($response);
    }
  }
}
