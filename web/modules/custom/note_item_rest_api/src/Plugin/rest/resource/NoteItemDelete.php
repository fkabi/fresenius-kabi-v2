<?php

namespace Drupal\note_item_rest_api\Plugin\rest\resource;

use Drupal\Core\Annotation\Translation;
use Drupal\node\Entity\Node;
use Drupal\rest\Annotation\RestResource;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Provides Resource for the Note Items functionality
 *
 * @RestResource(
 *   id = "note_item_delete",
 *   label = @Translation("NOTE_ITEM_DELETE"),
 *   uri_paths = {
 *     "canonical" = "/note-item-rest-api/{note_item_id}/delete-note-item",
 *     "create" = "/note-item-rest-api/{note_item_id}/delete-note-item"
 *   }
 * )
 */
class NoteItemDelete extends ResourceBase
{
  const HTTP_OK = 200;
  const HTTP_UNPROC = 422;

  /**
   * Responds to entity POST requests.
   * @param Request $request
   * @param $note_item_id
   * @return \Drupal\rest\ResourceResponse
   */
  public function post(Request $request, $note_item_id)
  {
    try {
      $user = \Drupal::currentUser();
      $user_id = $user->id();
    } catch (\Exception $e) {
      $response = [
        'success' => false,
        'status_code' => self::HTTP_UNPROC,
        'message' => $this->t("USER_NOT_FOUND"),
        'data' => [],
      ];

      return new ResourceResponse($response);
    }

    $account = \Drupal\user\Entity\User::load($user_id);
    if ($account->field_used_access_code[0]->target_id === null) {
      $response = [
        'success' => false,
        'status_code' => self::HTTP_OK,
        'message' => $this->t("USER_CANNOT_DELETE_NOTES"),
        'data' => [
          'content' => $this->t("USER_DOES_NOT_HAVE_A_VALID_ACCESS_CODE"),
        ],
      ];

      return new ResourceResponse($response);
    }

    try {
      $nids = \Drupal::entityTypeManager()
        ->getListBuilder('node')
        ->getStorage()
        ->loadByProperties([
          'type' => "note_item",
          'nid' => $note_item_id,
        ]);

      $keys = array_keys($nids);
      $node = null;

      if (isset($keys[0])) {
        $key = $keys[0];
        $node = $nids[$key];
      }

      if ($node === null) {
        $response = [
          'success' => false,
          'status_code' => self::HTTP_OK,
          'message' => $this->t("NOTE_ITEM_DOES_NOT_EXIST"),
          'data' => [
            'content' => $this->t("NOTE_ITEM_DOES_NOT_EXIST"),
          ],
        ];

        return new ResourceResponse($response);
      }

      $note_id = $node->field_note_item_notes_ref->target_id;

      $nids = \Drupal::entityTypeManager()
        ->getListBuilder('node')
        ->getStorage()
        ->loadByProperties([
          'type' => "notes",
          'nid' => $note_id,
          'field_notes_user_ref' => $user_id,
        ]);

      if (empty($nids)) {
        $response = [
          'success' => false,
          'status_code' => self::HTTP_OK,
          'message' => $this->t("NOTE_DOES_NOT_EXIST_OR_DOES_NOT_BELONG_TO_USER"),
          'data' => [
            'content' => $this->t("NOTE_DOES_NOT_EXIST_OR_DOES_NOT_BELONG_TO_USER"),
          ],
        ];

        return new ResourceResponse($response);
      }

      $node->delete();
    } catch (\Exception $e) {
      $response = [
        'success' => false,
        'status_code' => self::HTTP_UNPROC,
        'message' => $this->t("NOTE_CANNOT_BE_DELETED"),
        'data' => [],
      ];

      return new ResourceResponse($response);
    }

    return new ResourceResponse([
      'success' => true,
      'status_code' => self::HTTP_OK,
      'message' => $this->t("NOTE_DELETED"),
      'data' => [
        'content' => '',
      ],
    ]);
  }
}
