<?php

namespace Drupal\note_item_rest_api\Plugin\rest\resource;

use Drupal\Core\Annotation\Translation;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\kabi\Validation\ApiValidation;
use Drupal\node\Entity\Node;
use Drupal\rest\Annotation\RestResource;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Provides Resource for the Note Items functionality
 *
 * @RestResource(
 *   id = "note_item_create",
 *   label = @Translation("NOTE_ITEM_CREATE"),
 *   uri_paths = {
 *     "canonical" = "/note-item-rest-api/{note_id}/create-note-item",
 *     "create" = "/note-item-rest-api/{note_id}/create-note-item"
 *   }
 * )
 */
class NoteItemCreate extends ResourceBase
{
  const HTTP_OK = 200;
  const HTTP_UNPROC = 422;

  /**
   * Responds to entity POST requests.
   * @param Request $request
   * @param $note_id
   * @return \Drupal\rest\ResourceResponse
   */
  public function post(Request $request, $note_id)
  {
    $content = json_decode($request->getContent(), true);

    $errors = ApiValidation::validate($content, self::getRules(), 'note_item');

    if ($errors) {
      $response = [
        'success' => false,
        'status_code' => self::HTTP_UNPROC,
        'message' => $this->t("INVALID_DATA"),
        'data' => $errors,
      ];

      return new ResourceResponse($response);
    }

    try {
      $user = \Drupal::currentUser();
      $user_id = $user->id();
    } catch (\Exception $e) {
      $response = [
        'success' => false,
        'status_code' => self::HTTP_UNPROC,
        'message' => $this->t("USER_NOT_FOUND"),
        'data' => [],
      ];

      return new ResourceResponse($response);
    }

    //note id a userhez tarozik e
    $nids = \Drupal::entityTypeManager()
      ->getListBuilder('node')
      ->getStorage()
      ->loadByProperties([
        'type' => "notes",
        'nid' => $note_id,
        'field_notes_user_ref' => $user_id,
      ]);

    if (empty($nids)) {
      $response = [
        'success' => false,
        'status_code' => self::HTTP_OK,
        'message' => $this->t("NOTE_DOES_NOT_EXIST_OR_DOES_NOT_BELONG_TO_USER"),
        'data' => [
          'content' => $this->t("NOTE_DOES_NOT_EXIST_OR_DOES_NOT_BELONG_TO_USER"),
        ],
      ];

      return new ResourceResponse($response);
    }

    $account = \Drupal\user\Entity\User::load($user_id);
    if ($account->field_used_access_code[0]->target_id === null) {
      $response = [
        'success' => false,
        'status_code' => self::HTTP_OK,
        'message' => $this->t("USER_CANNOT_CREATE_NOTE_ITEM"),
        'data' => [
          'has_order' => false,
          'content' => $this->t("USER_DOES_NOT_HAVE_A_VALID_ACCESS_CODE"),
          'can_order' => false,
        ],
      ];

      return new ResourceResponse($response);
    }

    try {
      $note = [
        'type' => 'note_item',
      ];

      $note_item_name = $content['field_title'][0]['value'];

      $note["title"] = $note_item_name;
      $note["field_notes_user_ref"] = $user_id;
      $note["field_note_item_notes_ref"] = $note_id;

      $node = Node::create($note);
      $node->save();
    } catch (\Exception $e) {
      $response = [
        'success' => false,
        'status_code' => self::HTTP_UNPROC,
        'message' => $this->t("NOTE_ITEM_CANNOT_BE_CREATED"),
        'data' => [],
      ];

      return new ResourceResponse($response);
    }

    return new ResourceResponse([
      'success' => true,
      'status_code' => self::HTTP_OK,
      'message' => $this->t("NOTE_ITEM_CREATED"),
      'data' => [
        'content' => [
          $node->id() => [
            'id' => $node->id(),
            'title' => $node->getTitle(),
            'completed' => ($node->field_note_item_completed->value === '0' ? false : true),
          ],
        ],
      ],
    ]);
  }

  public static function getRules(): array
  {
    return [
      "field_title" => "required|min:3|max:255",
    ];
  }
}
