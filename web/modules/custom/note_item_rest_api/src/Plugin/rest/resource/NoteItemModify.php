<?php

namespace Drupal\note_item_rest_api\Plugin\rest\resource;

use Drupal\Core\Annotation\Translation;
use Drupal\kabi\Validation\ApiValidation;
use Drupal\node\Entity\Node;
use Drupal\rest\Annotation\RestResource;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Provides Resource for the Note Items functionality
 *
 * @RestResource(
 *   id = "note_item_modify",
 *   label = @Translation("NOTE_ITEM_MODIFY"),
 *   uri_paths = {
 *     "canonical" = "/note-item-rest-api/{note_item_id}/modify-note-item",
 *     "create" = "/note-item-rest-api/{note_item_id}/modify-note-item"
 *   }
 * )
 */
class NoteItemModify extends ResourceBase
{
  const HTTP_OK = 200;
  const HTTP_UNPROC = 422;

  /**
   * Responds to entity POST requests.
   * @param Request $request
   * @param $note_item_id
   * @return \Drupal\rest\ResourceResponse
   */
  public function post(Request $request, $note_item_id)
  {
    $content = json_decode($request->getContent(), true);

    if ($content === null) {
      $response = [
        'success' => false,
        'status_code' => self::HTTP_UNPROC,
        'message' => $this->t("INVALID_DATA"),
        'data' => [
          'error_title' => $this->t('EMPTY_REQUEST'),
        ],
      ];

      return new ResourceResponse($response);
    }

    $errors = ApiValidation::validate($content, self::getRules(), 'symptom');

    if ($errors) {
      $response = [
        'success' => false,
        'status_code' => self::HTTP_UNPROC,
        'message' => $this->t("INVALID_DATA"),
        'data' => $errors,
      ];

      return new ResourceResponse($response);
    }

    try {
      $user = \Drupal::currentUser();
      $user_id = $user->id();
    } catch (\Exception $e) {
      $response = [
        'success' => false,
        'status_code' => self::HTTP_UNPROC,
        'message' => $this->t("USER_NOT_FOUND"),
        'data' => [],
      ];

      return new ResourceResponse($response);
    }

    $account = \Drupal\user\Entity\User::load($user_id);
    if ($account->field_used_access_code[0]->target_id === null) {
      $response = [
        'success' => false,
        'status_code' => self::HTTP_OK,
        'message' => $this->t("USER_CANNOT_MODIFY_NOTE_ITEM"),
        'data' => [
          'content' => $this->t("USER_DOES_NOT_HAVE_A_VALID_ACCESS_CODE"),
        ],
      ];

      return new ResourceResponse($response);
    }

    try {
      $nids = \Drupal::entityTypeManager()
        ->getListBuilder('node')
        ->getStorage()
        ->loadByProperties([
          'type' => "note_item",
          'nid' => $note_item_id,
        ]);
      $keys = array_keys($nids);
      $node = null;
      if (isset($keys[0])) {
        $key = $keys[0];
        $node = $nids[$key];
      }

      if ($node === null) {
        $response = [
          'success' => false,
          'status_code' => self::HTTP_OK,
          'message' => $this->t("NOTE_ITEM_DOES_NOT_EXIST"),
          'data' => [
            'content' => $this->t("NOTE_ITEM_DOES_NOT_EXIST"),
          ],
        ];

        return new ResourceResponse($response);
      }

      $note_id = $node->field_note_item_notes_ref->target_id;

      $nids = \Drupal::entityTypeManager()
        ->getListBuilder('node')
        ->getStorage()
        ->loadByProperties([
          'type' => "notes",
          'nid' => $note_id,
          'field_notes_user_ref' => $user_id,
        ]);

      if (empty($nids)) {
        $response = [
          'success' => false,
          'status_code' => self::HTTP_OK,
          'message' => $this->t("NOTE_DOES_NOT_EXIST_OR_DOES_NOT_BELONG_TO_USER"),
          'data' => [
            'content' => $this->t("NOTE_DOES_NOT_EXIST_OR_DOES_NOT_BELONG_TO_USER"),
          ],
        ];

        return new ResourceResponse($response);
      }

      $shouldSave = false;

      if (isset($content['field_title']) && isset($content['field_title'][0]) && isset($content['field_title'][0]['value'])) {
        $node->set('title', $content['field_title'][0]['value']);
        $shouldSave = true;
      }

      if (isset($content['field_note_item_completed']) && isset($content['field_note_item_completed'][0]) && isset($content['field_note_item_completed'][0]['value']) && is_bool($content['field_note_item_completed'][0]['value'])) {
        $node->set('field_note_item_completed', $content['field_note_item_completed'][0]['value']);
        $shouldSave = true;
      }

      if ($shouldSave) {
        $node->save();
      }
    } catch (\Exception $e) {
      $response = [
        'success' => false,
        'status_code' => self::HTTP_UNPROC,
        'message' => $this->t("NOTE_ITEM_CANNOT_BE_MODIFIED"),
        'data' => [],
      ];

      return new ResourceResponse($response);
    }

    return new ResourceResponse([
      'success' => true,
      'status_code' => self::HTTP_OK,
      'message' => $this->t("NOTE_ITEM_MODIFIED"),
      'data' => [
        'content' => '',
      ],
    ]);
  }

  public static function getRules(): array
  {
    return [
      "field_title" => "min:3|max:255",
    ];
  }
}
