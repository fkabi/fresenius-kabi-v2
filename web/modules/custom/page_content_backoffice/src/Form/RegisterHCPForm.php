<?php

namespace Drupal\page_content_backoffice\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

class RegisterHCPForm extends FormBase
{

  /**
   * Returns a unique string identifying the form.
   *
   * The returned ID should be a unique string that can be a valid PHP function
   * name, since it's used in hook implementation names such as
   * hook_form_FORM_ID_alter().
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId()
  {
    return 'frontend_register_hcp_form_edit';
  }

  private function getData()
  {
    $vid = 'register_hcp_taxonomy';
    $properties['vid'] = $vid;

    $terms = \Drupal::entityTypeManager()
      ->getStorage('taxonomy_term')
      ->loadByProperties($properties);

    return reset($terms);
  }

  public function buildForm(array $form, FormStateInterface $form_state)
  {
    $form['preview'] = [
      '#type' => 'button',
      '#value' => $this->t('PREVIEW_STATE'),
      '#attributes' => array('onclick' => 'return false;'),
    ];
    $form['preview']['#attributes']['class'][] = 'preview-button';
    $form['preview']['#attributes']['class'][] = 'register-hcp-preview-button';

    $form['preview-mobile'] = [
      '#type' => 'button',
      '#value' => $this->t('PREVIEW_STATE_MOBILE'),
      '#attributes' => array('onclick' => 'return false;'),
    ];
    $form['preview-mobile']['#attributes']['class'][] = 'preview-button-mobile';
    $form['preview-mobile']['#attributes']['class'][] = 'register-hcp-preview-button-mobile';

    $term = $this->getData();

    $form['register_hcp_page_title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('REGISTER_PATIENT_PAGE_TITLE_LABEL'),
      '#description' => $this->t('DEMO: If you leave it empty, the original system message will be shown.'),
      '#required' => FALSE,
      '#default_value' => ($term->field_register_hcp_title->value ? $term->field_register_hcp_title->value : $this->t('REGISTER')),
    ];

    $form['register_hcp_page_user_title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('REGISTER_PATIENT_PAGE_USER_TITLE_LABEL'),
      '#description' => $this->t('DEMO: If you leave it empty, the original system message will be shown.'),
      '#required' => FALSE,
      '#default_value' => ($term->field_register_hcp_user_title->value ? $term->field_register_hcp_user_title->value : $this->t('TITLE')),
    ];

    $form['register_hcp_page_first_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('REGISTER_PATIENT_PAGE_FIRST_NAME_PLACEHOLDER_LABEL'),
      '#description' => $this->t('DEMO: If you leave it empty, the original system message will be shown.'),
      '#required' => FALSE,
      '#default_value' => ($term->field_register_hcp_fname->value ? $term->field_register_hcp_fname->value : $this->t('FIRST_NAME')),
    ];

    $form['register_hcp_page_last_name'] = [
      '#type' => 'textfield',
      '#title' => $this->t('REGISTER_PATIENT_PAGE_LAST_NAME_PLACEHOLDER_LABEL'),
      '#description' => $this->t('DEMO: If you leave it empty, the original system message will be shown.'),
      '#required' => FALSE,
      '#default_value' => ($term->field_register_hcp_lname->value ? $term->field_register_hcp_lname->value : $this->t('LAST_NAME')),
    ];

    $form['register_hcp_page_country'] = [
      '#type' => 'textfield',
      '#title' => $this->t('REGISTER_PATIENT_PAGE_COUNTRY_LABEL'),
      '#description' => $this->t('DEMO: If you leave it empty, the original system message will be shown.'),
      '#required' => FALSE,
      '#default_value' => ($term->field_register_hcp_country->value ? $term->field_register_hcp_country->value : $this->t('COUNTRY')),
    ];

    $form['register_hcp_page_healthcare_organization'] = [
      '#type' => 'textfield',
      '#title' => $this->t('REGISTER_PATIENT_PAGE_HEALTHCARE_ORGANIZATION_LABEL'),
      '#description' => $this->t('DEMO: If you leave it empty, the original system message will be shown.'),
      '#required' => FALSE,
      '#default_value' => ($term->field_register_hcp_org->value ? $term->field_register_hcp_org->value : $this->t('HEALTHCARE_ORGANIZATION')),
    ];

    $form['register_hcp_page_email'] = [
      '#type' => 'textfield',
      '#title' => $this->t('REGISTER_PATIENT_PAGE_EMAIL_LABEL'),
      '#description' => $this->t('DEMO: If you leave it empty, the original system message will be shown.'),
      '#required' => FALSE,
      '#default_value' => ($term->field_register_hcp_email->value ? $term->field_register_hcp_email->value : $this->t('EMAIL')),
    ];

    $form['register_hcp_page_password'] = [
      '#type' => 'textfield',
      '#title' => $this->t('REGISTER_PATIENT_PAGE_PASSWORD_LABEL'),
      '#description' => $this->t('DEMO: If you leave it empty, the original system message will be shown.'),
      '#required' => FALSE,
      '#default_value' => ($term->field_register_hcp_password->value ? $term->field_register_hcp_password->value : $this->t('PASSWORD')),
    ];

    $form['register_hcp_page_confirm_password'] = [
      '#type' => 'textfield',
      '#title' => $this->t('REGISTER_PATIENT_PAGE_CONFIRM_PASSWORD_LABEL'),
      '#description' => $this->t('DEMO: If you leave it empty, the original system message will be shown.'),
      '#required' => FALSE,
      '#default_value' => ($term->field_register_hcp_conf_pass->value ? $term->field_register_hcp_conf_pass->value : $this->t('CONFIRM_PASSWORD')),
    ];

    $form['register_hcp_page_registration_number_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('REGISTER_PATIENT_PAGE_REGISTRATION_NUMBER_LABEL'),
      '#description' => $this->t('DEMO: If you leave it empty, the original system message will be shown.'),
      '#required' => FALSE,
      '#default_value' => ($term->field_register_hcp_reg_num->value ? $term->field_register_hcp_reg_num->value : $this->t('REGISTRATION_NUMBER')),
    ];

    $form['register_hcp_page_checkbox_label'] = [
      '#type' => 'text_format',
      '#title' => $this->t('REGISTER_PATIENT_PAGE_CHECKBOX_LABEL'),
      '#description' => $this->t('DEMO: If you leave this empty, no content will be shown.'),
      '#required' => FALSE,
      '#default_value' => ($term->field_register_hcp_checkbox->value ? $term->field_register_hcp_checkbox->value : ''),
      '#format' => 'full_html',
    ];

    $form['register_hcp_page_submit_button_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('REGISTER_PATIENT_PAGE_SUBMIT_BUTTON_LABEL'),
      '#description' => $this->t('DEMO: If you leave it empty, the original system message will be shown.'),
      '#required' => FALSE,
      '#default_value' => ($term->field_register_hcp_submit_bt->value ? $term->field_register_hcp_submit_bt->value : $this->t('REGISTER')),
    ];

    $form['actions'] = [
      '#type' => 'actions',
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('SUBMIT'),
    ];

    unset($term);

    return $form;
  }

  /**
   * Validate the title and the checkbox of the form
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   */
  public function validateForm(array &$form, FormStateInterface $form_state)
  {
    parent::validateForm($form, $form_state);
  }

  public function submitForm(array &$form, FormStateInterface $form_state)
  {
    $values = $form_state->getValues();
    $term = $this->getData();

    $register_hcp_page_title = $values['register_hcp_page_title'];
    $register_hcp_page_user_title = $values['register_hcp_page_user_title'];
    $register_hcp_page_firstname = $values['register_hcp_page_first_name'];
    $register_hcp_page_lastname = $values['register_hcp_page_last_name'];
    $register_hcp_page_country = $values['register_hcp_page_country'];
    $register_hcp_page_healthcare_org = $values['register_hcp_page_healthcare_organization'];
    $register_hcp_page_email = $values['register_hcp_page_email'];
    $register_hcp_page_password = $values['register_hcp_page_password'];
    $register_hcp_page_confirm_password = $values['register_hcp_page_confirm_password'];
    $register_hcp_page_registration_number_label = $values['register_hcp_page_registration_number_label'];
    $register_hcp_page_checkbox_label = $values['register_hcp_page_checkbox_label']['value'];
    $register_hcp_page_submit_button_label = $values['register_hcp_page_submit_button_label'];

    $shouldSave = false;

    if (is_string($register_hcp_page_title)) {
      if ($term->field_register_hcp_title[0]->value !== $register_hcp_page_title) {
        $term->set('field_register_hcp_title', $register_hcp_page_title);

        $shouldSave = true;
      }
    }

    if (is_string($register_hcp_page_user_title)) {
      if ($term->field_register_hcp_user_title[0]->value !== $register_hcp_page_user_title) {
        $term->set('field_register_hcp_user_title', $register_hcp_page_user_title);

        $shouldSave = true;
      }
    }

    if (is_string($register_hcp_page_firstname)) {
      if ($term->field_register_hcp_fname[0]->value !== $register_hcp_page_firstname) {
        $term->set('field_register_hcp_fname', $register_hcp_page_firstname);

        $shouldSave = true;
      }
    }

    if (is_string($register_hcp_page_lastname)) {
      if ($term->field_register_hcp_lname[0]->value !== $register_hcp_page_lastname) {
        $term->set('field_register_hcp_lname', $register_hcp_page_lastname);

        $shouldSave = true;
      }
    }

    if (is_string($register_hcp_page_country)) {
      if ($term->field_register_hcp_country[0]->value !== $register_hcp_page_country) {
        $term->set('field_register_hcp_country', $register_hcp_page_country);

        $shouldSave = true;
      }
    }

    if (is_string($register_hcp_page_healthcare_org)) {
      if ($term->field_register_hcp_org[0]->value !== $register_hcp_page_healthcare_org) {
        $term->set('field_register_hcp_org', $register_hcp_page_healthcare_org);

        $shouldSave = true;
      }
    }

    if (is_string($register_hcp_page_email)) {
      if ($term->field_register_hcp_email[0]->value !== $register_hcp_page_email) {
        $term->set('field_register_hcp_email', $register_hcp_page_email);

        $shouldSave = true;
      }
    }

    if (is_string($register_hcp_page_password)) {
      if ($term->field_register_hcp_password[0]->value !== $register_hcp_page_password) {
        $term->set('field_register_hcp_password', $register_hcp_page_password);

        $shouldSave = true;
      }
    }

    if (is_string($register_hcp_page_confirm_password)) {
      if ($term->field_register_hcp_conf_pass[0]->value !== $register_hcp_page_confirm_password) {
        $term->set('field_register_hcp_conf_pass', $register_hcp_page_confirm_password);

        $shouldSave = true;
      }
    }

    if (is_string($register_hcp_page_registration_number_label)) {
      if ($term->field_register_hcp_reg_num[0]->value !== $register_hcp_page_registration_number_label) {
        $term->set('field_register_hcp_reg_num', $register_hcp_page_registration_number_label);

        $shouldSave = true;
      }
    }

    if (is_string($register_hcp_page_checkbox_label)) {
      if ($term->field_register_hcp_checkbox[0]->value !== $register_hcp_page_checkbox_label) {
        $term->set('field_register_hcp_checkbox', $register_hcp_page_checkbox_label);

        $shouldSave = true;
      }
    }

    if (is_string($register_hcp_page_submit_button_label)) {
      if ($term->field_register_hcp_submit_bt[0]->value !== $register_hcp_page_submit_button_label) {
        $term->set('field_register_hcp_submit_bt', $register_hcp_page_submit_button_label);

        $shouldSave = true;
      }
    }

    $messenger = \Drupal::messenger();

    try {
      if ($shouldSave) {
        $term->save();
      }
    } catch (\Exception $e) {
      $messenger->addError($e->getMessage());
      $form_state->setRedirect('page_content_backoffice.register_hcp_page_form');
    }

    if ($shouldSave) {
      $messenger->addMessage($this->t('SUCCESSFULLY_SAVED'));
    } else {
      $messenger->addMessage($this->t('NOTHING_TO_SAVE'));
    }

    $form_state->setRedirect('page_content_backoffice.register_hcp_page_form');
  }
}
