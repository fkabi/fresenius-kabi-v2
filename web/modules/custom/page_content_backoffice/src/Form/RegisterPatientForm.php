<?php

namespace Drupal\page_content_backoffice\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

class RegisterPatientForm extends FormBase
{

  /**
   * Returns a unique string identifying the form.
   *
   * The returned ID should be a unique string that can be a valid PHP function
   * name, since it's used in hook implementation names such as
   * hook_form_FORM_ID_alter().
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId()
  {
    return 'frontend_register_patient_form_edit';
  }

  private function getData()
  {
    $vid = 'register_patient_taxonomy';
    $properties['vid'] = $vid;

    $terms = \Drupal::entityTypeManager()
      ->getStorage('taxonomy_term')
      ->loadByProperties($properties);

    return reset($terms);
  }

  public function buildForm(array $form, FormStateInterface $form_state)
  {
    $form['preview'] = [
      '#type' => 'button',
      '#value' => $this->t('PREVIEW_STATE'),
      '#attributes' => array('onclick' => 'return false;'),
    ];
    $form['preview']['#attributes']['class'][] = 'preview-button';
    $form['preview']['#attributes']['class'][] = 'register-patient-preview-button';

    $form['preview-mobile'] = [
      '#type' => 'button',
      '#value' => $this->t('PREVIEW_STATE_MOBILE'),
      '#attributes' => array('onclick' => 'return false;'),
    ];
    $form['preview-mobile']['#attributes']['class'][] = 'preview-button-mobile';
    $form['preview-mobile']['#attributes']['class'][] = 'register-patient-preview-button-mobile';

    $term = $this->getData();

    $form['register_patient_page_title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('REGISTER_PATIENT_PAGE_TITLE_LABEL'),
      '#description' => $this->t('DEMO: If you leave it empty, the original system message will be shown.'),
      '#required' => FALSE,
      '#default_value' => ($term->field_register_patient_title->value ? $term->field_register_patient_title->value : $this->t('REGISTER')),
    ];

    $form['register_patient_page_email'] = [
      '#type' => 'textfield',
      '#title' => $this->t('REGISTER_PATIENT_PAGE_EMAIL_PLACEHOLDER_LABEL'),
      '#description' => $this->t('DEMO: If you leave it empty, the original system message will be shown.'),
      '#required' => FALSE,
      '#default_value' => ($term->field_register_patient_email->value ? $term->field_register_patient_email->value : $this->t('EMAIL')),
    ];

    $form['register_patient_page_condition'] = [
      '#type' => 'textfield',
      '#title' => $this->t('REGISTER_PATIENT_PAGE_DIAGNOSED_CONDITION_LABEL'),
      '#description' => $this->t('DEMO: If you leave it empty, the original system message will be shown.'),
      '#required' => FALSE,
      '#default_value' => ($term->field_register_patient_condition->value ? $term->field_register_patient_condition->value : $this->t('DIAGNOSED_CONDITION')),
    ];

    $form['register_patient_page_access_code'] = [
      '#type' => 'textfield',
      '#title' => $this->t('REGISTER_PATIENT_PAGE_ACCESS_CODE_LABEL'),
      '#description' => $this->t('DEMO: If you leave it empty, the original system message will be shown.'),
      '#required' => FALSE,
      '#default_value' => ($term->field_register_patient_acc_code->value ? $term->field_register_patient_acc_code->value : $this->t('ACCESS_CODE')),
    ];

    $form['register_patient_page_password'] = [
      '#type' => 'textfield',
      '#title' => $this->t('REGISTER_PATIENT_PAGE_PASSWORD_LABEL'),
      '#description' => $this->t('DEMO: If you leave it empty, the original system message will be shown.'),
      '#required' => FALSE,
      '#default_value' => ($term->field_register_patient_password->value ? $term->field_register_patient_password->value : $this->t('PASSWORD')),
    ];

    $form['register_patient_page_confirm_password'] = [
      '#type' => 'textfield',
      '#title' => $this->t('REGISTER_PATIENT_PAGE_CONFIRM_PASSWORD_LABEL'),
      '#description' => $this->t('DEMO: If you leave it empty, the original system message will be shown.'),
      '#required' => FALSE,
      '#default_value' => ($term->field_register_patient_conf_pass->value ? $term->field_register_patient_conf_pass->value : $this->t('CONFIRM_PASSWORD')),
    ];

    $form['register_patient_page_checkbox_label'] = [
      '#type' => 'text_format',
      '#title' => $this->t('REGISTER_PATIENT_PAGE_CHECKBOX_LABEL'),
      '#description' => $this->t('DEMO: If you leave this empty, no content will be shown.'),
      '#required' => FALSE,
      '#default_value' => ($term->field_register_patient_checkbox->value ? $term->field_register_patient_checkbox->value : ''),
      '#format' => 'full_html',
    ];

    $form['register_patient_page_submit_button_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('REGISTER_PATIENT_PAGE_SUBMIT_BUTTON_LABEL'),
      '#description' => $this->t('DEMO: If you leave it empty, the original system message will be shown.'),
      '#required' => FALSE,
      '#default_value' => ($term->field_register_patient_submit_bt->value ? $term->field_register_patient_submit_bt->value : $this->t('REGISTER')),
    ];

    $form['actions'] = [
      '#type' => 'actions',
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('SUBMIT'),
    ];

    unset($term);

    return $form;
  }

  /**
   * Validate the title and the checkbox of the form
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   */
  public function validateForm(array &$form, FormStateInterface $form_state)
  {
    parent::validateForm($form, $form_state);
  }

  public function submitForm(array &$form, FormStateInterface $form_state)
  {
    $values = $form_state->getValues();
    $term = $this->getData();

    $register_patient_page_title = $values['register_patient_page_title'];
    $register_patient_page_email = $values['register_patient_page_email'];
    $register_patient_page_condition = $values['register_patient_page_condition'];
    $register_patient_page_access_code = $values['register_patient_page_access_code'];
    $register_patient_page_password = $values['register_patient_page_password'];
    $register_patient_page_confirm_password = $values['register_patient_page_confirm_password'];
    $register_patient_page_checkbox_label = $values['register_patient_page_checkbox_label']['value'];
    $register_patient_page_submit_button_label = $values['register_patient_page_submit_button_label'];

    $shouldSave = false;

    if (is_string($register_patient_page_title)) {
      if ($term->field_register_patient_title[0]->value !== $register_patient_page_title) {
        $term->set('field_register_patient_title', $register_patient_page_title);

        $shouldSave = true;
      }
    }

    if (is_string($register_patient_page_email)) {
      if ($term->field_register_patient_email[0]->value !== $register_patient_page_email) {
        $term->set('field_register_patient_email', $register_patient_page_email);

        $shouldSave = true;
      }
    }

    if (is_string($register_patient_page_condition)) {
      if ($term->field_register_patient_condition[0]->value !== $register_patient_page_condition) {
        $term->set('field_register_patient_condition', $register_patient_page_condition);

        $shouldSave = true;
      }
    }

    if (is_string($register_patient_page_access_code)) {
      if ($term->field_register_patient_acc_code[0]->value !== $register_patient_page_access_code) {
        $term->set('field_register_patient_acc_code', $register_patient_page_access_code);

        $shouldSave = true;
      }
    }

    if (is_string($register_patient_page_password)) {
      if ($term->field_register_patient_password[0]->value !== $register_patient_page_password) {
        $term->set('field_register_patient_password', $register_patient_page_password);

        $shouldSave = true;
      }
    }

    if (is_string($register_patient_page_confirm_password)) {
      if ($term->field_register_patient_conf_pass[0]->value !== $register_patient_page_confirm_password) {
        $term->set('field_register_patient_conf_pass', $register_patient_page_confirm_password);

        $shouldSave = true;
      }
    }

    if (is_string($register_patient_page_checkbox_label)) {
      if ($term->field_register_patient_checkbox[0]->value !== $register_patient_page_checkbox_label) {
        $term->set('field_register_patient_checkbox', $register_patient_page_checkbox_label);

        $shouldSave = true;
      }
    }

    if (is_string($register_patient_page_submit_button_label)) {
      if ($term->field_register_patient_submit_bt[0]->value !== $register_patient_page_submit_button_label) {
        $term->set('field_register_patient_submit_bt', $register_patient_page_submit_button_label);

        $shouldSave = true;
      }
    }

    $messenger = \Drupal::messenger();

    try {
      if ($shouldSave) {
        $term->save();
      }
    } catch (\Exception $e) {
      $messenger->addError($e->getMessage());
      $form_state->setRedirect('page_content_backoffice.register_patient_page_form');
    }

    if ($shouldSave) {
      $messenger->addMessage($this->t('SUCCESSFULLY_SAVED'));
    } else {
      $messenger->addMessage($this->t('NOTHING_TO_SAVE'));
    }

    $form_state->setRedirect('page_content_backoffice.register_patient_page_form');
  }
}
