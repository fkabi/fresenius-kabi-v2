<?php

namespace Drupal\page_content_backoffice\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\file\Entity\File;
use Drupal\vimeo_module\Controller\VimeoController;

class FrontendYourConditionPageForm extends FormBase
{

  /**
   * Returns a unique string identifying the form.
   *
   * The returned ID should be a unique string that can be a valid PHP function
   * name, since it's used in hook implementation names such as
   * hook_form_FORM_ID_alter().
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId()
  {
    return 'frontend_your_condition_page_form_edit';
  }

  private function getData($saving = false)
  {
    $vid = 'frontend_your_condition_taxonomy';
    $properties['vid'] = $vid;

    $terms = \Drupal::entityTypeManager()
      ->getStorage('taxonomy_term')
      ->loadByProperties($properties);

    if ($saving && empty($terms)) {
      throw new \Exception("Internal error! Taxonomy Term not exists!");
    }

    return reset($terms);
  }

  public function buildForm(array $form, FormStateInterface $form_state)
  {
    /*$form['preview'] = [
      '#type' => 'button',
      '#value' => $this->t('PREVIEW_STATE'),
      '#attributes' => array('onclick' => 'return false;'),
    ];
    $form['preview']['#attributes']['class'][] = 'preview-button';
    $form['preview']['#attributes']['class'][] = 'footer-preview-button';

    $form['preview-mobile'] = [
      '#type' => 'button',
      '#value' => $this->t('PREVIEW_STATE_MOBILE'),
      '#attributes' => array('onclick' => 'return false;'),
    ];
    $form['preview-mobile']['#attributes']['class'][] = 'preview-button-mobile';
    $form['preview-mobile']['#attributes']['class'][] = 'footer-preview-button-mobile';*/

    $term = $this->getData();

    $form['hero_h1'] = [
      '#type' => 'item',
      '#markup' => '<h1>Hero</h1>',
    ];

    $form['frontend_your_condition_page_title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('FRONTEND_YOUR_CONDITION_PAGE_TITLE_LABEL'),
      '#description' => $this->t('DEMO: If you leave it empty, the original system message will be shown'),
      '#required' => false,
      '#default_value' => ($term->field_ur_cond_page_title->value ? $term->field_ur_cond_page_title->value : $this->t('YOUR_CONDITION_PAGE_TITLE')),
    ];

    $form['frontend_your_condition_page_description'] = [
      '#type' => 'textarea',
      '#title' => $this->t('FRONTEND_YOUR_CONDITION_PAGE_DESCRIPTION_LABEL'),
      '#description' => $this->t('DEMO: If you leave it empty, the original system message will be shown'),
      '#required' => false,
      '#default_value' => ($term->field_ur_cond_page_desc->value ? $term->field_ur_cond_page_desc->value : $this->t('')),
      '#attributes' => [
        'max-length' => 512,
        'maxlength' => 512,
      ],
    ];

    $result = \Drupal::entityQuery('node')
      ->condition('type', 'diagnosed_condition')
    ->execute();

    $conditions = [];
    foreach($result as $id){
      $condition = \Drupal\node\Entity\Node::load($id);
      $conditions[$id] = $condition->getTitle();
    }

    $form['frontend_your_condition_page_condition_select'] = [
      '#type' => 'select',
      '#title' => $this->t('FRONTEND_YOUR_CONDITION_PAGE_DEFAULT_CONDITION_SELECTOR_LABEL'),
      '#options' => $conditions,
      '#default_value' => $term->field_ur_cond_page_default_cond->value,
      '#description' => $this->t('DEMO: By default the User\'s condition will be selected. If the User for some reason does not have a condition or a Guest visits the page this value will be set as default.'),
    ];

    $form['diseases_list_h1'] = [
      '#type' => 'item',
      '#markup' => '<hr/><h1>Disease Subpages</h1><div class="description">If you click on links the unsaved content will be lost!</div>',
    ];

    $form['disease_1_link'] = [
      '#type' => 'item',
      '#markup' => '<a href="' . Url::fromRoute('page_content_backoffice.your_condition_page_ibd_form')->toString() . '">IBD</a>',
    ];

    $form['disease_2_link'] = [
      '#type' => 'item',
      '#markup' => '<a href="#">Crohn\'s disease</a>',
    ];

    $form['disease_3_link'] = [
      '#type' => 'item',
      '#markup' => '<a href="#">Ulcerative colitis</a>',
    ];

    $form['disease_4_link'] = [
      '#type' => 'item',
      '#markup' => '<a href="#">Plaque psoriasis</a>',
    ];

    $form['disease_5_link'] = [
      '#type' => 'item',
      '#markup' => '<a href="#">Hidradenitis suppurativa</a>',
    ];

    $form['disease_6_link'] = [
      '#type' => 'item',
      '#markup' => '<a href="#">Rheumatoid arthritis</a>',
    ];

    $form['disease_7_link'] = [
      '#type' => 'item',
      '#markup' => '<a href="#">Psoriatic arthritis</a>',
    ];

    $form['disease_8_link'] = [
      '#type' => 'item',
      '#markup' => '<a href="#">Spondyloarthritis or Ankylosing spondylitis</a>',
    ];

    $form['disease_9_link'] = [
      '#type' => 'item',
      '#markup' => '<a href="' . Url::fromRoute('page_content_backoffice.your_condition_page_juvenile_form')->toString() . '">Juvenile idiopathic arthritis</a>',
    ];

    $form['actions'] = [
      '#type' => 'actions',
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('SUBMIT'),
    ];

    unset($term);

    return $form;
  }

  /**
   * Validate the title and the checkbox of the form
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   */
  public function validateForm(array &$form, FormStateInterface $form_state)
  {
    parent::validateForm($form, $form_state);
  }

  public function submitForm(array &$form, FormStateInterface $form_state)
  {
    $values = $form_state->getValues();
    $messenger = \Drupal::messenger();

    try {
      $term = $this->getData(true);
    } catch (\Exception $e) {
      $messenger->addError($e->getMessage());
      return $form_state->setRedirect('page_content_backoffice.your_condition_page_form');
    }

    $frontend_your_condition_title = $values['frontend_your_condition_page_title'];
    $frontend_your_condition_description = $values['frontend_your_condition_page_description'];
    $frontend_your_condition_select_default_value = $values['frontend_your_condition_page_condition_select'];

    $shouldSave = false;

    if (is_string($frontend_your_condition_title)) {
      if ($term->field_ur_cond_page_title[0]->value !== $frontend_your_condition_title) {
        $term->set('field_ur_cond_page_title', $frontend_your_condition_title);

        $shouldSave = true;
      }
    }

    if (is_string($frontend_your_condition_description)) {
      if ($term->field_ur_cond_page_desc[0]->value !== $frontend_your_condition_description) {
        $term->set('field_ur_cond_page_desc', $frontend_your_condition_description);

        $shouldSave = true;
      }
    }

    if (is_string($frontend_your_condition_select_default_value)) {
      if ($term->field_ur_cond_page_default_cond[0]->value !== $frontend_your_condition_select_default_value) {
        $term->set('field_ur_cond_page_default_cond', $frontend_your_condition_select_default_value);

        $shouldSave = true;
      }
    }

    try {
      if ($shouldSave) {
        $term->save();
      }
    } catch (\Exception $e) {
      $messenger->addError($e->getMessage());
      $form_state->setRedirect('page_content_backoffice.your_condition_page_form');
    }

    if ($shouldSave) {
      $messenger->addMessage($this->t('SUCCESSFULLY_SAVED'));
    } else {
      $messenger->addMessage($this->t('NOTHING_TO_SAVE'));
    }

    $form_state->setRedirect('page_content_backoffice.your_condition_page_form');
  }
}
