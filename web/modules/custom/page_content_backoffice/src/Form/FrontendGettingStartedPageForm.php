<?php

namespace Drupal\page_content_backoffice\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\file\Entity\File;

class FrontendGettingStartedPageForm extends FormBase
{

  /**
   * Returns a unique string identifying the form.
   *
   * The returned ID should be a unique string that can be a valid PHP function
   * name, since it's used in hook implementation names such as
   * hook_form_FORM_ID_alter().
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId()
  {
    return 'frontend_getting_started_page_form_edit';
  }

  private function getData($saving = false)
  {
    $vid = 'frontend_getting_start_taxonomy';
    $properties['vid'] = $vid;

    $terms = \Drupal::entityTypeManager()
      ->getStorage('taxonomy_term')
      ->loadByProperties($properties);

    if ($saving && empty($terms)) {
      throw new \Exception("Internal error! Taxonomy Term not exists!");
    }

    return reset($terms);
  }

  public function buildForm(array $form, FormStateInterface $form_state)
  {
    /*$form['preview'] = [
      '#type' => 'button',
      '#value' => $this->t('PREVIEW_STATE'),
      '#attributes' => array('onclick' => 'return false;'),
    ];
    $form['preview']['#attributes']['class'][] = 'preview-button';
    $form['preview']['#attributes']['class'][] = 'footer-preview-button';

    $form['preview-mobile'] = [
      '#type' => 'button',
      '#value' => $this->t('PREVIEW_STATE_MOBILE'),
      '#attributes' => array('onclick' => 'return false;'),
    ];
    $form['preview-mobile']['#attributes']['class'][] = 'preview-button-mobile';
    $form['preview-mobile']['#attributes']['class'][] = 'footer-preview-button-mobile';*/

    $term = $this->getData();

    $form['frontend_getting_started_page_title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('FRONTEND_GETTING_STARTED_PAGE_TITLE_LABEL'),
      '#description' => $this->t('DEMO: If you leave it empty, the original system message will be shown'),
      '#required' => FALSE,
      '#default_value' => ($term->field_getting_started_title->value ? $term->field_getting_started_title->value : $this->t('GETTING_STARTED_PAGE_TITLE')),
    ];

    $form['frontend_getting_started_page_description'] = [
      '#type' => 'textarea',
      '#title' => $this->t('FRONTEND_GETTING_STARTED_PAGE_DESCRIPTION_LABEL'),
      '#description' => $this->t('DEMO: If you leave it empty, the original system message will be shown'),
      '#required' => FALSE,
      '#default_value' => ($term->field_getting_started_desc->value ? $term->field_getting_started_desc->value : $this->t('GETTING_STARTED_PAGE_DESCRIPTION')),
      '#attributes' => [
        'max-length' => 255,
        'maxlength' => 255,
      ],
    ];

    $getting_started_page_carousel = null;
    if ($term->field_getting_start_carousel_ref->target_id) {
      $getting_started_page_carousel = \Drupal\node\Entity\Node::load($term->field_getting_start_carousel_ref->target_id);
    }

    $form['frontend_getting_started_page_carousel'] = array(
      '#type' => 'entity_autocomplete',
      '#title' => $this->t('FRONTEND_GETTING_STARTED_PAGE_CAROUSEL_LABEL'),
      '#target_type' => 'node',
      '#default_value' => ($term->field_getting_start_carousel_ref->target_id ? $getting_started_page_carousel : ''),
      '#selection_handler' => 'default', // Optional. The default selection handler is pre-populated to 'default'.
      '#selection_settings' => array(
        'target_bundles' => array('homepage_carousel', 'content'),
      ),
    );

    $form['iso_h1'] = [
      '#type' => 'item',
      '#markup' => '<hr/><h1>ISO Number</h1>',
    ];

    $form['frontend_getting_started_page_iso_number'] = [
      '#type' => 'textfield',
      '#title' => $this->t('FRONTEND_GETTING_STARTED_PAGE_ISO_NUMBER_LABEL'),
      '#description' => $this->t('DEMO: If you leave it empty, the original system message will be shown'),
      '#required' => false,
      '#default_value' => ($term->field_getting_started_page_iso->value ? $term->field_getting_started_page_iso->value : $this->t('')),
    ];

    $form['actions'] = [
      '#type' => 'actions',
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('SUBMIT'),
    ];

    unset($term);

    return $form;
  }

  /**
   * Validate the title and the checkbox of the form
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   */
  public function validateForm(array &$form, FormStateInterface $form_state)
  {
    parent::validateForm($form, $form_state);
  }

  public function submitForm(array &$form, FormStateInterface $form_state)
  {
    $values = $form_state->getValues();
    $messenger = \Drupal::messenger();

    try {
      $term = $this->getData(true);
    } catch (\Exception $e) {
      $messenger->addError($e->getMessage());
      return $form_state->setRedirect('page_content_backoffice.getting_started_page_form');
    }

    $frontend_getting_started_page_title = $values['frontend_getting_started_page_title'];
    $frontend_getting_started_page_description = $values['frontend_getting_started_page_description'];
    $frontend_getting_started_page_carousel = $values['frontend_getting_started_page_carousel'];
    $frontend_getting_started_page_iso_number = $values['frontend_getting_started_page_iso_number'];

    $shouldSave = false;

    if (is_string($frontend_getting_started_page_title)) {
      if ($term->field_getting_started_title[0]->value !== $frontend_getting_started_page_title) {
        $term->set('field_getting_started_title', $frontend_getting_started_page_title);

        $shouldSave = true;
      }
    }

    if (is_string($frontend_getting_started_page_description)) {
      if ($term->field_getting_started_desc[0]->value !== $frontend_getting_started_page_description) {
        $term->set('field_getting_started_desc', $frontend_getting_started_page_description);

        $shouldSave = true;
      }
    }

    if (is_string($frontend_getting_started_page_carousel)) {
      if (isset($frontend_getting_started_page_carousel) && !empty($frontend_getting_started_page_carousel)) {
        $term->set('field_getting_start_carousel_ref', ['target_id' => $frontend_getting_started_page_carousel]);

        $shouldSave = true;
      }
    }

    if (is_string($frontend_getting_started_page_iso_number)) {
      if ($term->field_getting_started_page_iso[0]->value !== $frontend_getting_started_page_iso_number) {
        $term->set('field_getting_started_page_iso', $frontend_getting_started_page_iso_number);

        $shouldSave = true;
      }
    }

    try {
      if ($shouldSave) {
        $term->save();
      }
    } catch (\Exception $e) {
      $messenger->addError($e->getMessage());
      $form_state->setRedirect('page_content_backoffice.getting_started_page_form');
    }

    if ($shouldSave) {
      $messenger->addMessage($this->t('SUCCESSFULLY_SAVED'));
    } else {
      $messenger->addMessage($this->t('NOTHING_TO_SAVE'));
    }

    $form_state->setRedirect('page_content_backoffice.getting_started_page_form');
  }
}
