<?php

namespace Drupal\page_content_backoffice\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\file\Entity\File;
use Drupal\vimeo_module\Controller\VimeoController;

class FrontendEatingWellPageForm extends FormBase
{

  /**
   * Returns a unique string identifying the form.
   *
   * The returned ID should be a unique string that can be a valid PHP function
   * name, since it's used in hook implementation names such as
   * hook_form_FORM_ID_alter().
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId()
  {
    return 'frontend_eating_well_page_form_edit';
  }

  private function getData($saving = false)
  {
    $vid = 'frontend_eating_well_taxonomy';
    $properties['vid'] = $vid;

    $terms = \Drupal::entityTypeManager()
      ->getStorage('taxonomy_term')
      ->loadByProperties($properties);

    if ($saving && empty($terms)) {
      throw new \Exception("Internal error! Taxonomy Term not exists!");
    }

    return reset($terms);
  }

  public function buildForm(array $form, FormStateInterface $form_state)
  {
    /*$form['preview'] = [
      '#type' => 'button',
      '#value' => $this->t('PREVIEW_STATE'),
      '#attributes' => array('onclick' => 'return false;'),
    ];
    $form['preview']['#attributes']['class'][] = 'preview-button';
    $form['preview']['#attributes']['class'][] = 'footer-preview-button';

    $form['preview-mobile'] = [
      '#type' => 'button',
      '#value' => $this->t('PREVIEW_STATE_MOBILE'),
      '#attributes' => array('onclick' => 'return false;'),
    ];
    $form['preview-mobile']['#attributes']['class'][] = 'preview-button-mobile';
    $form['preview-mobile']['#attributes']['class'][] = 'footer-preview-button-mobile';*/

    $term = $this->getData();

    $form['hero_h1'] = [
      '#type' => 'item',
      '#markup' => '<h1>Hero</h1>',
    ];

    $form['frontend_eating_well_page_title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('FRONTEND_EATING_WELL_PAGE_TITLE_LABEL'),
      '#description' => $this->t('DEMO: If you leave it empty, the original system message will be shown'),
      '#required' => false,
      '#default_value' => ($term->field_eating_well_page_title->value ? $term->field_eating_well_page_title->value : $this->t('EATING_WELL_PAGE_TITLE')),
    ];

    $form['section_1_h1'] = [
      '#type' => 'item',
      '#markup' => '<hr/><h1>Section 1</h1>',
    ];

    $form['frontend_eating_well_page_section_1_string_1'] = [
      '#type' => 'textfield',
      '#title' => $this->t('FRONTEND_EATING_WELL_PAGE_SECTION_1_FIRST_STRING_LABEL'),
      '#description' => $this->t('DEMO: If you leave it empty, the original system message will be shown'),
      '#required' => false,
      '#default_value' => ($term->field_ew_sec1_str1->value ? $term->field_ew_sec1_str1->value : $this->t('')),
    ];

    $form['frontend_eating_well_page_section_1_string_2'] = [
      '#type' => 'textfield',
      '#title' => $this->t('FRONTEND_EATING_WELL_PAGE_SECTION_1_SECOND_STRING_LABEL'),
      '#description' => $this->t('DEMO: If you leave it empty, the original system message will be shown'),
      '#required' => false,
      '#default_value' => ($term->field_ew_sec1_str2->value ? $term->field_ew_sec1_str2->value : $this->t('')),
    ];

    $form['frontend_eating_well_page_section_1_desc'] = [
      '#type' => 'textarea',
      '#title' => $this->t('FRONTEND_EATING_WELL_PAGE_SECTION_1_DESCRIPTION_LABEL'),
      '#description' => $this->t('DEMO: If you leave it empty, the original system message will be shown'),
      '#required' => false,
      '#default_value' => ($term->field_ew_sec1_desc->value ? $term->field_ew_sec1_desc->value : $this->t('')),
      '#attributes' => [
        'max-length' => 512,
        'maxlength' => 512,
      ],
    ];

    $form['gallery_h1'] = [
      '#type' => 'item',
      '#markup' => '<hr/><h1>Gallery</h1>',
    ];

    $eating_well_page_carousel = null;
    if ($term->field_eating_well_carousel_ref->target_id) {
      $eating_well_page_carousel = \Drupal\node\Entity\Node::load($term->field_eating_well_carousel_ref->target_id);
    }

    $form['frontend_eating_well_page_carousel'] = [
      '#type' => 'entity_autocomplete',
      '#title' => $this->t('FRONTEND_EATING_WELL_PAGE_CAROUSEL_LABEL'),
      '#target_type' => 'node',
      '#default_value' => ($term->field_eating_well_carousel_ref->target_id ? $eating_well_page_carousel : ''),
      '#selection_handler' => 'default', // Optional. The default selection handler is pre-populated to 'default'.
      '#selection_settings' => [
        'target_bundles' => ['homepage_carousel', 'content'],
      ],
    ];

    $form['section_2_h1'] = [
      '#type' => 'item',
      '#markup' => '<hr/><h1>Section 2</h1>',
    ];

    $form['frontend_eating_well_page_section_2_string_1'] = [
      '#type' => 'textfield',
      '#title' => $this->t('FRONTEND_EATING_WELL_PAGE_SECTION_2_FIRST_STRING_LABEL'),
      '#description' => $this->t('DEMO: If you leave it empty, the original system message will be shown'),
      '#required' => false,
      '#default_value' => ($term->field_ew_sec2_str1->value ? $term->field_ew_sec2_str1->value : $this->t('')),
    ];

    $form['frontend_eating_well_page_section_2_string_2'] = [
      '#type' => 'textfield',
      '#title' => $this->t('FRONTEND_EATING_WELL_PAGE_SECTION_2_SECOND_STRING_LABEL'),
      '#description' => $this->t('DEMO: If you leave it empty, the original system message will be shown'),
      '#required' => false,
      '#default_value' => ($term->field_ew_sec2_str2->value ? $term->field_ew_sec2_str2->value : $this->t('')),
    ];

    $form['frontend_eating_well_page_section_2_desc'] = [
      '#type' => 'textarea',
      '#title' => $this->t('FRONTEND_EATING_WELL_PAGE_SECTION_2_DESCRIPTION_LABEL'),
      '#description' => $this->t('DEMO: If you leave it empty, the original system message will be shown'),
      '#required' => false,
      '#default_value' => ($term->field_ew_sec2_desc->value ? $term->field_ew_sec2_desc->value : $this->t('')),
      '#attributes' => [
        'max-length' => 512,
        'maxlength' => 512,
      ],
    ];

    $form['card_h1'] = [
      '#type' => 'item',
      '#markup' => '<hr/><h1>Card</h1>',
    ];

    $form['frontend_eating_well_page_card_title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('FRONTEND_EATING_WELL_PAGE_CARD_TITLE_LABEL'),
      '#description' => $this->t('DEMO: If you leave it empty, the original system message will be shown'),
      '#required' => false,
      '#default_value' => ($term->field_eating_well_card_title->value ? $term->field_eating_well_card_title->value : $this->t('')),
    ];

    $form['frontend_eating_well_page_card_description'] = [
      '#type' => 'textarea',
      '#title' => $this->t('FRONTEND_EATING_WELL_PAGE_CARD_DESCRIPTION_LABEL'),
      '#description' => $this->t('DEMO: If you leave it empty, the original system message will be shown'),
      '#required' => false,
      '#default_value' => ($term->field_eating_well_card_desc->value ? $term->field_eating_well_card_desc->value : $this->t('')),
      '#attributes' => [
        'max-length' => 512,
        'maxlength' => 512,
      ],
    ];

    $form['frontend_eating_well_page_card_image'] = [
      '#type' => 'managed_file',
      '#title' => $this->t('FRONTEND_EATING_WELL_PAGE_CARD_IMAGE_LABEL'),
      '#description' => $this->t('DEMO: If you leave it empty, the card will have a simple white background.'),
      '#required' => false,
      '#size' => 40,
      '#name' => 'eating_well_card_image',
      '#upload_location' => 'public://',
      '#theme' => 'image_widget',
      '#preview_image_style' => 'medium',
    ];

    $current_logo_id = $term->field_eating_well_card_image[0]->target_id;
    $current_Logo = File::load($current_logo_id);
    if ($current_Logo !== null) {
      $uri = $current_Logo->getFileUri();
      $stream_wrapper_manager = \Drupal::service('stream_wrapper_manager')->getViaUri($uri);
      $file_path = $stream_wrapper_manager->realpath();
      $ext_path = $stream_wrapper_manager->getExternalUrl();

      if (file_exists($file_path) && strpos($file_path, '/web/web/') && strpos($ext_path, '/web/web/') === false) {
        $ext_path = str_replace('/web/', '/web/web/', $ext_path);
      }

      $form['frontend_eating_well_page_card_current_image'] = [
        '#type' => 'item',
        '#markup' => '<img src="' . $ext_path . '" alt="' . $this->t('CURRENT_LOGO') . '" name="frontend_eating_well_page_card_current_image" id="frontend_eating_well_page_card_current_image">',
        '#prefix' => '<div class="current-logo">',
        '#suffix' => '</div>',
        '#title' => $this->t('FRONTEND_EATING_WELL_PAGE_CARD_CURRENT_IMAGE_LABEL'),
        '#theme_wrappers' => ['form_element', 'views_ui_container'],
        '#attributes' => ['class' => ['container-inline', 'views-add-form-selected']],
      ];
    }


    $form['section_3_h1'] = [
      '#type' => 'item',
      '#markup' => '<hr/><h1>Section 3</h1>',
    ];

    $form['frontend_eating_well_page_section_3_string_1'] = [
      '#type' => 'textfield',
      '#title' => $this->t('FRONTEND_EATING_WELL_PAGE_SECTION_3_FIRST_STRING_LABEL'),
      '#description' => $this->t('DEMO: If you leave it empty, the original system message will be shown'),
      '#required' => false,
      '#default_value' => ($term->field_ew_sec3_str1->value ? $term->field_ew_sec3_str1->value : $this->t('')),
    ];

    $form['frontend_eating_well_page_section_3_string_2'] = [
      '#type' => 'textfield',
      '#title' => $this->t('FRONTEND_EATING_WELL_PAGE_SECTION_3_SECOND_STRING_LABEL'),
      '#description' => $this->t('DEMO: If you leave it empty, the original system message will be shown'),
      '#required' => false,
      '#default_value' => ($term->field_ew_sec3_str2->value ? $term->field_ew_sec3_str2->value : $this->t('')),
    ];

    $form['frontend_eating_well_page_section_3_desc'] = [
      '#type' => 'textarea',
      '#title' => $this->t('FRONTEND_EATING_WELL_PAGE_SECTION_3_DESCRIPTION_LABEL'),
      '#description' => $this->t('DEMO: If you leave it empty, the original system message will be shown'),
      '#required' => false,
      '#default_value' => ($term->field_ew_sec3_desc->value ? $term->field_ew_sec3_desc->value : $this->t('')),
      '#attributes' => [
        'max-length' => 512,
        'maxlength' => 512,
      ],
    ];

    $form['symptoms_h1'] = [
      '#type' => 'item',
      '#markup' => '<hr/><h1>Symptoms</h1>',
    ];

    $eating_well_page_symptom_gallery = null;
    if ($term->field_ew_symptom_gallery_ref->target_id) {
      $eating_well_page_symptom_gallery = \Drupal\node\Entity\Node::load($term->field_ew_symptom_gallery_ref->target_id);
    }

    $form['frontend_eating_well_page_symptom_gallery'] = [
      '#type' => 'entity_autocomplete',
      '#title' => $this->t('FRONTEND_EATING_WELL_PAGE_SYMPTOM_GALLERY_LABEL'),
      '#target_type' => 'node',
      '#default_value' => ($term->field_ew_symptom_gallery_ref->target_id ? $eating_well_page_symptom_gallery : ''),
      '#selection_handler' => 'default', // Optional. The default selection handler is pre-populated to 'default'.
      '#selection_settings' => [
        'target_bundles' => ['symptom_gallery', 'content'],
      ],
    ];

    $form['video_content_h1'] = [
      '#type' => 'item',
      '#markup' => '<hr/><h1>Video content</h1>',
    ];

    $form['frontend_eating_well_page_video_content'] = [
      '#type' => 'textfield',
      '#title' => $this->t('FRONTEND_EATING_WELL_PAGE_VIDEO_CONTENT_LABEL'),
      '#description' => $this->t('DEMO: If you leave it empty, no video will be shown.'),
      '#required' => false,
      '#default_value' => ($term->field_ew_video->value ? $term->field_ew_video->value : $this->t('')),
    ];

    $form['cta_h1'] = [
      '#type' => 'item',
      '#markup' => '<hr/><h1>CTA Box</h1>',
    ];

    $form['frontend_eating_well_page_cta_title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('FRONTEND_EATING_WELL_PAGE_CTA_TITLE_LABEL'),
      '#description' => $this->t('DEMO: If you leave it empty, the original system message will be shown'),
      '#required' => false,
      '#default_value' => ($term->field_ew_cta_title->value ? $term->field_ew_cta_title->value : $this->t('')),
    ];

    $form['frontend_eating_well_page_cta_description'] = [
      '#type' => 'textarea',
      '#title' => $this->t('FRONTEND_EATING_WELL_PAGE_CTA_DESCRIPTION_LABEL'),
      '#description' => $this->t('DEMO: If you leave it empty, the original system message will be shown'),
      '#required' => false,
      '#default_value' => ($term->field_ew_cta_desc->value ? $term->field_ew_cta_desc->value : $this->t('')),
      '#attributes' => [
        'max-length' => 512,
        'maxlength' => 512,
      ],
    ];

    $form['frontend_eating_well_page_cta_button_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('FRONTEND_EATING_WELL_PAGE_CTA_BUTTON_LABEL_LABEL'),
      '#description' => $this->t('DEMO: If you leave it empty, the original system message will be shown'),
      '#required' => false,
      '#default_value' => ($term->field_ew_cta_button_label->value ? $term->field_ew_cta_button_label->value : $this->t('')),
    ];

    $form['frontend_eating_well_page_cta_button_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('FRONTEND_EATING_WELL_PAGE_CTA_BUTTON_URL_LABEL'),
      '#description' => $this->t('DEMO: If you leave it empty, the original system message will be shown'),
      '#required' => false,
      '#default_value' => ($term->field_ew_cta_button_url->value ? $term->field_ew_cta_button_url->value : $this->t('')),
    ];

    $form['references_h1'] = [
      '#type' => 'item',
      '#markup' => '<hr/><h1>References</h1>',
    ];

    $form['frontend_eating_well_page_references'] = [
      '#type' => 'textarea',
      '#title' => $this->t('FRONTEND_EATING_WELL_PAGE_REFERENCES_LABEL'),
      '#description' => $this->t('DEMO: If you leave it empty, the original system message will be shown'),
      '#required' => false,
      '#default_value' => ($term->field_eating_well_references->value ? $term->field_eating_well_references->value : $this->t('')),
      '#attributes' => [
        'max-length' => 512,
        'maxlength' => 512,
      ],
    ];

    $form['iso_h1'] = [
      '#type' => 'item',
      '#markup' => '<hr/><h1>ISO Number</h1>',
    ];

    $form['frontend_eating_well_page_iso_number'] = [
      '#type' => 'textfield',
      '#title' => $this->t('FRONTEND_EATING_WELL_PAGE_ISO_NUMBER_LABEL'),
      '#description' => $this->t('DEMO: If you leave it empty, the original system message will be shown'),
      '#required' => false,
      '#default_value' => ($term->field_ew_page_iso->value ? $term->field_ew_page_iso->value : $this->t('')),
    ];

    $form['actions'] = [
      '#type' => 'actions',
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('SUBMIT'),
    ];

    unset($term);

    return $form;
  }

  /**
   * Validate the title and the checkbox of the form
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   */
  public function validateForm(array &$form, FormStateInterface $form_state)
  {
    parent::validateForm($form, $form_state);
  }

  public function submitForm(array &$form, FormStateInterface $form_state)
  {
    $values = $form_state->getValues();
    $messenger = \Drupal::messenger();

    try {
      $term = $this->getData(true);
    } catch (\Exception $e) {
      $messenger->addError($e->getMessage());
      return $form_state->setRedirect('page_content_backoffice.eating_well_page_form');
    }

    $frontend_eating_well_page_title = $values['frontend_eating_well_page_title'];
    $frontend_eating_well_page_section_1_string_1 = $values['frontend_eating_well_page_section_1_string_1'];
    $frontend_eating_well_page_section_1_string_2 = $values['frontend_eating_well_page_section_1_string_2'];
    $frontend_eating_well_page_section_1_desc = $values['frontend_eating_well_page_section_1_desc'];
    $frontend_eating_well_page_carousel = $values['frontend_eating_well_page_carousel'];
    $frontend_eating_well_page_section_2_string_1 = $values['frontend_eating_well_page_section_2_string_1'];
    $frontend_eating_well_page_section_2_string_2 = $values['frontend_eating_well_page_section_2_string_2'];
    $frontend_eating_well_page_section_2_desc = $values['frontend_eating_well_page_section_2_desc'];
    $frontend_eating_well_card_title = $values['frontend_eating_well_page_card_title'];
    $frontend_eating_well_card_desc = $values['frontend_eating_well_page_card_description'];
    $frontend_eating_well_card_image = $values['frontend_eating_well_page_card_image'];
    $frontend_eating_well_page_section_3_string_1 = $values['frontend_eating_well_page_section_3_string_1'];
    $frontend_eating_well_page_section_3_string_2 = $values['frontend_eating_well_page_section_3_string_2'];
    $frontend_eating_well_page_section_3_desc = $values['frontend_eating_well_page_section_3_desc'];
    $frontend_eating_well_page_symptom_gallery = $values['frontend_eating_well_page_symptom_gallery'];
    $frontend_eating_well_page_video = $values['frontend_eating_well_page_video_content'];
    $frontend_eating_well_page_cta_title = $values['frontend_eating_well_page_cta_title'];
    $frontend_eating_well_page_cta_desc = $values['frontend_eating_well_page_cta_description'];
    $frontend_eating_well_page_cta_button_label = $values['frontend_eating_well_page_cta_button_label'];
    $frontend_eating_well_page_cta_button_url = $values['frontend_eating_well_page_cta_button_url'];
    $frontend_eating_well_page_references = $values['frontend_eating_well_page_references'];
    $frontend_eating_well_page_iso_number = $values['frontend_eating_well_page_iso_number'];

    $tmp = explode('/', $frontend_eating_well_page_video);
    $frontend_eating_well_page_video = end($tmp);

    $shouldSave = false;

    if (is_string($frontend_eating_well_page_video)) {
      if ($term->field_ew_video[0]->value !== $frontend_eating_well_page_video && $frontend_eating_well_page_video) {
        $vimeo_client = new VimeoController();
        $video_id = $frontend_eating_well_page_video;
        $video = $vimeo_client->isMyVideo($video_id);
        if (!$video) {
          $messenger->addError($this->t('INVALID_VIDEO'));
          $form_state->setRedirect('page_content_backoffice.eating_well_page_form');

          return true;
        } else {
          $term->set('field_ew_video', $frontend_eating_well_page_video);

          $shouldSave = true;
        }
      }
    }

    if (is_string($frontend_eating_well_page_title)) {
      if ($term->field_eating_well_page_title[0]->value !== $frontend_eating_well_page_title) {
        $term->set('field_eating_well_page_title', $frontend_eating_well_page_title);

        $shouldSave = true;
      }
    }

    if (is_string($frontend_eating_well_page_section_1_string_1)) {
      if ($term->field_ew_sec1_str1[0]->value !== $frontend_eating_well_page_section_1_string_1) {
        $term->set('field_ew_sec1_str1', $frontend_eating_well_page_section_1_string_1);

        $shouldSave = true;
      }
    }

    if (is_string($frontend_eating_well_page_section_1_string_2)) {
      if ($term->field_ew_sec1_str2[0]->value !== $frontend_eating_well_page_section_1_string_2) {
        $term->set('field_ew_sec1_str2', $frontend_eating_well_page_section_1_string_2);

        $shouldSave = true;
      }
    }

    if (is_string($frontend_eating_well_page_section_1_desc)) {
      if ($term->field_ew_sec1_desc[0]->value !== $frontend_eating_well_page_section_1_desc) {
        $term->set('field_ew_sec1_desc', $frontend_eating_well_page_section_1_desc);

        $shouldSave = true;
      }
    }

    if (is_string($frontend_eating_well_page_carousel)) {
      if (isset($frontend_eating_well_page_carousel) && !empty($frontend_eating_well_page_carousel)) {
        $term->set('field_eating_well_carousel_ref', ['target_id' => $frontend_eating_well_page_carousel]);

        $shouldSave = true;
      }
    }

    if (is_string($frontend_eating_well_page_section_2_string_1)) {
      if ($term->field_ew_sec2_str1[0]->value !== $frontend_eating_well_page_section_2_string_1) {
        $term->set('field_ew_sec2_str1', $frontend_eating_well_page_section_2_string_1);

        $shouldSave = true;
      }
    }

    if (is_string($frontend_eating_well_page_section_2_string_2)) {
      if ($term->field_ew_sec2_str2[0]->value !== $frontend_eating_well_page_section_2_string_2) {
        $term->set('field_ew_sec2_str2', $frontend_eating_well_page_section_2_string_2);

        $shouldSave = true;
      }
    }

    if (is_string($frontend_eating_well_page_section_2_desc)) {
      if ($term->field_ew_sec2_desc[0]->value !== $frontend_eating_well_page_section_2_desc) {
        $term->set('field_ew_sec2_desc', $frontend_eating_well_page_section_2_desc);

        $shouldSave = true;
      }
    }

    if (is_string($frontend_eating_well_card_title)) {
      if ($term->field_eating_well_card_title[0]->value !== $frontend_eating_well_card_title) {
        $term->set('field_eating_well_card_title', $frontend_eating_well_card_title);

        $shouldSave = true;
      }
    }

    if (is_string($frontend_eating_well_card_desc)) {
      if ($term->field_eating_well_card_desc[0]->value !== $frontend_eating_well_card_desc) {
        $term->set('field_eating_well_card_desc', $frontend_eating_well_card_desc);

        $shouldSave = true;
      }
    }

    if (isset($frontend_eating_well_card_image[0]) && !empty($frontend_eating_well_card_image[0])) {
      $file = File::load($frontend_eating_well_card_image[0]);
      $file->setPermanent();
      $file->save();

      $term->set('field_eating_well_card_image', ['target_id' => $frontend_eating_well_card_image[0]]);

      $shouldSave = true;
    }

    if (is_string($frontend_eating_well_page_section_3_string_1)) {
      if ($term->field_ew_sec3_str1[0]->value !== $frontend_eating_well_page_section_3_string_1) {
        $term->set('field_ew_sec3_str1', $frontend_eating_well_page_section_3_string_1);

        $shouldSave = true;
      }
    }

    if (is_string($frontend_eating_well_page_section_3_string_2)) {
      if ($term->field_ew_sec3_str2[0]->value !== $frontend_eating_well_page_section_3_string_2) {
        $term->set('field_ew_sec3_str2', $frontend_eating_well_page_section_3_string_2);

        $shouldSave = true;
      }
    }

    if (is_string($frontend_eating_well_page_section_3_desc)) {
      if ($term->field_ew_sec3_desc[0]->value !== $frontend_eating_well_page_section_3_desc) {
        $term->set('field_ew_sec3_desc', $frontend_eating_well_page_section_3_desc);

        $shouldSave = true;
      }
    }

    if (is_string($frontend_eating_well_page_symptom_gallery)) {
      if (isset($frontend_eating_well_page_symptom_gallery) && !empty($frontend_eating_well_page_symptom_gallery)) {
        $term->set('field_ew_symptom_gallery_ref', ['target_id' => $frontend_eating_well_page_symptom_gallery]);

        $shouldSave = true;
      }
    }

    if (is_string($frontend_eating_well_page_cta_title)) {
      if ($term->field_ew_cta_title[0]->value !== $frontend_eating_well_page_cta_title) {
        $term->set('field_ew_cta_title', $frontend_eating_well_page_cta_title);

        $shouldSave = true;
      }
    }

    if (is_string($frontend_eating_well_page_cta_desc)) {
      if ($term->field_ew_cta_desc[0]->value !== $frontend_eating_well_page_cta_desc) {
        $term->set('field_ew_cta_desc', $frontend_eating_well_page_cta_desc);

        $shouldSave = true;
      }
    }

    if (is_string($frontend_eating_well_page_cta_button_label)) {
      if ($term->field_ew_cta_button_label[0]->value !== $frontend_eating_well_page_cta_button_label) {
        $term->set('field_ew_cta_button_label', $frontend_eating_well_page_cta_button_label);

        $shouldSave = true;
      }
    }

    if (is_string($frontend_eating_well_page_cta_button_url)) {
      if ($term->field_ew_cta_button_url[0]->value !== $frontend_eating_well_page_cta_button_url) {
        $term->set('field_ew_cta_button_url', $frontend_eating_well_page_cta_button_url);

        $shouldSave = true;
      }
    }

    if (is_string($frontend_eating_well_page_references)) {
      if ($term->field_eating_well_references[0]->value !== $frontend_eating_well_page_references) {
        $term->set('field_eating_well_references', $frontend_eating_well_page_references);

        $shouldSave = true;
      }
    }

    if (is_string($frontend_eating_well_page_iso_number)) {
      if ($term->field_ew_page_iso[0]->value !== $frontend_eating_well_page_iso_number) {
        $term->set('field_ew_page_iso', $frontend_eating_well_page_iso_number);

        $shouldSave = true;
      }
    }

    try {
      if ($shouldSave) {
        $term->save();
      }
    } catch (\Exception $e) {
      $messenger->addError($e->getMessage());
      $form_state->setRedirect('page_content_backoffice.eating_well_page_form');
    }

    if ($shouldSave) {
      $messenger->addMessage($this->t('SUCCESSFULLY_SAVED'));
    } else {
      $messenger->addMessage($this->t('NOTHING_TO_SAVE'));
    }

    $form_state->setRedirect('page_content_backoffice.eating_well_page_form');
  }
}
