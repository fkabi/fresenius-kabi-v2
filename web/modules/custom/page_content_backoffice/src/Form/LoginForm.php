<?php

namespace Drupal\page_content_backoffice\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

class LoginForm extends FormBase
{

  /**
   * Returns a unique string identifying the form.
   *
   * The returned ID should be a unique string that can be a valid PHP function
   * name, since it's used in hook implementation names such as
   * hook_form_FORM_ID_alter().
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId()
  {
    return 'frontend_login_form_edit';
  }

  private function getData()
  {
    $vid = 'login_test_taxonomy';
    $properties['vid'] = $vid;

    $terms = \Drupal::entityTypeManager()
      ->getStorage('taxonomy_term')
      ->loadByProperties($properties);

    return reset($terms);
  }

  public function buildForm(array $form, FormStateInterface $form_state)
  {
    /*$form['description'] = [
      '#type' => 'item',
      '#markup' => $this->t('Please enter the title and accept the terms of use of the site.'),
    ];*/

    $form['preview'] = [
      '#type' => 'button',
      '#value' => $this->t('PREVIEW_STATE'),
      '#attributes' => array('onclick' => 'return false;'),
    ];
    $form['preview']['#attributes']['class'][] = 'preview-button';
    $form['preview']['#attributes']['class'][] = 'login-preview-button';

    $form['preview-mobile'] = [
      '#type' => 'button',
      '#value' => $this->t('PREVIEW_STATE_MOBILE'),
      '#attributes' => array('onclick' => 'return false;'),
    ];
    $form['preview-mobile']['#attributes']['class'][] = 'preview-button-mobile';
    $form['preview-mobile']['#attributes']['class'][] = 'login-preview-button-mobile';

    $term = $this->getData();

    $form['login_page_login_section_title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('LOG_IN_PAGE_LOGIN_SECTION_TITLE_LABEL'),
      '#description' => $this->t('DEMO: If you leave it empty, the original system message will be shown.'),
      '#required' => FALSE,
      '#default_value' => ($term->field_login_section_title->value ? $term->field_login_section_title->value : $this->t('LOG_IN')),
    ];

    $form['login_page_login_section_patient_restricted_message'] = [
      '#type' => 'textfield',
      '#title' => $this->t('LOG_IN_PAGE_LOGIN_SECTION_PATIENT_RESTRICTED_MESSAGE_LABEL'),
      '#description' => $this->t('DEMO: If you leave it empty, the original system message will be shown. This item will not be displayed on the preview.'),
      '#required' => FALSE,
      '#default_value' => ($term->field_login_sec_patient_rstr_msg->value ? $term->field_login_sec_patient_rstr_msg->value : $this->t('ACCESS_TO_THIS_CONTENT_IS_RESTRICTED')),
    ];

    $form['login_page_login_section_hcp_restricted_message'] = [
      '#type' => 'textfield',
      '#title' => $this->t('LOG_IN_PAGE_LOGIN_SECTION_HCP_RESTRICTED_MESSAGE_LABEL'),
      '#description' => $this->t('DEMO: If you leave it empty, the original system message will be shown. This item will not be displayed on the preview.'),
      '#required' => FALSE,
      '#default_value' => ($term->field_login_sec_hcp_rstr_msg->value ? $term->field_login_sec_hcp_rstr_msg->value : $this->t('ACCESS_TO_THIS_CONTENT_IS_RESTRICTED')),
    ];

    $form['login_page_login_section_email_placeholder'] = [
      '#type' => 'textfield',
      '#title' => $this->t('LOG_IN_PAGE_LOGIN_SECTION_EMAIL_PLACEHOLDER_LABEL'),
      '#description' => $this->t('DEMO: If you leave it empty, the original system message will be shown.'),
      '#required' => FALSE,
      '#default_value' => ($term->field_login_section_email->value ? $term->field_login_section_email->value : $this->t('EMAIL')),
    ];

    $form['login_page_login_section_password_placeholder'] = [
      '#type' => 'textfield',
      '#title' => $this->t('LOG_IN_PAGE_LOGIN_SECTION_PASSWORD_PLACEHOLDER_LABEL'),
      '#description' => $this->t('DEMO: If you leave it empty, the original system message will be shown.'),
      '#required' => FALSE,
      '#default_value' => ($term->field_login_section_password->value ? $term->field_login_section_password->value : $this->t('PASSWORD')),
    ];

    $form['login_page_login_section_forgot_email_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('LOG_IN_PAGE_LOGIN_SECTION_FORGOT_EMAIL_LABEL'),
      '#description' => $this->t('DEMO: If you leave it empty, the original system message will be shown.'),
      '#required' => FALSE,
      '#default_value' => ($term->field_login_section_forgot_email->value ? $term->field_login_section_forgot_email->value : $this->t('FORGOT_EMAIL')),
    ];

    $form['login_page_login_section_forgot_password_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('LOG_IN_PAGE_LOGIN_SECTION_FORGOT_PASSWORD_LABEL'),
      '#description' => $this->t('DEMO: If you leave it empty, the original system message will be shown.'),
      '#required' => FALSE,
      '#default_value' => ($term->field_login_section_forgot_pass->value ? $term->field_login_section_forgot_pass->value : $this->t('FORGOT_PASSWORD')),
    ];

    $form['login_page_login_section_submit_button_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('LOG_IN_PAGE_LOGIN_SECTION_SUBMIT_BUTTON_LABEL'),
      '#description' => $this->t('DEMO: If you leave it empty, the original system message will be shown.'),
      '#required' => FALSE,
      '#default_value' => ($term->field_login_section_sbmt_btn_lab->value ? $term->field_login_section_sbmt_btn_lab->value : $this->t('LOG_IN')),
    ];

    /*$form['accept'] = array(
      '#type' => 'checkbox',
      '#title' => $this
        ->t('I accept the terms of use of the site'),
      '#description' => $this->t('Please read and accept the terms of use'),
    );*/

    $form['login_page_register_section_title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('LOG_IN_PAGE_REGISTER_SECTION_TITLE_LABEL'),
      '#description' => $this->t('DEMO: If you leave it empty, the original system message will be shown.'),
      '#required' => FALSE,
      '#default_value' => ($term->field_login_reg_sec_title->value ? $term->field_login_reg_sec_title->value : $this->t('REGISTER_NOW_FOR_ADDITIONAL_BENEFITS')),
    ];

    $form['login_page_register_section_content'] = [
      '#type' => 'text_format',
      '#title' => $this->t('LOG_IN_PAGE_REGISTER_SECTION_CONTENT_LABEL'),
      '#description' => $this->t('DEMO: If you leave this empty, no content will be shown.'),
      '#default_value' => ($term->field_login_reg_sec_content->value ? $term->field_login_reg_sec_content->value : ''),
      '#format' => 'full_html',
    ];

    $form['login_page_register_section_patient_register_button_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('LOG_IN_PAGE_REGISTER_SECTION_PATIENT_REGISTER_BUTTON_LABEL'),
      '#description' => $this->t('DEMO: If you leave it empty, the original system message will be shown.'),
      '#required' => FALSE,
      '#default_value' => ($term->field_login_reg_sec_patient_reg->value ? $term->field_login_reg_sec_patient_reg->value : $this->t('REGISTER_AS_A_PATIENT')),
    ];

    $form['login_page_register_section_hcp_register_button_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('LOG_IN_PAGE_REGISTER_SECTION_HCP_REGISTER_BUTTON_LABEL'),
      '#description' => $this->t('DEMO: If you leave it empty, the original system message will be shown.'),
      '#required' => FALSE,
      '#default_value' => ($term->field_login_reg_sec_hcp_reg->value ? $term->field_login_reg_sec_hcp_reg->value : $this->t('REGISTER_AS_AN_HCP')),
    ];

    $form['login_page_register_section_description'] = [
      '#type' => 'textarea',
      '#title' => $this->t('LOG_IN_PAGE_REGISTER_SECTION_DESCRIPTION_LABEL'),
      '#description' => $this->t('DEMO: If you leave it empty, no content will be shown.'),
      '#required' => FALSE,
      '#default_value' => ($term->field_login_reg_sec_desc->value ? $term->field_login_reg_sec_desc->value : $this->t('YOU_WILL_NEED_THE_UNIQUE_CODE_PROVIDED_ON_THE_ACCESS_CARD_IN_YOUR_STARTED_PACK')),
    ];

    $form['login_page_patient_restricted_register_section_content'] = [
      '#type' => 'text_format',
      '#title' => $this->t('LOG_IN_PAGE_REGISTER_SECTION_PATIENT_RESTRICTED_CONTENT_LABEL'),
      '#description' => $this->t('DEMO: If you leave this empty, no content will be shown. This item will not be displayed on the preview.'),
      '#default_value' => ($term->field_login_reg_patient_rstr_con->value ? $term->field_login_reg_patient_rstr_con->value : ''),
      '#format' => 'full_html',
    ];

    $form['login_page_hcp_restricted_register_section_content'] = [
      '#type' => 'text_format',
      '#title' => $this->t('LOG_IN_PAGE_REGISTER_SECTION_HCP_RESTRICTED'),
      '#description' => $this->t('DEMO: If you leave this empty, no content will be shown. This item will not be displayed on the preview.'),
      '#default_value' => ($term->field_login_reg_hcp_rstr_con->value ? $term->field_login_reg_hcp_rstr_con->value : ''),
      '#format' => 'full_html',
    ];

    // Group submit handlers in an actions element with a key of "actions" so
    // that it gets styled correctly, and so that other modules may add actions
    // to the form. This is not required, but is convention.
    $form['actions'] = [
      '#type' => 'actions',
    ];

    // Add a submit button that handles the submission of the form.
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('SUBMIT'),
    ];

    unset($term);

    return $form;
  }

  /**
   * Validate the title and the checkbox of the form
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   */
  public function validateForm(array &$form, FormStateInterface $form_state)
  {
    parent::validateForm($form, $form_state);
  }

  public function submitForm(array &$form, FormStateInterface $form_state)
  {
    $values = $form_state->getValues();
    $term = $this->getData();

    $login_page_login_section_title = $values['login_page_login_section_title'];
    $login_page_login_section_patient_restricted_message = $values['login_page_login_section_patient_restricted_message'];
    $login_page_login_section_hcp_restricted_message = $values['login_page_login_section_hcp_restricted_message'];
    $login_page_login_section_email_placeholder = $values['login_page_login_section_email_placeholder'];
    $login_page_login_section_password_placeholder = $values['login_page_login_section_password_placeholder'];
    $login_page_login_section_forgot_email_label = $values['login_page_login_section_forgot_email_label'];
    $login_page_login_section_forgot_password_label = $values['login_page_login_section_forgot_password_label'];
    $login_page_login_section_submit_button_label = $values['login_page_login_section_submit_button_label'];
    $login_page_register_section_title = $values['login_page_register_section_title'];
    $login_page_register_section_content = $values['login_page_register_section_content']['value'];
    $login_page_register_section_patient_register_button_label = $values['login_page_register_section_patient_register_button_label'];
    $login_page_register_section_hcp_register_button_label = $values['login_page_register_section_hcp_register_button_label'];
    $login_page_register_section_description = $values['login_page_register_section_description'];
    $login_page_register_section_patient_restricted_content = $values['login_page_patient_restricted_register_section_content']['value'];
    $login_page_register_section_hcp_restricted_content = $values['login_page_hcp_restricted_register_section_content']['value'];

    $shouldSave = false;

    if (is_string($login_page_login_section_title)) {
      if ($term->field_login_section_title[0]->value !== $login_page_login_section_title) {
        $term->set('field_login_section_title', $login_page_login_section_title);

        $shouldSave = true;
      }
    }

    if (is_string($login_page_login_section_patient_restricted_message)) {
      if ($term->field_login_sec_patient_rstr_msg[0]->value !== $login_page_login_section_patient_restricted_message) {
        $term->set('field_login_sec_patient_rstr_msg', $login_page_login_section_patient_restricted_message);

        $shouldSave = true;
      }
    }

    if (is_string($login_page_login_section_hcp_restricted_message)) {
      if ($term->field_login_sec_hcp_rstr_msg[0]->value !== $login_page_login_section_hcp_restricted_message) {
        $term->set('field_login_sec_hcp_rstr_msg', $login_page_login_section_hcp_restricted_message);

        $shouldSave = true;
      }
    }

    if (is_string($login_page_login_section_email_placeholder)) {
      if ($term->field_login_section_email[0]->value !== $login_page_login_section_email_placeholder) {
        $term->set('field_login_section_email', $login_page_login_section_email_placeholder);

        $shouldSave = true;
      }
    }

    if (is_string($login_page_login_section_password_placeholder)) {
      if ($term->field_login_section_password[0]->value !== $login_page_login_section_password_placeholder) {
        $term->set('field_login_section_password', $login_page_login_section_password_placeholder);

        $shouldSave = true;
      }
    }

    if (is_string($login_page_login_section_forgot_email_label)) {
      if ($term->field_login_section_forgot_email[0]->value !== $login_page_login_section_forgot_email_label) {
        $term->set('field_login_section_forgot_email', $login_page_login_section_forgot_email_label);

        $shouldSave = true;
      }
    }

    if (is_string($login_page_login_section_forgot_password_label)) {
      if ($term->field_login_section_forgot_pass[0]->value !== $login_page_login_section_forgot_password_label) {
        $term->set('field_login_section_forgot_pass', $login_page_login_section_forgot_password_label);

        $shouldSave = true;
      }
    }

    if (is_string($login_page_login_section_submit_button_label)) {
      if ($term->field_login_section_sbmt_btn_lab[0]->value !== $login_page_login_section_submit_button_label) {
        $term->set('field_login_section_sbmt_btn_lab', $login_page_login_section_submit_button_label);

        $shouldSave = true;
      }
    }

    if (is_string($login_page_register_section_title)) {
      if ($term->field_login_reg_sec_title[0]->value !== $login_page_register_section_title) {
        $term->set('field_login_reg_sec_title', $login_page_register_section_title);

        $shouldSave = true;
      }
    }

    if (is_string($login_page_register_section_content)) {
      if ($term->field_login_reg_sec_content[0]->value !== $login_page_register_section_content) {
        $term->set('field_login_reg_sec_content', $login_page_register_section_content);

        $shouldSave = true;
      }
    }

    if (is_string($login_page_register_section_patient_register_button_label)) {
      if ($term->field_login_reg_sec_patient_reg[0]->value !== $login_page_register_section_patient_register_button_label) {
        $term->set('field_login_reg_sec_patient_reg', $login_page_register_section_patient_register_button_label);

        $shouldSave = true;
      }
    }

    if (is_string($login_page_register_section_hcp_register_button_label)) {
      if ($term->field_login_reg_sec_hcp_reg[0]->value !== $login_page_register_section_hcp_register_button_label) {
        $term->set('field_login_reg_sec_hcp_reg', $login_page_register_section_hcp_register_button_label);

        $shouldSave = true;
      }
    }

    if (is_string($login_page_register_section_description)) {
      if ($term->field_login_reg_sec_desc[0]->value !== $login_page_register_section_description) {
        $term->set('field_login_reg_sec_desc', $login_page_register_section_description);

        $shouldSave = true;
      }
    }

    if (is_string($login_page_register_section_patient_restricted_content)) {
      if ($term->field_login_reg_patient_rstr_con[0]->value !== $login_page_register_section_patient_restricted_content) {
        $term->set('field_login_reg_patient_rstr_con', $login_page_register_section_patient_restricted_content);

        $shouldSave = true;
      }
    }

    if (is_string($login_page_register_section_hcp_restricted_content)) {
      if ($term->field_login_reg_hcp_rstr_con[0]->value !== $login_page_register_section_hcp_restricted_content) {
        $term->set('field_login_reg_hcp_rstr_con', $login_page_register_section_hcp_restricted_content);

        $shouldSave = true;
      }
    }

    $messenger = \Drupal::messenger();

    try {
      if ($shouldSave) {
        $term->save();
      }
    } catch (\Exception $e) {
      $messenger->addError($e->getMessage());
      $form_state->setRedirect('page_content_backoffice.login_page_form');
    }

    if ($shouldSave) {
      $messenger->addMessage($this->t('SUCCESSFULLY_SAVED'));
    } else {
      $messenger->addMessage($this->t('NOTHING_TO_SAVE'));
    }

    $form_state->setRedirect('page_content_backoffice.login_page_form');
  }
}
