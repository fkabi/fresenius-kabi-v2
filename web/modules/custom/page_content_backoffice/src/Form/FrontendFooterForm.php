<?php

namespace Drupal\page_content_backoffice\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\file\Entity\File;

class FrontendFooterForm extends FormBase
{

  /**
   * Returns a unique string identifying the form.
   *
   * The returned ID should be a unique string that can be a valid PHP function
   * name, since it's used in hook implementation names such as
   * hook_form_FORM_ID_alter().
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId()
  {
    return 'frontend_footer_form_edit';
  }

  private function getData()
  {
    $vid = 'frontend_footer_taxonomy';
    $properties['vid'] = $vid;

    $terms = \Drupal::entityTypeManager()
      ->getStorage('taxonomy_term')
      ->loadByProperties($properties);

    return reset($terms);
  }

  public function buildForm(array $form, FormStateInterface $form_state)
  {
    $form['preview'] = [
      '#type' => 'button',
      '#value' => $this->t('PREVIEW_STATE'),
      '#attributes' => array('onclick' => 'return false;'),
    ];
    $form['preview']['#attributes']['class'][] = 'preview-button';
    $form['preview']['#attributes']['class'][] = 'footer-preview-button';

    $form['preview-mobile'] = [
      '#type' => 'button',
      '#value' => $this->t('PREVIEW_STATE_MOBILE'),
      '#attributes' => array('onclick' => 'return false;'),
    ];
    $form['preview-mobile']['#attributes']['class'][] = 'preview-button-mobile';
    $form['preview-mobile']['#attributes']['class'][] = 'footer-preview-button-mobile';

    $term = $this->getData();

    $form['frontend_footer_disclaimer_situational'] = [
      '#type' => 'text_format',
      '#title' => $this->t('FRONTEND_FOOTER_DISCLAIMER_SITUATIONAL_LABEL'),
      '#description' => $this->t('DEMO: If you leave it empty, the original system message will be shown. Only displayed on certain pages.'),
      '#required' => FALSE,
      '#default_value' => ($term->field_footer_disclaimer_situatio->value ? $term->field_footer_disclaimer_situatio->value : ''),
      '#format' => 'full_html',
    ];

    $form['frontend_footer_disclaimer_normal'] = [
      '#type' => 'text_format',
      '#title' => $this->t('FRONTEND_FOOTER_DISCLAIMER_NORMAL_LABEL'),
      '#description' => $this->t('DEMO: If you leave it empty, the original system message will be shown. Displayed on all pages.'),
      '#required' => FALSE,
      '#default_value' => ($term->field_footer_disclaimer_normal->value ? $term->field_footer_disclaimer_normal->value : ''),
      '#format' => 'full_html',
    ];

    $form['frontend_footer_date_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('FRONTEND_FOOTER_DATE_LABEL_LABEL'),
      '#description' => $this->t('DEMO: If you leave it empty, the original system message will be shown.'),
      '#required' => FALSE,
      '#default_value' => ($term->field_footer_date_label->value ? $term->field_footer_date_label->value : $this->t('DATE_OF_PREPARATION')),
    ];

    /*$form['frontend_footer_date'] = [
      '#type' => 'date',
      '#title' => $this->t('FRONTEND_FOOTER_DATE_LABEL'),
      '#description' => $this->t('DEMO: If you leave it empty, the original system message will be shown.'),
      '#required' => FALSE,
      '#default_value' => ($term->field_footer_date->value ? $term->field_footer_date->value : null),
    ];*/

    /*$form['frontend_footer_logo'] = [
      '#type' => 'managed_file',
      '#title' => $this->t('FRONTEND_FOOTER_LOGO_LABEL'),
      '#description' => $this->t('DEMO: If you leave it empty, the original system message will be shown.'),
      '#required' => FALSE,
      '#size' => 40,
      '#name' => 'frontend_footer_logo',
      '#upload_location' => 'public://footer_logo',
      '#theme' => 'image_widget',
      '#preview_image_style' => 'medium',
    ];*/

    $current_logo_id = $term->field_footer_logo[0]->target_id;
    $current_Logo = File::load($current_logo_id);
    $markup = '';
    if(isset($current_Logo)) {
      $uri = $current_Logo->getFileUri();
      $stream_wrapper_manager = \Drupal::service('stream_wrapper_manager')->getViaUri($uri);
      $file_path = $stream_wrapper_manager->realpath(); 
      $file_path = 'sites\default\files\footer_logo' . DIRECTORY_SEPARATOR . pathinfo($file_path)['basename'];
      $markup = '<img src="' . base_path() . $file_path . '" alt="'. $this->t('CURRENT_LOGO') .'" style="width:100px;height:auto;" name="frontend_footer_current_logo" id="frontend_footer_current_logo">';
    }

    $form['frontend_footer_current_logo'] = array(
      '#type' => 'item',
      '#markup' => $markup,
      '#prefix' => '<div class="current-logo">',
      '#suffix' => '</div>',
      '#title' => $this->t('FRONTEND_FOOTER_CURRENT_LOGO_LABEL'),
      '#theme_wrappers' => array('form_element', 'views_ui_container'),
      '#attributes' => array('class' => array('container-inline', 'views-add-form-selected')),
    );

    $sub_nav = \Drupal::menuTree()->load('disclaimer', new \Drupal\Core\Menu\MenuTreeParameters());
    $menu_tree2 = null;
    $this->generateSubMenuTree($menu_tree2, $sub_nav);
    $footer_menu = $menu_tree2['/']['child'];

    $footer_menu_markup = '';
    foreach ($footer_menu as $menu) {
      $footer_menu_markup .= '<div class="menu-item">'.$menu['label'].'</div>';
    }

    $form['frontend_footer_menu-items'] = array(
      '#type' => 'item',
      '#markup' => $footer_menu_markup,
      '#prefix' => '<div class="menu-items hidden">',
      '#suffix' => '</div>',
      '#theme_wrappers' => array('form_element', 'views_ui_container'),
      '#attributes' => array('class' => array('container-inline', 'views-add-form-selected')),
    );

    $form['actions'] = [
      '#type' => 'actions',
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('SUBMIT'),
    ];

    unset($term);

    return $form;
  }

  /**
   * Validate the title and the checkbox of the form
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   */
  public function validateForm(array &$form, FormStateInterface $form_state)
  {
    parent::validateForm($form, $form_state);
  }

  public function submitForm(array &$form, FormStateInterface $form_state)
  {
    $values = $form_state->getValues();
    $term = $this->getData();

    $frontend_footer_disclaimer_situational = $values['frontend_footer_disclaimer_situational']['value'];
    $frontend_footer_disclaimer_normal = $values['frontend_footer_disclaimer_normal']['value'];
    $frontend_footer_date_label = $values['frontend_footer_date_label'];
    $frontend_footer_date = $values['frontend_footer_date'];
    $frontend_footer_logo = $values['frontend_footer_logo'];

    $shouldSave = false;

    if (is_string($frontend_footer_disclaimer_situational)) {
      if ($term->field_footer_disclaimer_situatio[0]->value !== $frontend_footer_disclaimer_situational) {
        $term->set('field_footer_disclaimer_situatio', $frontend_footer_disclaimer_situational);

        $shouldSave = true;
      }
    }

    if (is_string($frontend_footer_disclaimer_normal)) {
      if ($term->field_footer_disclaimer_normal[0]->value !== $frontend_footer_disclaimer_normal) {
        $term->set('field_footer_disclaimer_normal', $frontend_footer_disclaimer_normal);

        $shouldSave = true;
      }
    }

    if (is_string($frontend_footer_date_label)) {
      if ($term->field_footer_date_label[0]->value !== $frontend_footer_date_label) {
        $term->set('field_footer_date_label', $frontend_footer_date_label);

        $shouldSave = true;
      }
    }

    if (is_string($frontend_footer_date)) {
      if ($term->field_footer_date[0]->value !== $frontend_footer_date) {
        $term->set('field_footer_date', $frontend_footer_date);

        $shouldSave = true;
      }
    }

    if (isset($frontend_footer_logo[0]) && !empty($frontend_footer_logo[0])) {
      $file = File::load($frontend_footer_logo[0]);
      $file->setPermanent();
      $file->save();

      $term->set('field_footer_logo' , ['target_id' => $frontend_footer_logo[0]]);

      $shouldSave = true;
    }

    $messenger = \Drupal::messenger();

    try {
      if ($shouldSave) {
        $term->save();
      }
    } catch (\Exception $e) {
      $messenger->addError($e->getMessage());
      $form_state->setRedirect('page_content_backoffice.frontend_footer_form');
    }

    if ($shouldSave) {
      $messenger->addMessage($this->t('SUCCESSFULLY_SAVED'));
    } else {
      $messenger->addMessage($this->t('NOTHING_TO_SAVE'));
    }

    $form_state->setRedirect('page_content_backoffice.frontend_footer_form');
  }

  private function generateSubMenuTree(&$output, &$input, $parent = FALSE)
  {
    $input = array_values($input);
    foreach ($input as $key => $item) {
      if ($item->link->isEnabled()) {
        $name = $item->link->getTitle();
        $url = $item->link->getUrlObject();
        $url_string = $url->toString();
        $tmp = explode('/', $url_string);

        if (count($tmp) < 3) {
          $url_string = $key = '/';
        } else {
          end($tmp);
          $key = prev($tmp);
          $url_string = "/{$key}";
        }

        if ($parent === FALSE) {
          $output[$key] = [
            'label' => $name,
            'url' => $url_string
          ];
        } else {
          $parent = 'submenu-' . $parent;
          $output['child'][$key] = [
            'label' => $name,
            'url' => $url_string
          ];
        }

        if ($item->hasChildren) {
          if ($item->depth == 1) {
            $this->generateSubMenuTree($output[$key], $item->subtree, $key);
          } else {
            $this->generateSubMenuTree($output['child'][$key], $item->subtree, $key);
          }
        }
      }
    }
  }
}
