<?php

namespace Drupal\page_content_backoffice\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\file\Entity\File;

class FrontendPoliciesPageForm extends FormBase
{

  /**
   * Returns a unique string identifying the form.
   *
   * The returned ID should be a unique string that can be a valid PHP function
   * name, since it's used in hook implementation names such as
   * hook_form_FORM_ID_alter().
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId()
  {
    return 'frontend_policies_page_form_edit';
  }

  private function getData($saving = false)
  {
    $vid = 'frontend_policies_taxonomy';
    $properties['vid'] = $vid;

    $terms = \Drupal::entityTypeManager()
      ->getStorage('taxonomy_term')
      ->loadByProperties($properties);

    if ($saving && empty($terms)) {
      throw new \Exception("Internal error! Taxonomy Term not exists!");
    }

    return reset($terms);
  }

  public function buildForm(array $form, FormStateInterface $form_state)
  {
    $term = $this->getData();

    $form['privacy_policy_h1'] = [
      '#type' => 'item',
      '#markup' => '<h1>Privacy Policy</h1>',
    ];

    $form['frontend_policies_page_privacy_title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('FRONTEND_POLICIES_PAGE_PRIVACY_POLICY_TITLE_LABEL'),
      '#description' => $this->t('DEMO: If you leave it empty, the original system message will be shown'),
      '#required' => false,
      '#default_value' => ($term->field_pols_priv_title->value ? $term->field_pols_priv_title->value : $this->t('PRIVACY_POLICY')),
    ];

    $form['frontend_policies_page_privacy_content'] = [
      '#type' => 'text_format',
      '#title' => $this->t('FRONTEND_POLICIES_PAGE_PRIVACY_POLICY_CONTENT_LABEL'),
      '#description' => $this->t('DEMO: If you leave it empty, the original system message will be shown'),
      '#required' => false,
      '#default_value' => ($term->field_pols_priv_cont->value ? $term->field_pols_priv_cont->value : ''),
      '#attributes' => [
        'max-length' => 1024,
        'maxlength' => 1024,
        'rows' => 20,
      ],
      '#format' => 'full_html',
    ];

    $form['frontend_policies_page_privacy_iso_number'] = [
      '#type' => 'textfield',
      '#title' => $this->t('FRONTEND_POLICIES_PAGE_PRIVACY_POLICY_ISO_NUMBER_LABEL'),
      '#description' => $this->t('DEMO: If you leave it empty, the original system message will be shown'),
      '#required' => false,
      '#default_value' => ($term->field_pols_priv_page_iso->value ? $term->field_pols_priv_page_iso->value : $this->t('')),
    ];

    $form['cache_policy_h1'] = [
      '#type' => 'item',
      '#markup' => '<hr/><h1>Cache Policy</h1>',
    ];

    $form['frontend_policies_page_cache_content'] = [
      '#type' => 'text_format',
      '#title' => $this->t('FRONTEND_POLICIES_PAGE_CACHE_POLICY_CONTENT_LABEL'),
      '#description' => $this->t('DEMO: If you leave it empty, the original system message will be shown'),
      '#required' => false,
      '#default_value' => ($term->field_pols_cach_cont->value ? $term->field_pols_cach_cont->value : ''),
      '#attributes' => [
        'max-length' => 1024,
        'maxlength' => 1024,
        'rows' => 10,
      ],
      '#format' => 'full_html',
    ];

    $form['frontend_policies_page_cache_cancel_button'] = [
      '#type' => 'textfield',
      '#title' => $this->t('FRONTEND_POLICIES_PAGE_CACHE_POLICY_CANCEL_BUTTON_LABEL'),
      '#description' => $this->t('DEMO: If you leave it empty, the original system message will be shown'),
      '#required' => false,
      '#default_value' => ($term->field_pols_cach_cb->value ? $term->field_pols_cach_cb->value : $this->t('CANCEL')),
    ];

    $form['frontend_policies_page_cache_submit_button'] = [
      '#type' => 'textfield',
      '#title' => $this->t('FRONTEND_POLICIES_PAGE_CACHE_POLICY_SUBMIT_BUTTON_LABEL'),
      '#description' => $this->t('DEMO: If you leave it empty, the original system message will be shown'),
      '#required' => false,
      '#default_value' => ($term->field_pols_cach_sb->value ? $term->field_pols_cach_sb->value : $this->t('SUBMIT')),
    ];

    $form['actions'] = [
      '#type' => 'actions',
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('SUBMIT'),
    ];

    unset($term);

    return $form;
  }

  /**
   * Validate the title and the checkbox of the form
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   */
  public function validateForm(array &$form, FormStateInterface $form_state)
  {
    parent::validateForm($form, $form_state);
  }

  public function submitForm(array &$form, FormStateInterface $form_state)
  {
    $values = $form_state->getValues();
    $messenger = \Drupal::messenger();

    try {
      $term = $this->getData(true);
    } catch (\Exception $e) {
      $messenger->addError($e->getMessage());
      return $form_state->setRedirect('page_content_backoffice.policies_page_form');
    }

    $frontend_policies_page_privacy_policy_title = $values['frontend_policies_page_privacy_title'];
    $frontend_policies_page_privacy_policy_content = $values['frontend_policies_page_privacy_content']['value'];
    $frontend_policies_page_privacy_policy_iso_number = $values['frontend_policies_page_privacy_iso_number'];
    $frontend_policies_page_cache_policy_content = $values['frontend_policies_page_cache_content']['value'];
    $frontend_policies_page_cache_policy_cancel_button = $values['frontend_policies_page_cache_cancel_button'];
    $frontend_policies_page_cache_policy_submit_button = $values['frontend_policies_page_cache_submit_button'];

    $shouldSave = false;

    if (is_string($frontend_policies_page_privacy_policy_title)) {
      if ($term->field_pols_priv_title[0]->value !== $frontend_policies_page_privacy_policy_title) {
        $term->set('field_pols_priv_title', $frontend_policies_page_privacy_policy_title);

        $shouldSave = true;
      }
    }

    if (is_string($frontend_policies_page_privacy_policy_content)) {
      if ($term->field_pols_priv_cont[0]->value !== $frontend_policies_page_privacy_policy_content) {
        $term->set('field_pols_priv_cont', $frontend_policies_page_privacy_policy_content);

        $shouldSave = true;
      }
    }

    if (is_string($frontend_policies_page_privacy_policy_iso_number)) {
      if ($term->field_pols_priv_page_iso[0]->value !== $frontend_policies_page_privacy_policy_iso_number) {
        $term->set('field_pols_priv_page_iso', $frontend_policies_page_privacy_policy_iso_number);

        $shouldSave = true;
      }
    }

    if (is_string($frontend_policies_page_cache_policy_content)) {
      if ($term->field_pols_cach_cont[0]->value !== $frontend_policies_page_cache_policy_content) {
        $term->set('field_pols_cach_cont', $frontend_policies_page_cache_policy_content);

        $shouldSave = true;
      }
    }

    if (is_string($frontend_policies_page_cache_policy_cancel_button)) {
      if ($term->field_pols_cach_cb[0]->value !== $frontend_policies_page_cache_policy_cancel_button) {
        $term->set('field_pols_cach_cb', $frontend_policies_page_cache_policy_cancel_button);

        $shouldSave = true;
      }
    }

    if (is_string($frontend_policies_page_cache_policy_submit_button)) {
      if ($term->field_pols_cach_sb[0]->value !== $frontend_policies_page_cache_policy_submit_button) {
        $term->set('field_pols_cach_sb', $frontend_policies_page_cache_policy_submit_button);

        $shouldSave = true;
      }
    }

    try {
      if ($shouldSave) {
        $term->save();
      }
    } catch (\Exception $e) {
      $messenger->addError($e->getMessage());
      $form_state->setRedirect('page_content_backoffice.policies_page_form');
    }

    if ($shouldSave) {
      $messenger->addMessage($this->t('SUCCESSFULLY_SAVED'));
    } else {
      $messenger->addMessage($this->t('NOTHING_TO_SAVE'));
    }

    $form_state->setRedirect('page_content_backoffice.policies_page_form');
  }
}
