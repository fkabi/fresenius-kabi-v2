<?php

namespace Drupal\page_content_backoffice\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\file\Entity\File;

class FrontendRestrictedPopupForm extends FormBase
{

  /**
   * Returns a unique string identifying the form.
   *
   * The returned ID should be a unique string that can be a valid PHP function
   * name, since it's used in hook implementation names such as
   * hook_form_FORM_ID_alter().
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId()
  {
    return 'frontend_restricted_popup_form_edit';
  }

  private function getData($saving = false)
  {
    $vid = 'frontend_rstr_popup_taxonomy';
    $properties['vid'] = $vid;

    $terms = \Drupal::entityTypeManager()
      ->getStorage('taxonomy_term')
      ->loadByProperties($properties);

    if ($saving && empty($terms)) {
      throw new \Exception("Internal error! Taxonomy Term not exists!");
    }

    return reset($terms);
  }

  public function buildForm(array $form, FormStateInterface $form_state)
  {
    $form['preview'] = [
      '#type' => 'button',
      '#value' => $this->t('PREVIEW_STATE'),
      '#attributes' => array('onclick' => 'return false;'),
    ];
    $form['preview']['#attributes']['class'][] = 'preview-button';
    $form['preview']['#attributes']['class'][] = 'restricted-popup-preview-button';

    $form['preview-mobile'] = [
      '#type' => 'button',
      '#value' => $this->t('PREVIEW_STATE_MOBILE'),
      '#attributes' => array('onclick' => 'return false;'),
    ];
    $form['preview-mobile']['#attributes']['class'][] = 'preview-button-mobile';
    $form['preview-mobile']['#attributes']['class'][] = 'restricted-popup-preview-button-mobile';

    $term = $this->getData();

    $form['frontend_restricted_popup_title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('FRONTEND_RESTRICTED_PAGE_TITLE_LABEL'),
      '#description' => $this->t('DEMO: If you leave it empty, the original system message will be shown'),
      '#required' => FALSE,
      '#default_value' => ($term->field_rsrt_popup_title->value ? $term->field_rsrt_popup_title->value : $this->t('ACCESS_ALL_AREAS')),
    ];

    $form['frontend_restricted_popup_content'] = [
      '#type' => 'text_format',
      '#title' => $this->t('FRONTEND_RESTRICTED_PAGE_CONTENT_LABEL'),
      '#default_value' => ($term->field_rstr_popup_content->value ? $term->field_rstr_popup_content->value : ''),
      '#format' => 'full_html',
    ];

    $form['frontend_restricted_popup_cancel_cta_button_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('FRONTEND_RESTRICTED_PAGE_CANCEL_CTA_BUTTON_LABEL_LABEL'),
      '#description' => $this->t('DEMO: If you leave it empty, the original system message will be shown'),
      '#required' => FALSE,
      '#default_value' => ($term->field_rsrt_popup_cancel_label->value ? $term->field_rsrt_popup_cancel_label->value : $this->t('NOT_NOW')),
    ];

    $form['frontend_restricted_popup_submit_cta_button_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('FRONTEND_RESTRICTED_PAGE_SUBMIT_CTA_BUTTON_LABEL_LABEL'),
      '#description' => $this->t('DEMO: If you leave it empty, the original system message will be shown'),
      '#required' => FALSE,
      '#default_value' => ($term->field_rsrt_popup_submit_label->value ? $term->field_rsrt_popup_submit_label->value : $this->t('LOGIN_SLASH_REGISTER')),
    ];

    $form['actions'] = [
      '#type' => 'actions',
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('SUBMIT'),
    ];

    unset($term);

    return $form;
  }

  /**
   * Validate the title and the checkbox of the form
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   */
  public function validateForm(array &$form, FormStateInterface $form_state)
  {
    parent::validateForm($form, $form_state);
  }

  public function submitForm(array &$form, FormStateInterface $form_state)
  {
    $values = $form_state->getValues();
    $messenger = \Drupal::messenger();

    try {
      $term = $this->getData(true);
    } catch (\Exception $e) {
      $messenger->addError($e->getMessage());
      return $form_state->setRedirect('page_content_backoffice.frontend_restricted_popup_form');
    }

    $frontend_restricted_popup_title = $values['frontend_restricted_popup_title'];
    $frontend_restricted_popup_content = $values['frontend_restricted_popup_content']['value'];
    $frontend_restricted_popup_cancel_cta_label = $values['frontend_restricted_popup_cancel_cta_button_label'];
    $frontend_restricted_popup_submit_cta_label = $values['frontend_restricted_popup_submit_cta_button_label'];

    $shouldSave = false;

    if (is_string($frontend_restricted_popup_title)) {
      if ($term->field_rsrt_popup_title[0]->value !== $frontend_restricted_popup_title) {
        $term->set('field_rsrt_popup_title', $frontend_restricted_popup_title);

        $shouldSave = true;
      }
    }

    if (is_string($frontend_restricted_popup_content)) {
      if ($term->field_rstr_popup_content[0]->value !== $frontend_restricted_popup_content) {
        $term->set('field_rstr_popup_content', $frontend_restricted_popup_content);

        $shouldSave = true;
      }
    }

    if (is_string($frontend_restricted_popup_cancel_cta_label)) {
      if ($term->field_rsrt_popup_cancel_label[0]->value !== $frontend_restricted_popup_cancel_cta_label) {
        $term->set('field_rsrt_popup_cancel_label', $frontend_restricted_popup_cancel_cta_label);

        $shouldSave = true;
      }
    }

    if (is_string($frontend_restricted_popup_submit_cta_label)) {
      if ($term->field_rsrt_popup_submit_label[0]->value !== $frontend_restricted_popup_submit_cta_label) {
        $term->set('field_rsrt_popup_submit_label', $frontend_restricted_popup_submit_cta_label);

        $shouldSave = true;
      }
    }

    try {
      if ($shouldSave) {
        $term->save();
      }
    } catch (\Exception $e) {
      $messenger->addError($e->getMessage());
      $form_state->setRedirect('page_content_backoffice.frontend_restricted_popup_form');
    }

    if ($shouldSave) {
      $messenger->addMessage($this->t('SUCCESSFULLY_SAVED'));
    } else {
      $messenger->addMessage($this->t('NOTHING_TO_SAVE'));
    }

    $form_state->setRedirect('page_content_backoffice.frontend_restricted_popup_form');
  }
}
