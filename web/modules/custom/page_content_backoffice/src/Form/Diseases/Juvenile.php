<?php

namespace Drupal\page_content_backoffice\Form\Diseases;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\file\Entity\File;
use Drupal\vimeo_module\Controller\VimeoController;

class Juvenile extends FormBase
{

  /**
   * Returns a unique string identifying the form.
   *
   * The returned ID should be a unique string that can be a valid PHP function
   * name, since it's used in hook implementation names such as
   * hook_form_FORM_ID_alter().
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId()
  {
    return 'frontend_juve_form_edit';
  }

  private function getData($saving = false)
  {
    $vid = 'frontend_juve_disease_taxonomy';
    $properties['vid'] = $vid;

    $terms = \Drupal::entityTypeManager()
      ->getStorage('taxonomy_term')
      ->loadByProperties($properties);

    if ($saving && empty($terms)) {
      throw new \Exception("Internal error! Taxonomy Term not exists!");
    }

    return reset($terms);
  }

  public function buildForm(array $form, FormStateInterface $form_state)
  {
    /*$form['preview'] = [
      '#type' => 'button',
      '#value' => $this->t('PREVIEW_STATE'),
      '#attributes' => array('onclick' => 'return false;'),
    ];
    $form['preview']['#attributes']['class'][] = 'preview-button';
    $form['preview']['#attributes']['class'][] = 'footer-preview-button';

    $form['preview-mobile'] = [
      '#type' => 'button',
      '#value' => $this->t('PREVIEW_STATE_MOBILE'),
      '#attributes' => array('onclick' => 'return false;'),
    ];
    $form['preview-mobile']['#attributes']['class'][] = 'preview-button-mobile';
    $form['preview-mobile']['#attributes']['class'][] = 'footer-preview-button-mobile';*/

    $term = $this->getData();

    $form['back'] = [
      '#type' => 'item',
      '#markup' => '<a href="' . Url::fromRoute('page_content_backoffice.your_condition_page_form')->toString() . '">Back to Your Condition page</a>',
    ];

    $result = \Drupal::entityQuery('node')
      ->condition('type', 'diagnosed_condition')
      ->execute();

    $conditions = [];
    foreach($result as $id){
      $condition = \Drupal\node\Entity\Node::load($id);
      $conditions[$id] = $condition->getTitle();
    }

    $form['condition_h1'] = [
      '#type' => 'item',
      '#markup' => '<h1>Associated Condition</h1>',
    ];

    $form['frontend_juv_page_condition_select'] = [
      '#type' => 'select',
      '#title' => $this->t('FRONTEND_JUV_PAGE_DEFAULT_CONDITION_SELECTOR_LABEL'),
      '#options' => $conditions,
      '#default_value' => $term->field_juv_assigned_condition_ref->target_id,
      '#description' => $this->t(''),
    ];

    $form['hero_h1'] = [
      '#type' => 'item',
      '#markup' => '<h1>Hero</h1>',
    ];

    $form['frontend_juv_disease_title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('FRONTEND_JUV_TITLE_LABEL'),
      '#description' => $this->t('DEMO: If you leave it empty, the original system message will be shown'),
      '#required' => false,
      '#default_value' => ($term->field_juv_dis_title->value ? $term->field_juv_dis_title->value : $this->t('JUVENILE_DISEASE_TITLE')),
    ];

    $form['section_h1'] = [
      '#type' => 'item',
      '#markup' => '<hr/><h1>Section Article</h1>',
    ];

    $form['frontend_juv_disease_section_string_1'] = [
      '#type' => 'textfield',
      '#title' => $this->t('FRONTEND_JUV_DISEASE_SECTION_FIRST_STRING_LABEL'),
      '#description' => $this->t('DEMO: If you leave it empty, the original system message will be shown'),
      '#required' => false,
      '#default_value' => ($term->field_juv_dis_section_str1->value ? $term->field_juv_dis_section_str1->value : $this->t('')),
    ];

    $form['frontend_juv_disease_page_section_string_2'] = [
      '#type' => 'textfield',
      '#title' => $this->t('FRONTEND_JUV_DISEASE_SECTION_SECOND_STRING_LABEL'),
      '#description' => $this->t('DEMO: If you leave it empty, the original system message will be shown'),
      '#required' => false,
      '#default_value' => ($term->field_juv_dis_section_str2->value ? $term->field_juv_dis_section_str2->value : $this->t('')),
    ];

    $form['frontend_juv_disease_page_section_desc'] = [
      '#type' => 'textarea',
      '#title' => $this->t('FRONTEND_JUV_DISEASE_SECTION_DESCRIPTION_LABEL'),
      '#description' => $this->t('DEMO: If you leave it empty, the original system message will be shown'),
      '#required' => false,
      '#default_value' => ($term->field_juv_dis_section_desc->value ? $term->field_juv_dis_section_desc->value : $this->t('')),
      '#attributes' => [
        'max-length' => 512,
        'maxlength' => 512,
      ],
    ];

    $form['symptoms_h1'] = [
      '#type' => 'item',
      '#markup' => '<hr/><h1>Symptoms</h1>',
    ];

    $juv_disease_page_symptom_gallery = null;
    if ($term->field_juv_dis_symptom_galler_ref->target_id) {
      $juv_disease_page_symptom_gallery = \Drupal\node\Entity\Node::load($term->field_juv_dis_symptom_galler_ref->target_id);
    }

    $form['frontend_juv_disease_page_symptom_gallery'] = [
      '#type' => 'entity_autocomplete',
      '#title' => $this->t('FRONTEND_JUV_DISEASE_SYMPTOM_GALLERY_LABEL'),
      '#target_type' => 'node',
      '#default_value' => ($term->field_juv_dis_symptom_galler_ref->target_id ? $juv_disease_page_symptom_gallery : ''),
      '#selection_handler' => 'default', // Optional. The default selection handler is pre-populated to 'default'.
      '#selection_settings' => [
        'target_bundles' => ['symptom_gallery', 'content'],
      ],
    ];

    $form['video_content_h1'] = [
      '#type' => 'item',
      '#markup' => '<hr/><h1>Video content</h1>',
    ];

    $form['frontend_juv_disease_page_video_content'] = [
      '#type' => 'textfield',
      '#title' => $this->t('FRONTEND_JUV_DISEASE_VIDEO_CONTENT_LABEL'),
      '#description' => $this->t('DEMO: If you leave it empty, no video will be shown.'),
      '#required' => false,
      '#default_value' => ($term->field_idb_dis_video->value ? $term->field_idb_dis_video->value : $this->t('')),
    ];

    $form['cta_h1'] = [
      '#type' => 'item',
      '#markup' => '<hr/><h1>CTA Box</h1>',
    ];

    $form['frontend_juv_disease_page_cta_title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('FRONTEND_JUV_DISEASE_CTA_TITLE_LABEL'),
      '#description' => $this->t('DEMO: If you leave it empty, the original system message will be shown'),
      '#required' => false,
      '#default_value' => ($term->field_juv_dis_cta_title->value ? $term->field_juv_dis_cta_title->value : $this->t('')),
    ];

    $form['frontend_juv_disease_page_cta_description'] = [
      '#type' => 'textarea',
      '#title' => $this->t('FRONTEND_JUV_DISEASE_CTA_DESCRIPTION_LABEL'),
      '#description' => $this->t('DEMO: If you leave it empty, the original system message will be shown'),
      '#required' => false,
      '#default_value' => ($term->field_juv_dis_cta_desc->value ? $term->field_juv_dis_cta_desc->value : $this->t('')),
      '#attributes' => [
        'max-length' => 512,
        'maxlength' => 512,
      ],
    ];

    $form['frontend_juv_disease_page_cta_button_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('FRONTEND_JUV_DISEASE_CTA_BUTTON_LABEL_LABEL'),
      '#description' => $this->t('DEMO: If you leave it empty, the original system message will be shown'),
      '#required' => false,
      '#default_value' => ($term->field_juv_dis_cta_button_label->value ? $term->field_juv_dis_cta_button_label->value : $this->t('')),
    ];

    $form['frontend_juv_disease_page_cta_button_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('FRONTEND_JUV_DISEASE_CTA_BUTTON_URL_LABEL'),
      '#description' => $this->t('DEMO: If you leave it empty, the original system message will be shown'),
      '#required' => false,
      '#default_value' => ($term->field_juv_dis_cta_button_url->value ? $term->field_juv_dis_cta_button_url->value : $this->t('')),
    ];

    $form['iso_h1'] = [
      '#type' => 'item',
      '#markup' => '<hr/><h1>ISO Number</h1>',
    ];

    $form['frontend_juv_iso_number'] = [
      '#type' => 'textfield',
      '#title' => $this->t('FRONTEND_JUV_ISO_NUMBER_LABEL'),
      '#description' => $this->t('DEMO: If you leave it empty, the original system message will be shown'),
      '#required' => false,
      '#default_value' => ($term->field_juv_dis_page_iso->value ? $term->field_juv_dis_page_iso->value : $this->t('')),
    ];

    $form['actions'] = [
      '#type' => 'actions',
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('SUBMIT'),
    ];

    unset($term);

    return $form;
  }

  /**
   * Validate the title and the checkbox of the form
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   */
  public function validateForm(array &$form, FormStateInterface $form_state)
  {
    parent::validateForm($form, $form_state);
  }

  public function submitForm(array &$form, FormStateInterface $form_state)
  {
    $values = $form_state->getValues();
    $messenger = \Drupal::messenger();

    try {
      $term = $this->getData(true);
    } catch (\Exception $e) {
      $messenger->addError($e->getMessage());
      return $form_state->setRedirect('page_content_backoffice.your_condition_page_juvenile_form');
    }

    $frontend_juv_disease_condition_select = $values['frontend_juv_page_condition_select'];
    $frontend_juv_disease_title = $values['frontend_juv_disease_title'];
    $frontend_juv_disease_section_string_1 = $values['frontend_juv_disease_section_string_1'];
    $frontend_juv_disease_section_string_2 = $values['frontend_juv_disease_page_section_string_2'];
    $frontend_juv_disease_section_desc = $values['frontend_juv_disease_page_section_desc'];
    $frontend_juv_disease_symptom_gallery = $values['frontend_juv_disease_page_symptom_gallery'];
    $frontend_juv_disease_video = $values['frontend_juv_disease_page_video_content'];
    $frontend_juv_disease_cta_title = $values['frontend_juv_disease_page_cta_title'];
    $frontend_juv_disease_cta_desc = $values['frontend_juv_disease_page_cta_description'];
    $frontend_juv_disease_cta_button_label = $values['frontend_juv_disease_page_cta_button_label'];
    $frontend_juv_disease_cta_button_url = $values['frontend_juv_disease_page_cta_button_url'];
    $frontend_juv_disease_iso_number = $values['frontend_juv_iso_number'];

    $tmp = explode('/', $frontend_juv_disease_video);
    $frontend_juv_disease_video = end($tmp);

    $shouldSave = false;

    if (is_string($frontend_juv_disease_video)) {
      if ($term->field_idb_dis_video[0]->value !== $frontend_juv_disease_video && $frontend_juv_disease_video) {
        $vimeo_client = new VimeoController();
        $video_id = $frontend_juv_disease_video;
        $video = $vimeo_client->isMyVideo($video_id);
        if (!$video) {
          $messenger->addError($this->t('INVALID_VIDEO'));
          $form_state->setRedirect('page_content_backoffice.your_condition_page_juvenile_form');

          return true;
        } else {
          $term->set('field_idb_dis_video', $frontend_juv_disease_video);

          $shouldSave = true;
        }
      }
    }

    if (is_string($frontend_juv_disease_condition_select)) {
      if ($term->field_juv_assigned_condition_ref[0]->target_id !== $frontend_juv_disease_condition_select) {
        $term->set('field_juv_assigned_condition_ref', ['target_id' => $frontend_juv_disease_condition_select]);

        $shouldSave = true;
      }
    }

    if (is_string($frontend_juv_disease_title)) {
      if ($term->field_juv_dis_title[0]->value !== $frontend_juv_disease_title) {
        $term->set('field_juv_dis_title', $frontend_juv_disease_title);

        $shouldSave = true;
      }
    }

    if (is_string($frontend_juv_disease_section_string_1)) {
      if ($term->field_juv_dis_section_str1[0]->value !== $frontend_juv_disease_section_string_1) {
        $term->set('field_juv_dis_section_str1', $frontend_juv_disease_section_string_1);

        $shouldSave = true;
      }
    }

    if (is_string($frontend_juv_disease_section_string_2)) {
      if ($term->field_juv_dis_section_str2[0]->value !== $frontend_juv_disease_section_string_2) {
        $term->set('field_juv_dis_section_str2', $frontend_juv_disease_section_string_2);

        $shouldSave = true;
      }
    }

    if (is_string($frontend_juv_disease_section_desc)) {
      if ($term->field_juv_dis_section_desc[0]->value !== $frontend_juv_disease_section_desc) {
        $term->set('field_juv_dis_section_desc', $frontend_juv_disease_section_desc);

        $shouldSave = true;
      }
    }

    if (is_string($frontend_juv_disease_symptom_gallery)) {
      if (isset($frontend_juv_disease_symptom_gallery) && !empty($frontend_juv_disease_symptom_gallery)) {
        $term->set('field_juv_dis_symptom_galler_ref', ['target_id' => $frontend_juv_disease_symptom_gallery]);

        $shouldSave = true;
      }
    }

    if (is_string($frontend_juv_disease_cta_title)) {
      if ($term->field_juv_dis_cta_title[0]->value !== $frontend_juv_disease_cta_title) {
        $term->set('field_juv_dis_cta_title', $frontend_juv_disease_cta_title);

        $shouldSave = true;
      }
    }

    if (is_string($frontend_juv_disease_cta_desc)) {
      if ($term->field_juv_dis_cta_desc[0]->value !== $frontend_juv_disease_cta_desc) {
        $term->set('field_juv_dis_cta_desc', $frontend_juv_disease_cta_desc);

        $shouldSave = true;
      }
    }

    if (is_string($frontend_juv_disease_cta_button_label)) {
      if ($term->field_juv_dis_cta_button_label[0]->value !== $frontend_juv_disease_cta_button_label) {
        $term->set('field_juv_dis_cta_button_label', $frontend_juv_disease_cta_button_label);

        $shouldSave = true;
      }
    }

    if (is_string($frontend_juv_disease_cta_button_url)) {
      if ($term->field_juv_dis_cta_button_url[0]->value !== $frontend_juv_disease_cta_button_url) {
        $term->set('field_juv_dis_cta_button_url', $frontend_juv_disease_cta_button_url);

        $shouldSave = true;
      }
    }

    if (is_string($frontend_juv_disease_iso_number)) {
      if ($term->field_juv_dis_page_iso[0]->value !== $frontend_juv_disease_iso_number) {
        $term->set('field_juv_dis_page_iso', $frontend_juv_disease_iso_number);

        $shouldSave = true;
      }
    }

    try {
      if ($shouldSave) {
        $term->save();
      }
    } catch (\Exception $e) {
      $messenger->addError($e->getMessage());
      $form_state->setRedirect('page_content_backoffice.your_condition_page_juvenile_form');
    }

    if ($shouldSave) {
      $messenger->addMessage($this->t('SUCCESSFULLY_SAVED'));
    } else {
      $messenger->addMessage($this->t('NOTHING_TO_SAVE'));
    }

    $form_state->setRedirect('page_content_backoffice.your_condition_page_juvenile_form');
  }
}
