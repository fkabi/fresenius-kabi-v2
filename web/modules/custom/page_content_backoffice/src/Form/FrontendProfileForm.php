<?php

namespace Drupal\page_content_backoffice\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\file\Entity\File;

class FrontendProfileForm extends FormBase
{

  /**
   * Returns a unique string identifying the form.
   *
   * The returned ID should be a unique string that can be a valid PHP function
   * name, since it's used in hook implementation names such as
   * hook_form_FORM_ID_alter().
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId()
  {
    return 'frontend_profile_page_form_edit';
  }

  private function getData($saving = false)
  {
    $vid = 'frontend_profile_taxonomy';
    $properties['vid'] = $vid;

    $terms = \Drupal::entityTypeManager()
      ->getStorage('taxonomy_term')
      ->loadByProperties($properties);

    if ($saving && empty($terms)) {
      throw new \Exception("Internal error! Taxonomy Term not exists!");
    }

    return reset($terms);
  }

  public function buildForm(array $form, FormStateInterface $form_state)
  {
    /*$form['preview'] = [
      '#type' => 'button',
      '#value' => $this->t('PREVIEW_STATE'),
      '#attributes' => array('onclick' => 'return false;'),
    ];
    $form['preview']['#attributes']['class'][] = 'preview-button';
    $form['preview']['#attributes']['class'][] = 'footer-preview-button';

    $form['preview-mobile'] = [
      '#type' => 'button',
      '#value' => $this->t('PREVIEW_STATE_MOBILE'),
      '#attributes' => array('onclick' => 'return false;'),
    ];
    $form['preview-mobile']['#attributes']['class'][] = 'preview-button-mobile';
    $form['preview-mobile']['#attributes']['class'][] = 'footer-preview-button-mobile';*/

    $term = $this->getData();

    $form['frontend_profile_page_title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('FRONTEND_PROFILE_PAGE_TITLE_LABEL'),
      '#description' => $this->t('DEMO: If you leave it empty, the original system message will be shown'),
      '#required' => FALSE,
      '#default_value' => ($term->field_profile_page_title->value ? $term->field_profile_page_title->value : $this->t('PROFILE_PAGE_TITLE')),
    ];

    $form['frontend_profile_page_email_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('FRONTEND_PROFILE_PAGE_EMAIL_LABEL_LABEL'),
      '#description' => $this->t('DEMO: If you leave it empty, the original system message will be shown'),
      '#required' => FALSE,
      '#default_value' => ($term->field_profile_page_email_label->value ? $term->field_profile_page_email_label->value : $this->t('EMAIL')),
    ];

    $form['frontend_profile_page_condition_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('FRONTEND_PROFILE_PAGE_CONDITION_LABEL_LABEL'),
      '#description' => $this->t('DEMO: If you leave it empty, the original system message will be shown'),
      '#required' => FALSE,
      '#default_value' => ($term->field_profile_page_cond_label->value ? $term->field_profile_page_cond_label->value : $this->t('DIAGNOSED_CONDITION')),
    ];

    $form['frontend_profile_page_checkbox_label'] = [
      '#type' => 'text_format',
      '#title' => $this->t('FRONTEND_PROFILE_PAGE_CHECKBOX_LABEL'),
      '#description' => $this->t('DEMO: If you leave it empty, the original system message will be shown.'),
      '#required' => FALSE,
      '#default_value' => ($term->field_profile_page_checkbox_labe->value ? $term->field_profile_page_checkbox_labe->value : ''),
      '#format' => 'full_html',
    ];

    $form['frontend_profile_page_save_button_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('FRONTEND_PROFILE_PAGE_SAVE_BUTTON_LABEL_LABEL'),
      '#description' => $this->t('DEMO: If you leave it empty, the original system message will be shown'),
      '#required' => FALSE,
      '#default_value' => ($term->field_profile_page_save_btn_labe->value ? $term->field_profile_page_save_btn_labe->value : $this->t('SAVE_SETTINGS')),
    ];

    $form['frontend_profile_page_logout_button_label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('FRONTEND_PROFILE_PAGE_LOGOUT_BUTTON_LABEL_LABEL'),
      '#description' => $this->t('DEMO: If you leave it empty, the original system message will be shown'),
      '#required' => FALSE,
      '#default_value' => ($term->field_profile_page_logout_btn_la->value ? $term->field_profile_page_logout_btn_la->value : $this->t('LOGOUT')),
    ];

    $form['iso_h1'] = [
      '#type' => 'item',
      '#markup' => '<hr/><h1>ISO Number</h1>',
    ];

    $form['frontend_profile_page_iso_number'] = [
      '#type' => 'textfield',
      '#title' => $this->t('FRONTEND_PROFILE_PAGE_ISO_NUMBER_LABEL'),
      '#description' => $this->t('DEMO: If you leave it empty, the original system message will be shown'),
      '#required' => false,
      '#default_value' => ($term->field_profile_page_iso->value ? $term->field_profile_page_iso->value : $this->t('')),
    ];

    $form['actions'] = [
      '#type' => 'actions',
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('SUBMIT'),
    ];

    unset($term);

    return $form;
  }

  /**
   * Validate the title and the checkbox of the form
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   */
  public function validateForm(array &$form, FormStateInterface $form_state)
  {
    parent::validateForm($form, $form_state);
  }

  public function submitForm(array &$form, FormStateInterface $form_state)
  {
    $values = $form_state->getValues();
    $messenger = \Drupal::messenger();

    try {
      $term = $this->getData(true);
    } catch (\Exception $e) {
      $messenger->addError($e->getMessage());
      return $form_state->setRedirect('page_content_backoffice.frontend_profile_page_form');
    }

    $frontend_profile_page_title = $values['frontend_profile_page_title'];
    $frontend_profile_page_email_label = $values['frontend_profile_page_email_label'];
    $frontend_profile_page_condition_label = $values['frontend_profile_page_condition_label'];
    $frontend_profile_page_checkbox_label = $values['frontend_profile_page_checkbox_label']['value'];
    $frontend_profile_page_save_button_label = $values['frontend_profile_page_save_button_label'];
    $frontend_profile_page_logout_button_label = $values['frontend_profile_page_logout_button_label'];
    $frontend_profile_page_iso_number = $values['frontend_profile_page_iso_number'];

    $shouldSave = false;

    if (is_string($frontend_profile_page_title)) {
      if ($term->field_profile_page_title[0]->value !== $frontend_profile_page_title) {
        $term->set('field_profile_page_title', $frontend_profile_page_title);

        $shouldSave = true;
      }
    }

    if (is_string($frontend_profile_page_email_label)) {
      if ($term->field_profile_page_email_label[0]->value !== $frontend_profile_page_email_label) {
        $term->set('field_profile_page_email_label', $frontend_profile_page_email_label);

        $shouldSave = true;
      }
    }

    if (is_string($frontend_profile_page_condition_label)) {
      if ($term->field_profile_page_cond_label[0]->value !== $frontend_profile_page_condition_label) {
        $term->set('field_profile_page_cond_label', $frontend_profile_page_condition_label);

        $shouldSave = true;
      }
    }

    if (is_string($frontend_profile_page_checkbox_label)) {
      if ($term->field_profile_page_checkbox_labe[0]->value !== $frontend_profile_page_checkbox_label) {
        $term->set('field_profile_page_checkbox_labe', $frontend_profile_page_checkbox_label);

        $shouldSave = true;
      }
    }

    if (is_string($frontend_profile_page_save_button_label)) {
      if ($term->field_profile_page_save_btn_labe[0]->value !== $frontend_profile_page_save_button_label) {
        $term->set('field_profile_page_save_btn_labe', $frontend_profile_page_save_button_label);

        $shouldSave = true;
      }
    }

    if (is_string($frontend_profile_page_logout_button_label)) {
      if ($term->field_profile_page_logout_btn_la[0]->value !== $frontend_profile_page_logout_button_label) {
        $term->set('field_profile_page_logout_btn_la', $frontend_profile_page_logout_button_label);

        $shouldSave = true;
      }
    }

    if (is_string($frontend_profile_page_iso_number)) {
      if ($term->field_profile_page_iso[0]->value !== $frontend_profile_page_iso_number) {
        $term->set('field_profile_page_iso', $frontend_profile_page_iso_number);

        $shouldSave = true;
      }
    }

    try {
      if ($shouldSave) {
        $term->save();
      }
    } catch (\Exception $e) {
      $messenger->addError($e->getMessage());
      $form_state->setRedirect('page_content_backoffice.frontend_profile_page_form');
    }

    if ($shouldSave) {
      $messenger->addMessage($this->t('SUCCESSFULLY_SAVED'));
    } else {
      $messenger->addMessage($this->t('NOTHING_TO_SAVE'));
    }

    $form_state->setRedirect('page_content_backoffice.frontend_profile_page_form');
  }
}
