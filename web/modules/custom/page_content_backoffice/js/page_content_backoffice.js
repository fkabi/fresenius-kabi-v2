(function ($) {
  'use strict';

  Drupal.behaviors.listenClick = {
    attach: function (context, settings) {
      const btn = $('.frontend-login-form-edit .preview-button', context);
      btn.on('click', e => {
        e.preventDefault();

        let content = '';

        const target = $(e.target);
        if (target.hasClass('login-preview-button')) {
          content = getLoginContent();
        }

        content = `<div class="modal-container">${content}</div`;
        showModal(content, 'kabi-preview-modal');
      });

      const btnMobile = $('.frontend-login-form-edit .preview-button-mobile', context);
      btnMobile.on('click', e => {
        e.preventDefault();

        let content = '';

        const target = $(e.target);
        if (target.hasClass('login-preview-button-mobile')) {
          content = getLoginContent();
        }

        content = `<div class="modal-container">${content}</div`;
        showModal(content, 'kabi-preview-modal-mobile');
      });
    }
  };

  Drupal.behaviors.listenClickRegisterPatient = {
    attach: function (context, settings) {
      const btn = $('.frontend-register-patient-form-edit .preview-button', context);
      btn.on('click', e => {
        e.preventDefault();

        let content = '';

        const target = $(e.target);
        if (target.hasClass('register-patient-preview-button')) {
          content = getRegisterPatientContent();
        }

        content = `<div class="modal-container">${content}</div`;
        showModal(content, 'kabi-preview-modal');
      });

      const btnMobile = $('.frontend-register-patient-form-edit .preview-button-mobile', context);
      btnMobile.on('click', e => {
        e.preventDefault();

        let content = '';

        const target = $(e.target);
        if (target.hasClass('register-patient-preview-button-mobile')) {
          content = getRegisterPatientContent();
        }

        content = `<div class="modal-container">${content}</div`;
        showModal(content, 'kabi-preview-modal-mobile');
      });
    }
  };

  Drupal.behaviors.listenClickRegisterHcp = {
    attach: function (context, settings) {
      const btn = $('.frontend-register-hcp-form-edit .preview-button', context);
      btn.on('click', e => {
        e.preventDefault();

        let content = '';

        const target = $(e.target);
        if (target.hasClass('register-hcp-preview-button')) {
          content = getRegisterHCPContent();
        }

        content = `<div class="modal-container">${content}</div`;
        showModal(content, 'kabi-preview-modal');
      });

      const btnMobile = $('.frontend-register-hcp-form-edit .preview-button-mobile', context);
      btnMobile.on('click', e => {
        e.preventDefault();

        let content = '';

        const target = $(e.target);
        if (target.hasClass('register-hcp-preview-button-mobile')) {
          content = getRegisterHCPContent();
        }

        content = `<div class="modal-container">${content}</div`;
        showModal(content, 'kabi-preview-modal-mobile');
      });
    }
  };

  Drupal.behaviors.listenClickFooter = {
    attach: function (context, settings) {
      const btn = $('.frontend-footer-form-edit .preview-button', context);
      btn.on('click', e => {
        e.preventDefault();

        let content = '';

        const target = $(e.target);
        if (target.hasClass('footer-preview-button')) {
          content = getFooterContent();
        }

        content = `<div class="modal-container">${content}</div`;
        showModal(content, 'kabi-preview-modal');
      });

      const btnMobile = $('.frontend-footer-form-edit .preview-button-mobile', context);
      btnMobile.on('click', e => {
        e.preventDefault();

        let content = '';

        const target = $(e.target);
        if (target.hasClass('footer-preview-button-mobile')) {
          content = getFooterContent();
        }

        content = `<div class="modal-container">${content}</div`;
        showModal(content, 'kabi-preview-modal-mobile');
      });
    }
  };

  Drupal.behaviors.listenClickRestrictedPopup = {
    attach: function (context, settings) {
      const btn = $('.frontend-restricted-popup-form-edit .preview-button', context);
      btn.on('click', e => {
        e.preventDefault();

        let content = '';

        const target = $(e.target);
        if (target.hasClass('restricted-popup-preview-button')) {
          content = getRestrictedPopupContent();
        }

        content = `<div class="modal-container">${content}</div`;
        showModal(content, 'kabi-preview-modal');
      });

      const btnMobile = $('.frontend-restricted-popup-form-edit .preview-button-mobile', context);
      btnMobile.on('click', e => {
        e.preventDefault();

        let content = '';

        const target = $(e.target);
        if (target.hasClass('restricted-popup-preview-button-mobile')) {
          content = getRestrictedPopupContent();
        }

        content = `<div class="modal-container">${content}</div`;
        showModal(content, 'kabi-preview-modal-mobile');
      });
    }
  };

  const showModal = (content, className) => {
    const html = `<div class="modal ${className}">${getControls()}${content}</div>`;
    $('#page-wrapper').append(html);

    const modal = $(`.${className}`);

    addControlEvents(modal);
  };

  const getControls = () => {
    const controlClose = `<div class="close-button"><div class="label">x</div></div>`;
    const controlToMobile = `<div class="resolution-control controls-to-mobile"><div class="label">Switch to Mobile</div></div>`;
    const controlToDesktop = `<div class="resolution-control controls-to-desktop"><div class="label">Switch to Desktop</div></div>`;
    const controlScale = `<div class="controls-scale"><div class="label">Scale to Fit</div></div>`;

    return `${controlClose}${controlToMobile}${controlToDesktop}${controlScale}`;
  };

  const addControlEvents = modal => {
    const closeButton = modal.find('.close-button');
    closeButton.on('click', e => {
      modal.remove();
    });

    const resControlButton = modal.find('.resolution-control');
    resControlButton.on('click', e => {
      if (modal.hasClass('kabi-preview-modal-mobile')) {
        modal.removeClass('kabi-preview-modal-mobile').addClass('kabi-preview-modal');

        $('.main-section')
          .css('position', 'unset')
          .css('margin-top', 'auto')
          .css('transform', 'scale(1)')
          .css('left', 'unset');
      } else {
        modal.addClass('kabi-preview-modal-mobile').removeClass('kabi-preview-modal');
      }
    });

    const scaleControlButton = modal.find('.controls-scale');
    scaleControlButton.on('click', e => {
      const section = $('.main-section');
      let height = $('.modal-container').height();
      let sum = 0;
      $.each(section.children(), (i, e) => {
        sum += $(e).outerHeight();
      });

      const scale = height / sum;
      if (scale > 1) {
        return;
      }

      let dif = sum - height;
      let margindif = dif * scale;

      const newWidth = 375 * scale;

      const left = $('.modal-container').width() / 2 - newWidth / 2;

      section
        .css('position', 'absolute')
        .css('margin-top', `-${margindif}px`)
        .css('transform', `scale(${scale})`)
        .css('left', left);
    });
  };

  const getLoginContent = () => {
    const logInPageLoginSectionTitle = $('[name="login_page_login_section_title"]').val();
    const logInPageLoginSectionEmailPlaceholder = $('[name="login_page_login_section_email_placeholder"]').val();
    const logInPageLoginSectionPasswordPlaceholder = $('[name="login_page_login_section_password_placeholder"]').val();
    const logInPageLoginSectionForgotEmailLabel = $('[name="login_page_login_section_forgot_email_label"]').val();
    const logInPageLoginSectionForgotPasswordLabel = $('[name="login_page_login_section_forgot_password_label"]').val();
    const logInPageLoginSectionSubmitButtonLabel = $('[name="login_page_login_section_submit_button_label"]').val();
    const logInPageRegisterSectionTitle = $('[name="login_page_register_section_title"]').val();
    const logInPageRegisterSectionContent = $('#cke_edit-login-page-register-section-content-value iframe').contents().find('body').html();
    const logInPageRegisterPatientButtonLabel = $('[name="login_page_register_section_patient_register_button_label"]').val();
    const logInPageRegisterHCPButtonLabel = $('[name="login_page_register_section_hcp_register_button_label"]').val();
    const logInPageRegisterDescription = $('[name="login_page_register_section_description"]').text();

    return `
            <div class="main-section">
            <div class="login-section">
            <div class="title">${logInPageLoginSectionTitle}</div>
            <div class="email">
            <input type="text" class="text" value="${logInPageLoginSectionEmailPlaceholder}" />
            </div>
            <div class="password">
            <input type="text" class="text" value="${logInPageLoginSectionPasswordPlaceholder}" />
            </div>
            <div class="forgots">
            <div class="forgot-email">${logInPageLoginSectionForgotEmailLabel}</div>
            <div class="forgot-password">${logInPageLoginSectionForgotPasswordLabel}</div>
</div>
<div class="submit-button">
<div class="label">${logInPageLoginSectionSubmitButtonLabel}</div>
</div>
</div>
<div class="register-section">
<div class="title">${logInPageRegisterSectionTitle}</div>
<div class="content">${logInPageRegisterSectionContent}</div>
<div class="button-container">
<div class="patient-button">
<div class="label">${logInPageRegisterPatientButtonLabel}</div>
</div>
<div class="hcp-button">
<div class="label">${logInPageRegisterHCPButtonLabel}</div>
</div>
</div>
<div class="description">${logInPageRegisterDescription}</div>
</div>
</div>
          `;
  };

  const getRegisterPatientContent = () => {
    const registerPatientPageTitle = $('[name="register_patient_page_title"]').val();
    const registerPatientPageEmail = $('[name="register_patient_page_email"]').val();
    const registerPatientPageDiagnosedCondition = $('[name="register_patient_page_condition"]').val();
    const registerPatientPageAccessCode = $('[name="register_patient_page_access_code"]').val();
    const registerPatientPagePassword = $('[name="register_patient_page_password"]').val();
    const registerPatientPageConfirmPassword = $('[name="register_patient_page_confirm_password"]').val();
    const registerPatientPageCheckboxLabel = $('[name="register_patient_page_checkbox_label"]').val();
    const registerPatientPageSubmitButtonLabel = $('[name="register_patient_page_submit_button_label"]').val();

    return `
            <div class="main-section">
            <div class="register-patient-section">
            <div class="title">${registerPatientPageTitle}</div>
            <div class="email">
            <input type="text" class="text" value="${registerPatientPageEmail}" />
            </div>
            <div class="condition">
            <input type="text" class="text" disabled value="${registerPatientPageDiagnosedCondition}" />
            </div>
            <div class="access-code">
            <input type="text" class="text" value="${registerPatientPageAccessCode}" />
            </div>
            <div class="password">
            <input type="text" class="text" value="${registerPatientPagePassword}" />
            </div>
            <div class="confirm-password">
            <input type="text" class="text" value="${registerPatientPageConfirmPassword}" />
            </div>
            <div class="checkbox">
            <div class="box"></div>
            <div class="label">${registerPatientPageCheckboxLabel}</div>
</div>
<div class="submit-button">
<div class="label">${registerPatientPageSubmitButtonLabel}</div>
</div>
</div>
</div>
          `;
  };

  const getRegisterHCPContent = () => {
    const registerHCPPageTitle = $('[name="register_hcp_page_title"]').val();
    const registerHCPPageUserTitle = $('[name="register_hcp_page_user_title"]').val();
    const registerHCPPageFirstName = $('[name="register_hcp_page_first_name"]').val();
    const registerHCPPageLastName = $('[name="register_hcp_page_last_name"]').val();
    const registerHCPPageCountry = $('[name="register_hcp_page_country"]').val();
    const registerHCPPageOrganization = $('[name="register_hcp_page_healthcare_organization"]').val();
    const registerHCPPageEmail = $('[name="register_hcp_page_email"]').val();
    const registerHCPPagePassword = $('[name="register_hcp_page_password"]').val();
    const registerHCPPageConfirmPassword = $('[name="register_hcp_page_confirm_password"]').val();
    const registerHCPPageRegistrationNumber = $('[name="register_hcp_page_registration_number_label"]').val();
    const registerHCPPageCheckboxLabel = $('[name="register_hcp_page_checkbox_label"]').val();
    const registerHCPPageSubmitButtonLabel = $('[name="register_hcp_page_submit_button_label"]').val();

    return `
            <div class="main-section">
            <div class="register-hcp-section">
            <div class="title">${registerHCPPageTitle}</div>
            <div class="user-title">
            <input type="text" class="text" value="${registerHCPPageUserTitle}" />
            </div>
            <div class="firstname">
            <input type="text" class="text" value="${registerHCPPageFirstName}" />
            </div>
            <div class="lastname">
            <input type="text" class="text" value="${registerHCPPageLastName}" />
            </div>
            <div class="country">
            <input type="text" class="text" value="${registerHCPPageCountry}" />
            </div>
            <div class="healthcare-organization">
            <input type="text" class="text" value="${registerHCPPageOrganization}" />
            </div>
            <div class="email">
            <input type="text" class="text" value="${registerHCPPageEmail}" />
            </div>
            <div class="password">
            <input type="text" class="text" value="${registerHCPPagePassword}" />
            </div>
            <div class="confirm-password">
            <input type="text" class="text" value="${registerHCPPageConfirmPassword}" />
            </div>
            <div class="registration-number">
            <input type="text" class="text" value="${registerHCPPageRegistrationNumber}" />
            </div>
            <div class="checkbox">
            <div class="box"></div>
            <div class="label">${registerHCPPageCheckboxLabel}</div>
</div>
<div class="submit-button">
<div class="label">${registerHCPPageSubmitButtonLabel}</div>
</div>
</div>
</div>
          `;
  };

  const getFooterContent = () => {
    const footerDisclaimerSituational = $('#cke_edit-frontend-footer-disclaimer-situational-value iframe').contents().find('body').html();
    const footerDisclaimerNormal = $('#cke_edit-frontend-footer-disclaimer-normal-value iframe').contents().find('body').html();
    const footerDateLabel = $('[name="frontend_footer_date_label"]').val();
    const footerDate = $('[name="frontend_footer_date"]').val();
    const footerCurrentLogo = $('[name="frontend_footer_current_logo"]')[0].outerHTML;
    const footerMenu = $('.menu-items #edit-frontend-footer-menu-items').html();

    return `
            <div class="main-section footer">
            <div class="disclaimers">
            <div class="situational-disclaimer">${footerDisclaimerSituational}</div>
            <div class="normal-disclaimer">${footerDisclaimerNormal}</div>
            </div>
            <div class="footer-body">
            <div class="logo-container">${footerCurrentLogo}</div>
            <div class="links-and-date">
            <div class="date-container">
            <span class="date-label">${footerDateLabel}</span>
            <span class="date">${footerDate}</span>
            </div>
            <div class="links">${footerMenu}</div>
</div>
</div>
</div>
          `;
  };

  const getRestrictedPopupContent = () => {
    const restrictedPopupTitle = $('[name="frontend_restricted_popup_title"]').val();
    const restrictedPopupContent = $('#cke_edit-frontend-restricted-popup-content-value iframe').contents().find('body').html();
    const restrictedPopupCancelCTALabel = $('[name="frontend_restricted_popup_cancel_cta_button_label"]').val();
    const restrictedPopupSubmitCTALabel = $('[name="frontend_restricted_popup_submit_cta_button_label"]').val();

    return `
            <div class="main-section restricted-popup">
                <div class="backdrop">
                    <div class="container">
                        <div class="title">${restrictedPopupTitle}</div>
                        <div class="image-container">
                            IMG
                        </div>
                        <div class="content">${restrictedPopupContent}</div>
                        <div class="button-container">
                            <button class="cancel-cta">${restrictedPopupCancelCTALabel}</button>
                            <button class="submit-cta">${restrictedPopupSubmitCTALabel}</button>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
          `;
  };
})(jQuery);
