<?php

namespace Drupal\hcp_register_rest_api\Plugin\rest\resource;

use Drupal\Core\Annotation\Translation;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Site\Settings;
use Drupal\kabi\Validation\ApiValidation;
use Drupal\node\Entity\Node;
use Drupal\rest\Annotation\RestResource;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Provides Resource for the HCP Register functionality
 *
 * @RestResource(
 *   id = "hcp_register",
 *   label = @Translation("HCP_REGISTER"),
 *   uri_paths = {
 *     "canonical" = "api/json/hcp/register",
 *     "create" = "api/json/hcp/register"
 *   }
 * )
 */
class HcpRegister extends ResourceBase
{
  const HTTP_OK = 200;
  const HTTP_UNPROC = 422;

  /**
   * Responds to entity POST requests.
   * @return \Drupal\rest\ResourceResponse
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function post(Request $request)
  {
    $content = json_decode($request->getContent(), true);

    $errors = ApiValidation::validate($content, self::getRules(), 'user');

    if ($errors) {
      $response = [
        'success' => false,
        'status_code' => self::HTTP_UNPROC,
        'message' => $this->t("INVALID_DATA"),
        'data' => $errors,
      ];

      return new ResourceResponse($response);
    }

    try {
      $email = $content["email"][0]["value"];
      $email = strtolower($email);
      $password = $content["password"][0]["value"];
      $name = $content["name"][0]["value"];

      $title = $content["title"][0]["value"] ?? NULL;
      $health_org = $content["healthcare_organization"][0]["value"] ?? NULL;
      $reg_num = $content["registration_number"][0]["value"] ?? NULL;
      $country = $content["country"][0]["value"] ?? NULL;
      $email_subscription = $content["field_user_email_sending"][0]["value"] ?? FALSE;

      if (!user_load_by_mail($email) && !user_load_by_name($name)) {
        $language = \Drupal::languageManager()->getCurrentLanguage()->getId();
        $user = \Drupal\user\Entity\User::create();

        $user->setPassword($password);
        $user->enforceIsNew();
        $user->setEmail($email);
        $user->setUsername($email);

        $user->set("langcode", $language);
        $user->set("preferred_langcode", $language);
        $user->set("preferred_admin_langcode", $language);

        if ($name) {
          $name_tags = preg_split('/\s+/', $name, -1, PREG_SPLIT_NO_EMPTY);

          $user->set('field_user_first_name', $name_tags[0]);
          $user->set('field_user_last_name', $name_tags[1]);
        }

        $user->set('field_user_title', $title);
        $user->set('field_user_hcp_healthcare_org', $health_org);
        $user->set('field_user_hcp_reg_number', $reg_num);
        $user->set('field_user_country', $country);
        $user->set('status', 0);

        $user->set('field_user_email_sending', $email_subscription);

        $timestamp = \Drupal::time()->getRequestTime();
        $date = DrupalDateTime::createFromTimestamp($timestamp, 'UTC');
        $user->set('field_user_password_set', $date->format("Y-m-d\TH:i:s"));

        $user->activate();

        $user->addRole('hcp');
        $user->removeRole('patient');

        $site_settings = \Drupal::service('site_settings.loader');
        $settings = $site_settings->loadByFieldset('hcp_registration_settings');

        if ($settings['hcp_registration_settings'] === '1') {
          $user->block();
        }

        $user->save();
      } else {
        $response = [
          'success' => false,
          'status_code' => self::HTTP_UNPROC,
          'message' => $this->t("USER_WITH_EMAIL_ALREADY_EXISTS"),
          'data' => $errors,
        ];

        return new ResourceResponse($response);
      }
    } catch (\Exception $e) {
      $response = [
        'success' => false,
        'status_code' => self::HTTP_UNPROC,
        'message' => $this->t("HCP_USER_CANNOT_BE_CREATED"),
        'data' => [
          "exception" => $e->getMessage(),
          "trace" => $e->getTraceAsString(),
        ],
      ];

      return new ResourceResponse($response);
    }

    $mailManager = \Drupal::service('plugin.manager.mail');
    $module = 'kabi';
    $key = 'hcp_register';
    $to = $email;
    $frontend_url = Settings::get('frontend_url');
    $params = [
      'subject' => t('Account details for :name at Kabicare v2', [':name' => $name]),
      'email' => $email,
      'username' => $email,
      'name' => $name,
      'frontend_url' => $frontend_url,
    ];
    $send = TRUE;
    $result = $mailManager->mail($module, $key, $to, 'en', $params, $params['email'], $send);
    if ($result['result'] !== TRUE) {
      \Drupal::messenger()->addError(t('There was a problem sending your message and it was not sent.'));
    }
    else {
      \Drupal::messenger()->addStatus(t('Your message has been sent.'));
    }

    return new ResourceResponse([
      'success' => true,
      'status_code' => self::HTTP_OK,
      'message' => $this->t("HCP_USER_CREATED"),
      'data' => [],
    ]);
  }

  public static function getRules(): array
  {
    return [
      "email" => "required|max:60|pattern:/^([a-z0-9_\-\.]+)@([a-z0-9_\-\.]+)\.([a-z]{2,5})$/",
      "password" => "required|min:6|max:60|pattern:/^.*(?=\S{10,})(?=\S*[a-z])(?=\S*[A-Z])(?=\S*[\d])[^\"'\s]*$/",
      "confirm_password" => "required|min:10|max:60|pattern:/^.*(?=\S{10,})(?=\S*[a-z])(?=\S*[A-Z])(?=\S*[\d])[^\"'\s]*$/",
      "name" => "required|min:2|max:60",
    ];
  }
}
