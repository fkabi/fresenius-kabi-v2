<?php

namespace Drupal\profile_edit_rest_api\Plugin\rest\resource;

use Drupal\Core\Annotation\Translation;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\kabi\Validation\ApiValidation;
use Drupal\node\Entity\Node;
use Drupal\rest\Annotation\RestResource;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Provides Resource for the Profile Edit functionality
 *
 * @RestResource(
 *   id = "profile_modify",
 *   label = @Translation("PROFILE_MODIFY"),
 *   uri_paths = {
 *     "canonical" = "/profile-edit-rest-api/modify-profile",
 *     "create" = "/profile-edit-rest-api/modify-profile",
 *   }
 * )
 */
class ProfileModify extends ResourceBase
{
  const HTTP_OK = 200;
  const HTTP_UNPROC = 422;

  /**
   * Responds to entity POST requests.
   * @return \Drupal\rest\ResourceResponse
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function post(Request $request)
  {
    $content = json_decode($request->getContent(), true);
    $user = \Drupal\user\Entity\User::load(\Drupal::currentUser()
      ->id());

    $node_original_status = null;

    if ($content === null) {
      $response = [
        'success' => false,
        'status_code' => self::HTTP_UNPROC,
        'message' => $this->t("INVALID_DATA"),
        'data' => [
          'error_title' => $this->t('EMPTY_REQUEST'),
        ],
      ];

      return new ResourceResponse($response);
    }

    $saving_email = false;
    if ($content['email'] && $content['email'][0] && $content['email'][0]['value']) {
      $pattern = "/^([a-z0-9_\-\.]+)@([a-z0-9_\-\.]+)\.([a-z]{2,5})$/";
      preg_match($pattern, $content['email'][0]['value'], $matches, PREG_UNMATCHED_AS_NULL);
      if ($matches) {
        $ids = \Drupal::entityQuery('user')
          ->condition('mail', $content['email'][0]['value'])
          ->condition('mail', $user->getEmail(), '!=')
          ->execute();

        if (!empty($ids)) {
          $response = [
            'success' => false,
            'status_code' => self::HTTP_UNPROC,
            'message' => $this->t("E-mail already exists"),
            'data' => [],
          ];
          return new ResourceResponse($response);
        }
        $saving_email = true;
      } else {
        $response = [
          'success' => false,
          'status_code' => self::HTTP_UNPROC,
          'message' => $this->t("INVALID_DATA"),
          'data' => [
            'error_title' => $this->t('INVALID_EMAIL'),
          ],
        ];

        return new ResourceResponse($response);
      }
    }
    $saving_first_name = false;
    if ($content['field_user_first_name'] && $content['field_user_first_name'][0] && $content['field_user_first_name'][0]['value']) {
      $saving_first_name = true;
    }

    $saving_last_name = false;
    if ($content['field_user_last_name'] && $content['field_user_last_name'][0] && $content['field_user_last_name'][0]['value']) {
      $saving_last_name = true;
    }

    $saving_email_sending = false;
    if ($content['field_user_email_sending'] && isset($content['field_user_email_sending'][0]) && isset($content['field_user_email_sending'][0]['value'])) {
      $email_sending_value = $content['field_user_email_sending'][0]['value'];
      if ($email_sending_value === 'true' || $email_sending_value === 'false' || $email_sending_value === true || $email_sending_value === false || $email_sending_value === 0 || $email_sending_value === 1 || $email_sending_value === '1' || $email_sending_value === '0') {
        $saving_email_sending = true;
      } else {
        $response = [
          'success' => false,
          'status_code' => self::HTTP_UNPROC,
          'message' => $this->t("INVALID_DATA"),
          'data' => [
            'error_title' => $this->t('INVALID_EMAIL_SENDING_VALUE'),
          ],
        ];

        return new ResourceResponse($response);
      }
    }

    try {
      $user = \Drupal::currentUser();
      $user_id = $user->id();
    } catch (\Exception $e) {
      $response = [
        'success' => false,
        'status_code' => self::HTTP_UNPROC,
        'message' => $this->t("USER_NOT_FOUND"),
        'data' => [],
      ];

      return new ResourceResponse($response);
    }

    $account = \Drupal\user\Entity\User::load($user_id);

    try {
      $saving_condition = false;
      if ($content['field_diagnosed_condition_ref'] && isset($content['field_diagnosed_condition_ref'][0]) && isset($content['field_diagnosed_condition_ref'][0]['target_id'])) {
        $condition_id = $content['field_diagnosed_condition_ref'][0]['target_id'];

        $nodes = \Drupal::entityTypeManager()
          ->getListBuilder('node')
          ->getStorage()
          ->loadByProperties([
            'type' => 'diagnosed_condition',
            'nid' => $condition_id,
          ]);

        if (empty($nodes)) {
          $response = [
            'success' => false,
            'status_code' => self::HTTP_OK,
            'message' => $this->t("CONDITION_DOES_NOT_EXIST"),
            'data' => [
              'content' => $this->t("CONDITION_DOES_NOT_EXIST"),
            ],
          ];

          return new ResourceResponse($response);
        }
        $saving_condition = true;
      }

      if ($saving_first_name) {
        $first_name = $content['field_user_first_name'][0]['value'];
        $account->set('field_user_first_name', [$first_name]);
      }

      if ($saving_last_name) {
        $last_name = $content['field_user_last_name'][0]['value'];
        $account->set('field_user_last_name', [$last_name]);
      }

      if ($saving_email) {
        $email = $content['email'][0]['value'];
        $account->setEmail($email);
        $account->setUsername($email);
      }

      if ($saving_condition) {
        $account->set('field_diagnosed_condition_ref', [$condition_id]);
      }

      if ($saving_email_sending) {
        $email_subscription = $content['field_user_email_sending'][0]['value'];
        $account->set('field_user_email_sending', $email_subscription);
      }

      $should_logout = false;

      if ($saving_email || $saving_condition || $saving_email_sending) {
        $account->save();

        if ($saving_email) {
          $should_logout = true;
        }
      }
    } catch (\Exception $e) {
      $response = [
        'success' => false,
        'status_code' => self::HTTP_UNPROC,
        'message' => $this->t("PROFILE_CANNOT_BE_MODIFIED"),
        'data' => [],
      ];

      return new ResourceResponse($response);
    }

    return new ResourceResponse([
      'success' => true,
      'status_code' => self::HTTP_OK,
      'message' => $this->t("PROFILE_MODIFIED"),
      'data' => [
        'content' => 'SUCCESSFULLY_SAVED',
        'shouldLogout' => $should_logout,
      ],
    ]);
  }
}
