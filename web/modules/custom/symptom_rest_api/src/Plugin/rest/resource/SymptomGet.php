<?php

namespace Drupal\symptom_rest_api\Plugin\rest\resource;

use Drupal\Core\Annotation\Translation;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\rest\Annotation\RestResource;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;

/**
 * Provides Resource for the Symptom functionality
 *
 * @RestResource(
 *   id = "symptom_get",
 *   label = @Translation("SYMPTOM_GET"),
 *   uri_paths = {
 *     "canonical" = "/symptom-rest-api/get-symptoms"
 *   }
 * )
 */
class SymptomGet extends ResourceBase
{
  const HTTP_OK = 200;
  const HTTP_UNPROC = 422;

  /**
   * Responds to entity GET requests.
   * @return \Drupal\rest\ResourceResponse
   */
  public function get()
  {
    try {
      $user = \Drupal::currentUser();
      $userLangId = $user->getPreferredLangcode();

      $nids = \Drupal::entityTypeManager()
        ->getListBuilder('node')
        ->getStorage()
        ->loadByProperties([
          'type' => "symptom",
          'field_symptom_user_ref' => $user->id(),
        ]);

      $user_symptoms = [];

      foreach ($nids as $id => $node) {
        $user_symptoms[$id] = [
          'id' => $id,
          'name' => $node->field_symptom_name->value,
        ];
      }

      if (empty($user_symptoms)) {
        $payload = [
          'success' => false,
          'status_code' => self::HTTP_OK,
          'message' => $this->t("USER_DOES_NOT_HAVE_ANY_SYMPTOM"),
          'data' => [
            'content' => [],
          ],
        ];
      } else {
        $payload = [
          'success' => true,
          'status_code' => self::HTTP_OK,
          'message' => $this->t("SUCCESSFUL"),
          'data' => [
            'content' => $user_symptoms,
          ],
        ];
      }

      $response = new ResourceResponse($payload);
      $response->getCacheableMetadata()->addCacheContexts(['user']);
      $response->addCacheableDependency(CacheableMetadata::createFromRenderArray([
        '#cache' => [
          'context' => ['user'],
          'tags' => ['user:3', 'languages:' . $userLangId],
          'max-age' => 0,
        ],
      ]));

      return $response;
    } catch (\Exception $e) {
      $response = [
        'success' => false,
        'status_code' => self::HTTP_UNPROC,
        'message' => $e->getMessage(),
        'data' => [],
      ];

      return new ResourceResponse($response);
    }
  }
}
