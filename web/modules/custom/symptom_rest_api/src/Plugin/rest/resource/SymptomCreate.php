<?php

namespace Drupal\symptom_rest_api\Plugin\rest\resource;

use Drupal\Core\Annotation\Translation;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\kabi\Validation\ApiValidation;
use Drupal\node\Entity\Node;
use Drupal\rest\Annotation\RestResource;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Provides Resource for the Symptom functionality
 *
 * @RestResource(
 *   id = "symptom_create",
 *   label = @Translation("SYMPTOM_CREATE"),
 *   uri_paths = {
 *     "canonical" = "/symptom-rest-api/create-symptom",
 *     "create" = "/symptom-rest-api/create-symptom"
 *   }
 * )
 */
class SymptomCreate extends ResourceBase
{
  const HTTP_OK = 200;
  const HTTP_UNPROC = 422;

  /**
   * Responds to entity POST requests.
   * @return \Drupal\rest\ResourceResponse
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function post(Request $request)
  {
    $content = json_decode($request->getContent(), true);

    $errors = ApiValidation::validate($content, self::getRules(), 'symptom');

    if ($errors) {
      $response = [
        'success' => false,
        'status_code' => self::HTTP_UNPROC,
        'message' => $this->t("INVALID_DATA"),
        'data' => $errors,
      ];

      return new ResourceResponse($response);
    }

    try {
      $user = \Drupal::currentUser();
      $user_id = $user->id();
    } catch (\Exception $e) {
      $response = [
        'success' => false,
        'status_code' => self::HTTP_UNPROC,
        'message' => $this->t("USER_NOT_FOUND"),
        'data' => [],
      ];

      return new ResourceResponse($response);
    }

    $account = \Drupal\user\Entity\User::load($user_id);
    if ($account->field_used_access_code[0]->target_id === null) {
      $response = [
        'success' => false,
        'status_code' => self::HTTP_OK,
        'message' => $this->t("USER_CANNOT_CREATE_SYMPTOM"),
        'data' => [
          'has_order' => false,
          'content' => $this->t("USER_DOES_NOT_HAVE_A_VALID_ACCESS_CODE"),
          'can_order' => false,
        ],
      ];

      return new ResourceResponse($response);
    }

    try {
      $nids = \Drupal::entityTypeManager()
        ->getListBuilder('node')
        ->getStorage()
        ->loadByProperties([
          'type' => "symptom",
          'field_symptom_user_ref' => $user->id(),
        ]);

      if (count($nids) >= 5) {
        throw new \Exception("LIMIT_REACHED");
      }
    } catch (\Exception $e) {
      $response = [
        'success' => false,
        'status_code' => self::HTTP_UNPROC,
        'message' => $this->t("SYMPTOM_CANNOT_BE_CREATED_DUE_LIMIT_REACHED"),
        'data' => [],
      ];

      return new ResourceResponse($response);
    }

    try {
      $symptom = [
        'type' => 'symptom',
      ];

      $symptom_name = $content['field_symptom_name'][0]['value'];

      $symptom["title"] = $symptom_name;
      $symptom["field_symptom_name"] = $symptom_name;
      $symptom["field_symptom_user_ref"] = $user_id;

      $node = Node::create($symptom);
      $node->save();
    } catch (\Exception $e) {
      $response = [
        'success' => false,
        'status_code' => self::HTTP_UNPROC,
        'message' => $this->t("SYMPTOM_CANNOT_BE_CREATED"),
        'data' => [],
      ];

      return new ResourceResponse($response);
    }

    return new ResourceResponse([
      'success' => true,
      'status_code' => self::HTTP_OK,
      'message' => $this->t("SYMPTOM_CREATED"),
      'data' => [
        'content' => [
          $node->id() => [
            'id' => $node->id(),
            'name' => $node->field_symptom_name->value,
          ],
        ],
      ],
    ]);
  }

  public static function getRules(): array
  {
    return [
      "field_symptom_name" => "required|min:1|max:128",
    ];
  }
}
