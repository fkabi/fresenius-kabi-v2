<?php

namespace Drupal\coolbag_order_rest_api\Plugin\rest\resource;

use Drupal\Core\Annotation\Translation;
use Drupal\kabi\Validation\ApiValidation;
use Drupal\node\Entity\Node;
use Drupal\rest\Annotation\RestResource;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Provides Resource for the Coolbag Order functionality
 *
 * @RestResource(
 *   id = "coolbag_place_order",
 *   label = @Translation("COOLBAG_PLACE_ORDER"),
 *   uri_paths = {
 *     "canonical" = "/place-order",
 *     "create" = "/place-order"
 *   }
 * )
 */
class CoolbagPlaceOrder extends ResourceBase
{
  const HTTP_OK = 200;
  const HTTP_UNPROC = 422;

  /**
   * Responds to entity POST requests.
   * @return \Drupal\rest\ResourceResponse
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function post(Request $request)
  {
    $content = json_decode($request->getContent(), true);

    $errors = ApiValidation::validate($content, self::getRules(), 'coolbag_order');

    if ($errors) {
      $response = [
        'success' => false,
        'status_code' => self::HTTP_UNPROC,
        'message' => $this->t("INVALID_DATA"),
        'data' => $errors,
      ];

      return new ResourceResponse($response);
    }

    try {
      $user = \Drupal::currentUser();
      $user_id = $user->id();
    } catch (\Exception $e) {
      $response = [
        'success' => false,
        'status_code' => self::HTTP_UNPROC,
        'message' => $this->t("USER_NOT_FOUND"),
        'data' => [],
      ];

      return new ResourceResponse($response);
    }

    $account = \Drupal\user\Entity\User::load($user_id);
    if ($account->field_used_access_code[0]->target_id === null) {
      $response = [
        'success' => false,
        'status_code' => self::HTTP_OK,
        'message' => $this->t("USER_CANNOT_ORDER"),
        'data' => [
          'has_order' => false,
          'content' => $this->t("USER_DOES_NOT_HAVE_A_VALID_ACCESS_CODE"),
          'can_order' => false,
        ],
      ];

      return new ResourceResponse($response);
    }

    try {
      $nids = \Drupal::entityTypeManager()
        ->getListBuilder('node')
        ->getStorage()
        ->loadByProperties([
          'type' => "coolbag_order",
          'field_order_user_reference' => $user_id,
        ]);

      $keys = array_keys($nids);
      $node = null;
      if (isset($keys[0])) {
        $key = $keys[0];
        $node = $nids[$key];
      }
    } catch (\Exception $e) {
      $response = [
        'success' => false,
        'status_code' => self::HTTP_UNPROC,
        'message' => $e->getMessage(),
        'data' => [],
      ];

      return new ResourceResponse($response);
    }

    if ($node && is_numeric($node->id())) {
      $response = [
        'success' => false,
        'status_code' => self::HTTP_OK,
        'message' => $this->t("USER_HAS_AN_ORDER_ALREADY"),
        'data' => [
          'has_order' => true,
          'can_order' => true,
        ],
      ];

      return new ResourceResponse($response);
    }

    try {
      $address_data = [
        'type' => 'coolbag_order_address',
      ];

      $title = ($content["field_order_address_address"] ? $content["field_order_address_address"][0]["value"] . ", " : "");
      $title .= ($content["field_order_address_city"] ? $content["field_order_address_city"][0]["value"] . ", " : "");
      $title .= ($content["field_order_address_postal_code"] ? $content["field_order_address_postal_code"][0]["value"] . ", " : "");
      $title .= ($content["field_order_address_country"] ? $content["field_order_address_country"][0]["value"] : "");

      ($title) ? ($address_data["title"] = $title) : "";
      ($content["field_order_address_postal_code"]) ? ($address_data["field_order_address_postal_code"] = $content["field_order_address_postal_code"][0]["value"]) : "";
      ($content["field_order_address_country"]) ? ($address_data["field_order_address_country"] = $content["field_order_address_country"][0]["value"]) : "";
      ($content["field_order_address_city"]) ? ($address_data["field_order_address_city"] = $content["field_order_address_city"][0]["value"]) : "";
      ($content["field_order_address_address"]) ? ($address_data["field_order_address_address"] = $content["field_order_address_address"][0]["value"]) : "";

      $address = Node::create($address_data);
      $address->save();
    } catch (\Exception $e) {
      $response = [
        'success' => false,
        'status_code' => self::HTTP_UNPROC,
        'message' => $this->t("COOLBAG_ORDER_ADDRESS_CANNOT_BE_CREATED"),
        'data' => [],
      ];

      return new ResourceResponse($response);
    }

    try {
      $order_address = [
        'type' => 'coolbag_order',
      ];

      $title = "Coolbag Order for ";
      $title .= ($content["field_order_patient_title"] ? $content["field_order_patient_title"][0]["value"] . " " : "");
      $title .= ($content["field_order_patient_name"] ? $content["field_order_patient_name"][0]["value"] . ", " : "");

      $order_address["title"] = $title;
      $order_address["field_order_address_reference"] = $address->id();
      $order_address["field_order_user_reference"] = $user_id;

      ($content["field_order_patient_title"]) ? ($order_address["field_order_patient_title"] = $content["field_order_patient_title"][0]["value"]) : "";
      ($content["field_order_patient_name"]) ? ($order_address["field_order_patient_name"] = $content["field_order_patient_name"][0]["value"]) : "";
      ($content["field_order_telephone_number"]) ? ($order_address["field_order_telephone_number"] = $content["field_order_telephone_number"][0]["value"]) : "";

      $order_address["field_order_consent_checkbox"] = $content["field_order_consent_checkbox"][0]["value"];

      $order = Node::create($order_address);
      $order->save();
    } catch (\Exception $e) {
      $address->delete();

      $response = [
        'success' => false,
        'status_code' => self::HTTP_UNPROC,
        'message' => $this->t("COOLBAG_ORDER_CANNOT_BE_CREATED"),
        'data' => [],
      ];

      return new ResourceResponse($response);
    }

    $site_settings = \Drupal::service('site_settings.loader');
    $settings = $site_settings->loadByFieldset('coolbag_successful_order_msg');

    return new ResourceResponse([
      'success' => true,
      'status_code' => self::HTTP_OK,
      'message' => $this->t("COOLBAG_ORDER_CREATED"),
      'data' => [
        'content' => $settings["coolbag_successful_order_msg"]["value"],
      ],
    ]);
  }

  public static function getRules(): array
  {
    return [
      "field_order_consent_checkbox" => "required|true",
      "field_order_patient_name" => "min:4|max:50",
      "field_order_telephone_number" => "pattern:/[\+]?[\(]{0,1}[0-9]{1,4}[\)]{0,1}[-\s\.\/0-9]{7,}/",
      "field_order_address_address" => "max:254",
      "field_order_address_city" => "max:63",
      "field_order_address_country" => "max:63",
      "field_order_address_postal_code" => "max:63",
    ];
  }
}
