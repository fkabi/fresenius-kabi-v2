<?php

namespace Drupal\coolbag_order_rest_api\Plugin\rest\resource;

use Drupal\Core\Annotation\Translation;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\rest\Annotation\RestResource;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Drupal\rest\ModifiedResourceResponse;

/**
 * Provides Resource for the Coolbag Order functionality
 *
 * @RestResource(
 *   id = "coolbag_order_resource",
 *   label = @Translation("COOLBAG_ORDER_RESOURCE"),
 *   uri_paths = {
 *     "canonical" = "/coolbag-order-rest-api/get-order"
 *   }
 * )
 */
class CoolbagOrderResource extends ResourceBase
{
  const HTTP_OK = 200;
  const HTTP_UNPROC = 422;

  /**
   * Responds to entity GET requests.
   * @return \Drupal\rest\ResourceResponse
   */
  public function get()
  {
    try {
      $user = \Drupal::currentUser();
      $user_id = $user->id();
      $userLangId = $user->getPreferredLangcode();

      $account = \Drupal\user\Entity\User::load($user_id);
      $access_code = $account->field_used_access_code[0]->target_id;

      if ($access_code === null) {
        $payload = [
          'success' => false,
          'status_code' => self::HTTP_OK,
          'message' => $this->t("USER_CANNOT_ORDER"),
          'data' => [
            'has_order' => false,
            'content' => $this->t("USER_DOES_NOT_HAVE_A_VALID_ACCESS_CODE"),
            'can_order' => false,
          ],
        ];

        $response =  new ResourceResponse($payload);
        $response->getCacheableMetadata()->addCacheContexts(['user']);
        $response->addCacheableDependency(CacheableMetadata::createFromRenderArray([
          '#cache' => [
            'context' => ['user'],
            'tags' => ['user:3', 'languages:' . $userLangId],
            'max-age' => 0,
          ],
        ]));
        return $response;
      }
    } catch (\Exception $e) {
      $response = [
        'success' => false,
        'status_code' => self::HTTP_UNPROC,
        'message' => $e->getMessage(),
        'data' => [],
      ];

      return new ResourceResponse($response);
    }

    try {
      $nids = \Drupal::entityTypeManager()
        ->getListBuilder('node')
        ->getStorage()
        ->loadByProperties([
          'type' => "coolbag_order",
          'field_order_user_reference' => $user_id,
        ]);

      $result = \Drupal::entityQuery('node')
        ->condition('type', 'coolbag_order')
        ->condition('field_order_user_reference', $user_id);
      $result = $result->execute();
      $query_nids = \Drupal::entityTypeManager()->getStorage('node')->loadMultiple($result);
    } catch (\Exception $e) {
      $response = [
        'success' => false,
        'status_code' => self::HTTP_UNPROC,
        'message' => $e->getMessage(),
        'data' => [],
      ];

      return new ModifiedResourceResponse($response, 200);
    }

    if (empty($nids) && empty($query_nids)) {
      $response = [
        'success' => true,
        'status_code' => self::HTTP_OK,
        'message' => $this->t("USER_DOES_NOT_HAVE_AN_ORDER_YET"),
        'data' => [
          'has_order' => false,
          'content' => '',
          'can_order' => true,
        ],
      ];
    } else {
      $site_settings = \Drupal::service('site_settings.loader');
      $settings = $site_settings->loadByFieldset('coolbag_order_cannot_order_message');

      $response = [
        'success' => false,
        'status_code' => self::HTTP_OK,
        'message' => $this->t("USER_HAS_AN_ORDER_ALREADY"),
        'data' => [
          'has_order' => true,
          'content' => $settings["coolbag_cannot_order_msg"]["value"],
          'can_order' => true,
        ],
      ];
    }
    return new ModifiedResourceResponse($response, 200);
  }
}
