<?php

namespace Drupal\coaching_tool_rest_api\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

class RouteSubscriber extends RouteSubscriberBase
{

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection)
  {
    if ($route = $collection->get('rest.coaching_tool_resource.GET')) {
      $options = $route->getOptions();
      $options['no_cache'] = TRUE;
      $route->setOptions($options);
    }
  }
}
