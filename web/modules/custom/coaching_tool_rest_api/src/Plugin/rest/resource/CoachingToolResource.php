<?php

namespace Drupal\coaching_tool_rest_api\Plugin\rest\resource;

use Drupal\Core\Annotation\Translation;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\rest\Annotation\RestResource;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;

/**
 * Provides Resource for the Coaching Tool functionality
 *
 * @RestResource(
 *   id = "coaching_tool_resource",
 *   label = @Translation("COACHING_TOOL_RESOURCE"),
 *   uri_paths = {
 *     "canonical" = "/coaching-tool-rest-api/get-coaching"
 *   }
 * )
 */
class CoachingToolResource extends ResourceBase
{
  const HTTP_OK = 200;
  const HTTP_UNPROC = 422;

  /**
   * Responds to entity GET requests.
   * @return \Drupal\rest\ResourceResponse
   */
  public function get()
  {
    try {
      $nids = \Drupal::entityTypeManager()
        ->getListBuilder('node')
        ->getStorage()
        ->loadByProperties([
          'type' => "coaching_tool",
        ]);

      $result = \Drupal::entityQuery('node')
        ->condition('type', 'coaching_tool');
      $result = $result->execute();
      $query_nids = \Drupal::entityTypeManager()->getStorage('node')->loadMultiple($result);
      return new ResourceResponse($query_nids);
    } catch (\Exception $e) {
      $response = [
        'success' => false,
        'status_code' => self::HTTP_UNPROC,
        'message' => $e->getMessage(),
        'data' => [],
      ];
      return new ResourceResponse($response);
    }

    if (empty($nids) && empty($query_nids)) {
      $response = [
        'success' => true,
        'status_code' => self::HTTP_OK,
        'message' => $this->t("NO_COURSE_YET"),
        'data' => [
          'has_course' => false,
          'content' => '',
        ],
      ];
    }
  }
}
