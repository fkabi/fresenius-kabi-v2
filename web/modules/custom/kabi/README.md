# Usage of user import

1. Go to /admin/people/bulk_user_register
2. Choose your CSV file.
3. Click import.

# CSV fields
- username
- email
- status --> ***0** for blocked, ***1*** for active users*
- role --> *“hcp” or “patient” *required*
- access_code --> ***Y** to generate access code, **N** to leave it empty.*
- field_diagnosed_condition_ref --> *The tile of diagnosed condition node or node ID.*
- field_user_first_name -> *First name*
- field_user_last_name -> *Last name*
- field_user_country -> *Name of the country*
- field_user_email_sending -> *Y or N*
- field_user_hcp_healthcare_org -> *The healthcare organisation’s name.*
- field_user_hcp_reg_number -> *Registration number.*
- field_user_title -> *Values can be: **dr, prof, mr, mrs, miss, ms***
-
On the import this is automatically create and assign new access code to the user, so you have **nothing to do**.

# Example CSV file

    username,email,status,role,access_code,field_diagnosed_condition_ref,field_user_first_name,field_user_last_name,field_user_country,field_user_email_sending,field_user_hcp_healthcare_org,field_user_hcp_reg_number,field_user_title
    dummy1@dummy.com,dummy1@dummy.com,1,hcp,Y,Crohn's disease,Dummy,1,Hungary,Y,HCP org1,regnum1,mr
    dummy2@dummy.com,dummy2@dummy.com,0,patient,Y,Non-infectious uveitis,Dummy,2,UK,N,HCP org2,regnum2,dr
    dummy3@dummy.com,dummy3@dummy.com,1,patient,N,10,Dummy,3,India,Y,HCP org3,regnum3,prof
    dummy4@dummy.com,dummy4@dummy.com,1,hcp,Y,Juvenile idiopathic arthritis,Dummy,4,Poland,Y,HCP org4,regnum4,mrs

## CSV import tech specs
For hooks see `kabi.module` file `kabi_bulk_user_registration_user_presave` and
`kabi_bulk_user_registration_user_aftersave` functions.

**Module dependecies:** [Bulk user registration](https://www.drupal.org/project/bulk_user_registration)

**Patches**: In file `web/modules/contrib/bulk_user_registration/src/BulkUserRegistration.php` added a hook for after save the user.

`\Drupal::moduleHandler()
->invokeAll('bulk_user_registration_user_aftersave', [$user, $userData]);`

# Generate access codes

- Go to Configuration -> People -> Generate access codes
- Or by URL: `/admin/people/generate_access_code`

On this form you can choose how many access codes you want to generate by set a number in
"Number of generated access codes" field.
On the second field you can set a user. If this field have been set, access codes
will be assigned to this user. The field is autocomplete, just start to type user's name.
By pressing the "Generate Access Codes" button, codes will be generated and you will see a
message after successful generation with node IDs and titles.

## Generate access code tech specs
- **access code generator class**: `web/modules/custom/kabi/src/Controller/AccessCodeController.php`.
- **form**: `web/modules/custom/kabi/src/Form/AccessCodeSettingsForm.php`.
- **menu links**: `web/modules/custom/kabi/kabi.links.menu.yml`.
- **routing**: `web/modules/custom/kabi/kabi.routing.yml`.
