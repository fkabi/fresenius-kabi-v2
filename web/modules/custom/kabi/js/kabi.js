(function ($) {
  'use strict';

  const uploadBtn = $('.view-patient-access-codes [data-drupal-selector="edit-container-for-button"] input.button');
  uploadBtn.on('click', e => {
    e.preventDefault();

    if ($('.after-upload-success-message').length > 0) {
      $('.after-upload-success-message').remove();
    }

    const file = $('.view-patient-access-codes [data-drupal-selector="edit-container-for-button"] input[name="files[file]"]');

    getCSRFToken(file);
  });

  async function uploadFile(csrf, file) {
    const payload = {
      filename: file[0].files[0].name
    };

    $.ajax({
      async: true,
      url: '/api/json/access_codes/upload?_format=json',
      method: "POST",
      data: JSON.stringify(payload),
      type: 'post',
      dataType: 'json',
      processData: false,
      contentType: false,
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'X-CSRF-TOKEN': csrf
      },
      success: (r) => {
        $('.block-page-title-block').append(`<div class="after-upload-success-message">${r.message}</div>`);
      }
    });
  }

  function getCSRFToken(file) {
    $.ajax({
      url: '/session/token',
      method: "GET",
      dataType: 'json',
    })
      .always(function (response) {
        return uploadFile(response.responseText, file)
          .catch(error => {
            console.log('error', error);
          })
          .finally(response => {
            if (typeof response !== 'undefined') {
              console.log('finally', response);
            }
          });
        return response.responseText;
      });
  }
})(jQuery);
