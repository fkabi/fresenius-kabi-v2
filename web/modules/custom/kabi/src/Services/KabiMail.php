<?php

namespace Drupal\kabi\Services;

/**
 * Class KabiMail.
 */
class KabiMail {
  /**
   * Route.
   *
   * @var route
   */
  protected $mailer;

  /**
   * Current user.
   *
   * @var $current_user
   */
  protected $current_user;

  /**
   * Constructor.
   *
   * @param object $mailer
   *   Mailer.
   * @param object $current_user
   *   Current user.
   */
  public function __construct($mailer, $current_user) {
    $this->mailer = $mailer;
    $this->current_user = $current_user;
  }

  /**
   * Sends the mails.
   *
   * @param array $params
   *   Mail parameters.
   */
  public function sendMail(array $params = []) {
    // Build mail vars.
    $module = 'kabi';
    $key = 'kabi_html_mail';
    $lang_code = $this->current_user->getPreferredLangcode();
    $params['base_url'] = \Drupal::request()->getSchemeAndHttpHost();
    $params['sender_name'] = $this->current_user->getAccountName();

    // Send emails.
    $users = $params['users'];
    $user_count = count($users);
    foreach ($users as $user) {
      $params['name_recipient'] = $user->name;
      $result = $this->mailer->mail($module, $key, $user->mail, $lang_code, $params, NULL, TRUE);
    }

    if ($result['result'] === TRUE) {
      \Drupal::messenger()->addStatus($user_count . ' ' . t('user(s) notified successfully.'));
    }
    else {
      \Drupal::messenger()->addError(t('Unable to send emails, please contact administrator!'));
    }

  }

}
