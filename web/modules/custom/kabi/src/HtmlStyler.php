<?php

namespace Drupal\kabi;

use Drupal\Core\Asset\LibraryDiscoveryInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Extension\ThemeHandlerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Render\Markup;
use Drupal\Core\Theme\ThemeManagerInterface;
use Drupal\Core\Url;
use Pelago\Emogrifier\CssInliner;

class HtmlStyler
{
  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Drupal\Core\Theme\ThemeManagerInterface definition.
   *
   * @var \Drupal\Core\Theme\ThemeManagerInterface
   */
  protected $themeManager;

  /**
   * Drupal\Core\File\FileSystemInterface definition.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * Drupal\Core\Asset\LibraryDiscoveryInterface definition.
   *
   * @var \Drupal\Core\Asset\LibraryDiscoveryInterface
   */
  protected $libraryDiscovery;

  /**
   * Drupal\Core\Extension\ThemeHandlerInterface definition.
   *
   * @var \Drupal\Core\Extension\ThemeHandlerInterface
   */
  protected $themeHandler;

  /**
   * Drupal\Core\Extension\ModuleHandlerInterface definition.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Emogrify constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   * @param \Drupal\Core\Theme\ThemeManagerInterface $theme_manager
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   * @param \Drupal\Core\Asset\LibraryDiscoveryInterface $library_discovery
   * @param \Drupal\Core\Extension\ThemeHandlerInterface $theme_handler
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   */
  public function __construct(ConfigFactoryInterface $config_factory, ThemeManagerInterface $theme_manager, FileSystemInterface $file_system, LibraryDiscoveryInterface $library_discovery, ThemeHandlerInterface $theme_handler, ModuleHandlerInterface $module_handler)
  {
    $this->configFactory = $config_factory;
    $this->themeManager = $theme_manager;
    $this->fileSystem = $file_system;
    $this->libraryDiscovery = $library_discovery;
    $this->themeHandler = $theme_handler;
    $this->moduleHandler = $module_handler;
  }

  /**
   * Get all the libraries given a theme.
   *
   * @param $theme
   * @return array
   */
  protected function getThemeLibraries($theme)
  {
    $libraries = [];
    $themes = $this->themeHandler->rebuildThemeData();
    $root = \Drupal::root();

    if (isset($themes[$theme])) {
      /** @var \Drupal\Core\Extension\Extension $extension */
      $extension = $themes[$theme];
      $library_file = $extension->getPath() . '/' . $theme . '.libraries.yml';
      if (is_file($root . '/' . $library_file)) {
        $libraries = $this->libraryDiscovery->getLibrariesByExtension($theme);
      }
    }

    return $libraries;
  }

  /**
   * Gets the libraries to use for the CssInliner.
   *
   * @return array
   */
  protected function getCssLibraries()
  {
    return [
      'kabi',
    ];
  }

  /**
   * Gets the css to apply on mail.
   *
   * @return string
   */
  protected function getCss($theme)
  {
    $css = $this->getCssOverride();

    if (empty($css)) {
      $css = $this->getCssTheme($theme);
    }

    $css_injector = $this->getCssInjector();

    if (!empty($css_injector)) {
      $css = $css . PHP_EOL . $css_injector;
    }

    return $css;
  }

  /**
   * Gets the css from default theme.
   *
   * @return string
   */
  protected function getCssTheme($theme)
  {
    $css = '';
    $libraries = $this->getThemeLibraries($theme);
    $css_libraries = $this->getCssLibraries();

    foreach ($css_libraries as $css_library) {
      if (isset($libraries[$css_library]['css'])) {
        $theme_css_files = $libraries[$css_library]['css'];

        foreach ($theme_css_files as $key => $theme_css_file) {
          if (empty($theme_css_file['data'])) {
            continue;
          }

          $data = $theme_css_file['data'];
          $data_file = $this->fileSystem->realpath($data);
          $css .= PHP_EOL . file_get_contents($data_file);
        }
      }
    }

    return $css;
  }

  /**
   * Gets the css override from usine_theme module.
   *
   * @return string
   */
  protected function getCssOverride()
  {
    $css = '';

    if ($this->moduleHandler->moduleExists('usine_theme')) {
      /** @var \Drupal\usine_theme\ManagerAssetInterface $manager */
      $manager = \Drupal::service('usine_theme.manager');

      if ($manager->hasAssetOverride('color')) {
        $data = $manager->getAsset('mail');

        if ($data) {
          $data = ltrim($data, '/');
          $data_file = $this->fileSystem->realpath($data);
          $css = file_get_contents($data_file);
        }
      }
    }

    return $css;
  }

  /**
   * Gets the css for asset_injector module.
   *
   * @return string
   */
  protected function getCssInjector()
  {
    $css = '';

    if ($this->moduleHandler->moduleExists('asset_injector')) {
      foreach (asset_injector_get_assets(TRUE, ['asset_injector_css']) as $asset) {
        if (strpos($asset->id(), 'mail') !== FALSE) {
          $css_content = $asset->getCode();
          $css = $css . PHP_EOL . $css_content;
        }
      }
    }

    return $css;
  }

  private function addLogo()
  {
    $base_url = Url::fromUserInput('/', ['absolute' => TRUE])->toString();
    $url_info = parse_url($base_url);
    $trimmed_url = $url_info['scheme'] . '://' . $url_info['host'];
    $default_sites_library = "/sites/default";
    $url = $trimmed_url . $default_sites_library . theme_get_setting('logo.url');

    $logo_alt = t("LOGO_ALT");
    $logo_title = t("LOGO_TITLE");

    return '<img src="' . $url . '" class="mail-logo" alt="' . $logo_alt . '" title="' . $logo_title . '" />';
  }

  /**
   * {@inheritdoc}
   */
  public function style($html, $theme = 'kabi', $mail_type = null)
  {
    if ($mail_type !== null) {
      $html = $this->stringToHtml($html);
    }

    if ($html instanceof Markup) {
      $html = (string)$html;
    }

    if (!is_string($html)) {
      return $html;
    }

    $css = $this->getCss($theme);

    if (empty($css)) {
      return $html;
    }

    $header_logo = $this->addLogo();

    $html = $header_logo . $html;

    try {
      $html_inline_css = CssInliner::fromHtml($html)->inlineCss($css)->renderBodyContent();
    } catch (\Exception $e) {
      return $html;
    }

    $html = Markup::create($html_inline_css);

    return $html;
  }

  private function stringToHtml($html)
  {
    $from = [
      "{tag:p}",
      "{tag:/p}",
      "{tag:div}",
      "{tag:/div}",
      "{tag:ul}",
      "{tag:/ul}",
      "{tag:li}",
      "{tag:/li}",
      "{tag:h1}",
      "{tag:/h1}",
      "{tag:h2}",
      "{tag:/h2}",
      "{tag:h3}",
      "{tag:/h3}",
    ];

    $to = [
      '<p>',
      '</p>',
      "<div>",
      "</div>",
      "<ul>",
      "</ul>",
      "<li>",
      "</li>",
      "<h1>",
      "</h1>",
      "<h2>",
      "</h2>",
      "<h3>",
      "</h3>",
    ];

    return str_replace($from, $to, $html);
  }
}
