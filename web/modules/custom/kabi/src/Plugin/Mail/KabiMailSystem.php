<?php

namespace Drupal\kabi\Plugin\Mail;

use Drupal\Component\Utility\Unicode;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Mail\MailInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Site\Settings;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\htmlmail\Utility\HtmlMailMime;
use Egulias\EmailValidator\EmailValidator;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\File\MimeType\MimeTypeGuesserInterface;

/**
 * Modify the Drupal mail system to use HTML Mail when sending emails.
 *
 * @Mail(
 *   id = "kabimail",
 *   label = @Translation("Kabi Mail mailer"),
 *   description = @Translation("Sends the message using Kabi Mail.")
 * )
 */
class KabiMailSystem implements MailInterface, ContainerFactoryPluginInterface {

  /**
   * The email validator service.
   *
   * @var \Egulias\EmailValidator\EmailValidator
   */
  protected EmailValidator $emailValidator;

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected ModuleHandlerInterface $moduleHandler;

  /**
   * The file system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected FileSystemInterface $fileSystem;

  /**
   * The logger service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected LoggerChannelFactoryInterface $logger;

  /**
   * The site settings service.
   *
   * @var \Drupal\Core\Site\Settings
   */
  protected Settings $siteSettings;

  /**
   * The render service.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected RendererInterface $renderer;

  /**
   * The mime type guesser service.
   *
   * @var \Symfony\Component\HttpFoundation\File\MimeType\MimeTypeGuesserInterface
   */
  protected MimeTypeGuesserInterface $mimeType;

  protected \Drupal\Core\Config\ConfigFactoryInterface $configFactory;
  protected \Drupal\Core\Config\ImmutableConfig $systemConfig;
  protected \Drupal\Core\Config\ImmutableConfig $configVariables;

  /**
   * KabiMailSystem constructor.
   *
   * @param array $configuration
   *   The configuration array.
   * @param int $plugin_id
   *   Plugin ID.
   * @param string $plugin_definition
   *   Plugin definition.
   * @param \Egulias\EmailValidator\EmailValidator $emailValidator
   *   The email validator service.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   The module handler service.
   * @param \Drupal\Core\File\FileSystemInterface $fileSystem
   *   The file system service.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger
   *   The logger service.
   * @param \Drupal\Core\Site\Settings $settings
   *   The site settings service.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The render service.
   * @param \Symfony\Component\HttpFoundation\File\MimeType\MimeTypeGuesserInterface $mimeTypeGuesser
   *   The mime type guesser service.
   */
  public function __construct(
                                  $configuration,
                                  $plugin_id,
                                  $plugin_definition,
    EmailValidator                $emailValidator,
    ModuleHandlerInterface        $moduleHandler,
    FileSystemInterface           $fileSystem,
    LoggerChannelFactoryInterface $logger,
    Settings                      $settings,
    RendererInterface             $renderer,
    MimeTypeGuesserInterface      $mimeTypeGuesser
  ) {
    $this->emailValidator = $emailValidator;
    $this->moduleHandler = $moduleHandler;
    $this->fileSystem = $fileSystem;
    $this->logger = $logger;
    $this->systemConfig = \Drupal::config('system.site');
    $this->configVariables = \Drupal::config('htmlmail.settings');
    $this->siteSettings = $settings;
    $this->renderer = $renderer;
    $this->mimeType = $mimeTypeGuesser;
    $this->configFactory = \Drupal::configFactory();
  }

  /**
   * Let full HTMl format.
   *
   * @param array $message
   *   An associative array with at least the following parts:
   *   - headers: An array of (name => value) email headers.
   *   - body: The text/plain or text/html message part.
   *
   * @return array
   *   The formatted $message, ready for sending.
   */
  public function format(array $message): array {
    $eol = $this->siteSettings->get('mail_line_endings', PHP_EOL);
    $message['body'] = implode("$eol$eol", $message['body']);
    return $message;
  }

  public function mail(array $message) {
    $eol = $this->siteSettings->get('mail_line_endings', PHP_EOL);
    $params = [];
    // Ensure that subject is non-null.
    $message += ['subject' => t('(No subject)')];
    // Check for empty recipient.
    if (empty($message['to'])) {
      if (empty($message['headers']['To'])) {
        $this->getLogger()->error('Cannot send email about %subject without a recipient.', [
          '%subject' => $message['subject'],
        ]);
        return FALSE;
      }
      $message['to'] = $message['headers']['To'];
    }

    if (class_exists('HtmlMailMime')) {
      $mime = new HtmlMailMime($this->logger, $this->siteSettings, $this->mimeType, $this->fileSystem);
      $to = $mime->mimeEncodeHeader('to', $message['to']);
      $subject = $mime->mimeEncodeHeader('subject', $message['subject']);
      $txt_headers = $mime->mimeTxtHeaders($message['headers']);
    }
    else {
      $to = Unicode::mimeHeaderEncode($message['to']);
      $subject = Unicode::mimeHeaderEncode($message['subject']);
      $txt_headers = $this->txtHeaders($message['headers']);
    }

    $body = preg_replace('#(\r\n|\r|\n)#s', $eol, $message['body']);
    // Check for empty body.
    if (empty($body)) {
      $this->getLogger()->warning('Refusing to send a blank email to %recipient about %subject.', [
        '%recipient' => $message['to'],
        '%subject' => $message['subject'],
      ]);
      return FALSE;
    }
    if ($this->configVariables->get('debug')) {
      $params = [
        $to,
        $subject,
        mb_substr($body, 0, min(80, strpos("\n", $body))) . '...',
        $txt_headers,
      ];
    }
    if (isset($message['headers']['Return-Path'])) {
      // A return-path was set.
      if (isset($_SERVER['WINDIR']) || strpos($_SERVER['SERVER_SOFTWARE'], 'Win32') !== FALSE) {
        // On Windows, PHP will use the value of sendmail_from for the
        // Return-Path header.
        $old_from = ini_get('sendmail_from');
        ini_set('sendmail_from', $message['headers']['Return-Path']);
        $result = @mail($to, $subject, $body, $txt_headers);
        ini_set('sendmail_from', $old_from);
      }
      else {
        // On most non-Windows systems, the "-f" option to the sendmail command
        // is used to set the Return-Path.
        // We validate the return path, unless it is equal to the site mail,
        // which we assume to be safe.
        $site_mail = $this->getDefaultFromMail();
        $extra = ($site_mail === $message['headers']['Return-Path'] || static::isShellSafe($message['headers']['Return-Path'])) ? '-f' . $message['headers']['Return-Path'] : '';
        $result = @mail($to, $subject, $body, $txt_headers, $extra);
        if ($this->configVariables->get('debug')) {
          $params[] = $extra;
        }
      }
    }
    else {
      // No return-path was set.
      $result = @mail($to, $subject, $body, $txt_headers);
    }
    if (!$result && $this->configVariables->get('debug')) {
      $call = '@mail(' . implode(', ', $params) . ')';
      foreach ($params as $i => $value) {
        $params[$i] = var_export($value, TRUE);
      }
      $trace = print_r(debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS), TRUE);
      $this->getLogger()->info('Mail sending failed because:<br /><pre>@call</pre><br />returned FALSE.<br /><pre>@trace</pre>', [
        '@call' => $call,
        '@trace' => $trace,
      ]);
    }
    return $result;
  }

  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): KabiMailSystem {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('email.validator'),
      $container->get('module_handler'),
      $container->get('file_system'),
      $container->get('logger.factory'),
      $container->get('settings'),
      $container->get('renderer'),
      $container->get('file.mime_type.guesser')
    );
  }

}
