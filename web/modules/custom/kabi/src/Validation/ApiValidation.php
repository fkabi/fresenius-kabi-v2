<?php

namespace Drupal\kabi\Validation;

class ApiValidation
{
  public static function validate(array $content, array $rules, string $entity_type): array
  {
    $errors = [];

    $fields = array_filter(
      \Drupal::service('entity_field.manager')->getFieldDefinitions('node', $entity_type),
      function ($fieldDefinition) {
        return $fieldDefinition instanceof \Drupal\field\FieldConfigInterface;
      }
    );

    $result = [];
    foreach ($fields as $key => $definition) {
      $result[$key] = [
        'readable_label' => $definition->getLabel(),
      ];
    }

    foreach ($rules as $field_name => $item) {
      $ruleset = explode("|", $item);

      if (in_array('required', $ruleset)) {

        if (!isset($content[$field_name]) || !isset($content[$field_name][0])) {
          if((!isset($content[$field_name][0]["value"]) || strlen($content[$field_name][0]["value"]) < 1) || (!isset($content[$field_name][0]["target_id"]) || strlen($content[$field_name][0]["target_id"]) < 1)){
            $data = [
              "error_title" => t('FIELD_IS_REQUIRED'),
            ];

            if ($result[$field_name]["readable_label"] !== null) {
              $data["field_label"] = $result[$field_name]["readable_label"];
            }

            $errors[$field_name][] = $data;
            continue;
          }
        }
      }

      if (in_array('true', $ruleset)) {
        if (!$content[$field_name][0]["value"]) {
          $data = [
            "error_title" => t('FIELD_VALUE_MUST_BE_TRUE'),
          ];

          if ($result[$field_name]["readable_label"] !== null) {
            $data["field_label"] = $result[$field_name]["readable_label"];
          }

          $errors[$field_name][] = $data;
        }
      }

      if (strpos($item, 'min:') !== false) {
        preg_match('/(min:){1,}\d*/', $item, $matches, PREG_UNMATCHED_AS_NULL);
        $min = (int)str_replace('min:', '', $matches[0]);
        unset($matches);

        if (isset($content[$field_name][0]["value"]) && strlen($content[$field_name][0]["value"]) < $min) {
          $data = [
            "error_title" => t('FIELD_VALUE_TOO_SHORT'),
          ];

          if ($result[$field_name]["readable_label"] !== null) {
            $data["field_label"] = $result[$field_name]["readable_label"];
          }

          $errors[$field_name][] = $data;
        }
      }

      if (strpos($item, 'max:') !== false) {
        preg_match('/(max:){1,}\d*/', $item, $matches, PREG_UNMATCHED_AS_NULL);
        $max = (int)str_replace('max:', '', $matches[0]);
        unset($matches);

        if (isset($content[$field_name][0]["value"]) && strlen($content[$field_name][0]["value"]) > $max) {
          $data = [
            "error_title" => t('FIELD_VALUE_TOO_LONG'),
          ];

          if ($result[$field_name]["readable_label"] !== null) {
            $data["field_label"] = $result[$field_name]["readable_label"];
          }

          $errors[$field_name][] = $data;
        }
      }

      if (strpos($item, "pattern:") !== false) {
        foreach ($ruleset as $key => $rule) {
          if (strpos($rule, "pattern:") !== false) {
            $pattern = str_replace("pattern:", "", $rule);
            break;
          }
        }

        if(isset($content[$field_name][0]["value"])){
          preg_match($pattern, $content[$field_name][0]["value"], $matches, PREG_UNMATCHED_AS_NULL);
          if (!$matches) {
            $data = [
              "error_title" => t('INVALID_FORMAT'),
            ];

            if ($result[$field_name]["readable_label"] !== null) {
              $data["field_label"] = $result[$field_name]["readable_label"];
            }

            $errors[$field_name][] = $data;
          }
        }
      }
    }

    return $errors;
  }
}
