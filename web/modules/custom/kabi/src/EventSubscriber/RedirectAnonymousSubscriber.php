<?php

namespace Drupal\kabi\EventSubscriber;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Event subscriber subscribing to KernelEvents::REQUEST.
 */
class RedirectAnonymousSubscriber implements EventSubscriberInterface
{

  public function __construct()
  {
    $this->account = \Drupal::currentUser();
  }

  public function checkAuthStatus(GetResponseEvent $event)
  {
    $anonymous_routes = [
      'user.login',
      'user.login.http',
      'rest.hcp_register.POST',
      'rest.user_registration.POST',
      'rest.diagnosed_condition.GET',
      'oauth2_token.token',
      'rest.patient_register.POST',
      'rest.password_renewal_get.GET',
      'rest.forgot_password_post.POST',
      'rest.change_password_post.POST',
      'rest.page_content_get.GET',
      'rest.suggested_goal_get.GET',
    ];

    if ($this->account->isAnonymous() && !in_array(\Drupal::routeMatch()->getRouteName(), $anonymous_routes)) {
      // add logic to check other routes you want available to anonymous users,
      // otherwise, redirect to login page.
      $route_name = \Drupal::routeMatch()->getRouteName();
      if (
        (
          strpos($route_name, 'view') === 0 &&
          strpos($route_name, 'rest_') !== FALSE
        ) ||
        strpos($route_name, 'user.pass') !== FALSE ||
        strpos($route_name, 'user.register') !== FALSE
      ) {
        return;
      }

      $response = new RedirectResponse('/user/login', 301);
      $event->setResponse($response);
      $event->stopPropagation();
    }
  }

  public static function getSubscribedEvents()
  {
    $events[KernelEvents::REQUEST][] = array('checkAuthStatus');
    return $events;
  }

}
