<?php

namespace Drupal\kabi\EventSubscriber;

use Drupal\Component\EventDispatcher\ContainerAwareEventDispatcher;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Component\Serialization\Xml;
use Drupal\Component\Serialization\Json;

/**
 * Class ExampleLoginListener.
 *
 * @package Drupal\example
 */
class KabiLoginListener implements EventSubscriberInterface
{
  /**
   * @var path.current service
   */
  private $currentPath;

  /**
   * Constructor with dependency injection
   */
  public function __construct($currentPath)
  {
    $this->currentPath = $currentPath;
  }

  /**
   * Add User Roles to user login API response
   */
  public function onHttpLoginResponse(FilterResponseEvent $event)
  {
    if ($this->currentPath->getPath() !== '/user/login') {
      return;
    }

    $response = $event->getResponse();

    if ($response->getStatusCode() === 400 && $response->getContent() === '{"message":"The user has not been activated or is blocked."}') {
      if ($decoded = Json::decode($response->getContent())) {
        $message = 'Your account has not been activated. Please contact KabiCare support for assistance.';

        $site_settings = \Drupal::service('site_settings.loader');
        $settings = $site_settings->loadByFieldset('deactivated_user_warning_message');

        if ($settings && $setting = $settings['deactivated_user_warning_message']) {
          $message = $setting;
        }

        $decoded['message'] = $message;
        $decoded['can_login'] = false;
        $decoded['user_blocked'] = true;

        $response->setContent(Json::encode($decoded));
        $event->setResponse($response);
      }
    }

    if ($response->getStatusCode() !== 200) {
      return;
    }

    $request = $event->getRequest();

    if ($request->query->get('_format') !== 'json') {
      return;
    }

    if ($content = $response->getContent()) {
      if ($decoded = Json::decode($content)) {
        $user = \Drupal::currentUser();

        $decoded['roles'] = $user->getRoles();

        $response->setContent(Json::encode($decoded));
        $event->setResponse($response);
      }
    }
  }

  /**
   * The subscribed events.
   */
  public static function getSubscribedEvents()
  {
    $events = [];
    $events[KernelEvents::RESPONSE][] = ['onHttpLoginResponse'];

    return $events;
  }
}
