<?php

namespace Drupal\kabi\EventSubscriber;

use Drupal\Component\Serialization\Json;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Event subscriber subscribing to KernelEvents::REQUEST.
 */
class PasswordAgeSubscriber implements EventSubscriberInterface
{

  public function __construct()
  {
    $this->account = \Drupal::currentUser();
  }

  public function checkPasswordStatus(GetResponseEvent $event)
  {
    if (\Drupal::routeMatch()->getRouteName() !== 'user.login.http') {
      return;
    }

    $request = $event->getRequest();
    $content = json_decode($request->getContent(), true);
    $user = user_load_by_mail($content['name']);
    if (!$user) {
      return;
    }

    if ($user->id() === 1 || $user->id() === '1') {
      return;
    }

    if ($user->field_user_password_set[0] === null || $user->field_user_password_set[0]->value === null) {
      $response = new RedirectResponse('/user/password-renewal?_format=json', 301);
      $event->setResponse($response);
      $event->stopPropagation();
    }

    $date = strtotime($user->field_user_password_set[0]->value);
    $now = strtotime('now');
    $elapsed_time = $now - $date;
    $threshold = 60 * 60 * 24 * 180;

    if ($threshold < $elapsed_time) {
      $response = new RedirectResponse('/user/password-renewal?_format=json', 301);
      $event->setResponse($response);
      $event->stopPropagation();
    }

    return;
  }

  public static function getSubscribedEvents()
  {
    $events[KernelEvents::REQUEST][] = array('checkPasswordStatus');
    return $events;
  }

}
