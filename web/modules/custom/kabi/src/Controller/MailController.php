<?php

namespace Drupal\kabi\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\kabi\Services\KabiMail;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class MailController.
 */
class MailController extends ControllerBase {

  /**
   * @var mail_service
   */
  protected $mail_service;

  /**
   * Constructor
   */
  public function __construct(KabiMail $mail_service) {
    $this->mail_service = $mail_service;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('kabi.mail')
    );
  }

  /**
   * Sends a test mail.
   */
  public function sendTestMail() {
    // Build mail params.
    $params['subject'] = t('Test email');
    $params['cta_url'] = '/node/1';
    $params['message'] = t('Someone just posted new content:');
    $params['cta_text'] = t('View new post');
    $params['bold_text'] = t('Example title / subject');
    $params['lower_body'] = t('This is a lower body example text.');
    $params['body'][] = t('This is a body example text.');
    $params['users'] = $this->getAllUsers();
    // Send mail via service.
    $mail_service = \Drupal::service('kabi.mail');
    $mail_service->sendMail($params);
    return [];
  }

  /**
   * Helper function to get all users.
   * @return mixed
   */
  private function getAllUsers() {
    $query = \Drupal::database()->select('users_field_data', 'ufd');
    $query->addField('ufd', 'name');
    $query->addField('ufd', 'mail');
    $query->condition('ufd.status', 1);
    $query->condition('ufd.uid', 1);
    return $query->execute()->fetchAll();
  }

}
