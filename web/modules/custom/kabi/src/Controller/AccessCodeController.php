<?php

namespace Drupal\kabi\Controller;

use Drupal\Component\Utility\Random;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\node\Entity\Node;

/**
 * Class AccessCodeController.
 */
class AccessCodeController extends ControllerBase {

  /**
   * Generate access_code content with random code.
   *
   * @param object|NULL $user
   *   The user object or null to leave it unoccupied.
   * @param int $num
   *   Number of generated access codes.
   *
   * @return array
   *   Array of nodes generated.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function generateAccessCode(object $user = NULL, int $num = 1) {
    $nodes = [];
    for ($i = 0; $i < $num; $i++) {
      $timestamp = \Drupal::time()->getRequestTime();
      $date = DrupalDateTime::createFromTimestamp($timestamp, 'UTC');
      $random = new Random();
      $access_code = $random->name(8, TRUE);
      $options = [];
      $options['type'] = 'access_code';
      $options['title'] = $access_code;
      $options['field_access_code_code'] = [
        'value' => $access_code,
      ];
      $options['uid'] = 1;
      if ($user) {
        $options['field_access_code_assigned_to'] = [
          'target_id' => $user->id(),
        ];
        $options['field_access_code_assign_date'] = [
          'value' => $date->format("Y-m-d\TH:i:s"),
        ];
        $options['field_access_code_is_used'] = [
          'value' => TRUE,
        ];
        $options['uid'] = $user->id();
      }
      $node = Node::create($options);
      $node->save();
      $nodes[] = $node;
    }
    return $nodes;
  }
}
