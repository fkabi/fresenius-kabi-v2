<?php
namespace Drupal\kabi\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\kabi\Controller\AccessCodeController;
use Drupal\user\Entity\User;

/**
 * Configure example settings for this site.
 */
class AccessCodeSettingsForm extends FormBase {
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'access_code_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['access_code_number'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Number of generated access codes'),
      '#default_value' => 1,
    ];
    $form['user_id'] = [
      '#type' => 'entity_autocomplete',
      '#title' => $this->t('User ID to assign this access code to. Leave it empty if you do not want to assign that.'),
      '#target_type' => 'user',
    ];

    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => $this->t('Generate access codes'),
      '#button_type' => 'primary',
    );

    return $form;
  }

  public function validateForm(array &$form, FormStateInterface $form_state) {
    if(strlen($form_state->getValue('access_code_number')) < 1) {
      $form_state->setErrorByName('access_code_number', $this->t('Please enter a number greater than 0'));
    }
    if(strlen($form_state->getValue('access_code_number')) > 99) {
      $form_state->setErrorByName('access_code_number', $this->t('Please enter a number greater than 100'));
    }
  }

  /**
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @return void
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $access_code_num = $form_state->getValue('access_code_number');
    $user_id = $form_state->getValue('user_id');
    $user = null;
    if ($user_id && is_numeric($user_id)) {
      $user = User::load($user_id);;
    }
    $nodes = (new AccessCodeController)->generateAccessCode($user, $access_code_num);
    foreach ($nodes as $node) {
      \Drupal::messenger()->addMessage(
        t(
          "Access codes generated Node ID: %id, Access code: %access_code.",
          [
            '%id' => $node->id(),
            '%access_code' => $node->getTitle(),
          ]
        )
      );
    }
  }

}
