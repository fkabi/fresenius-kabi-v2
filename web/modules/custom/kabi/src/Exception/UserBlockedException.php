<?php

namespace Drupal\kabi\Exception;

use Exception;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

class UserBlockedException extends Exception
{
  /**
   * @var int
   */
  private $httpStatusCode;

  /**
   * @var string
   */
  private $errorType;

  /**
   * @var null|string
   */
  private $hint;

  /**
   * @var null|string
   */
  private $redirectUri;

  /**
   * @var array
   */
  private $payload;

  /**
   * @var ServerRequestInterface
   */
  private $serverRequest;

  /**
   * Throw a new exception.
   *
   * @param string $message Error message
   * @param int $code Error code
   * @param string $errorType Error type
   * @param int $httpStatusCode HTTP status code to send (default = 400)
   * @param null|string $hint A helper hint
   * @param null|string $redirectUri A HTTP URI to redirect the user back to
   * @param Throwable $previous Previous exception
   */
  public function __construct($message, $code, $errorType, $httpStatusCode = 400, $hint = null, $redirectUri = null, Throwable $previous = null)
  {
    parent::__construct($message, $code, $previous);
    $this->httpStatusCode = $httpStatusCode;
    $this->errorType = $errorType;
    $this->hint = $hint;
    $this->redirectUri = $redirectUri;
    $this->payload = [
      'error' => $errorType,
      'error_description' => $message,
    ];
    if ($hint !== null) {
      $this->payload['hint'] = $hint;
    }
  }

  /**
   * Returns the current payload.
   *
   * @return array
   */
  public function getPayload()
  {
    $payload = $this->payload;

    // The "message" property is deprecated and replaced by "error_description"
    // TODO: remove "message" property
    if (isset($payload['error_description']) && !isset($payload['message'])) {
      $payload['message'] = $payload['error_description'];
    }

    return $payload;
  }

  /**
   * Updates the current payload.
   *
   * @param array $payload
   */
  public function setPayload(array $payload)
  {
    $this->payload = $payload;
  }

  /**
   * Set the server request that is responsible for generating the exception
   *
   * @param ServerRequestInterface $serverRequest
   */
  public function setServerRequest(ServerRequestInterface $serverRequest)
  {
    $this->serverRequest = $serverRequest;
  }

  public static function blockedUserError()
  {
    $message = 'Your account has not been activated. Please contact KabiCare support for assistance.';

    $site_settings = \Drupal::service('site_settings.loader');
    $settings = $site_settings->loadByFieldset('deactivated_user_warning_message');

    if ($settings && $setting = $settings['deactivated_user_warning_message']) {
      $message = $setting;
    }

    return new static($message, 6, 'user_is_blocked', 401);
  }

  public function generateHttpResponse(ResponseInterface $response, $useFragment = false, $jsonOptions = 0)
  {
    $headers = $this->getHttpHeaders();

    $payload = $this->getPayload();

    if ($this->redirectUri !== null) {
      if ($useFragment === true) {
        $this->redirectUri .= (\strstr($this->redirectUri, '#') === false) ? '#' : '&';
      } else {
        $this->redirectUri .= (\strstr($this->redirectUri, '?') === false) ? '?' : '&';
      }

      return $response->withStatus(302)->withHeader('Location', $this->redirectUri . \http_build_query($payload));
    }

    foreach ($headers as $header => $content) {
      $response = $response->withHeader($header, $content);
    }

    $responseBody = \json_encode($payload, $jsonOptions) ?: 'JSON encoding of payload failed';

    $response->getBody()->write($responseBody);

    return $response->withStatus($this->getHttpStatusCode());
  }

  public function getHttpHeaders()
  {
    $headers = [
      'Content-type' => 'application/json',
    ];

    // Add "WWW-Authenticate" header
    //
    // RFC 6749, section 5.2.:
    // "If the client attempted to authenticate via the 'Authorization'
    // request header field, the authorization server MUST
    // respond with an HTTP 401 (Unauthorized) status code and
    // include the "WWW-Authenticate" response header field
    // matching the authentication scheme used by the client.
    // @codeCoverageIgnoreStart
    if ($this->errorType === 'invalid_client' && $this->serverRequest->hasHeader('Authorization') === true) {
      $authScheme = \strpos($this->serverRequest->getHeader('Authorization')[0], 'Bearer') === 0 ? 'Bearer' : 'Basic';

      $headers['WWW-Authenticate'] = $authScheme . ' realm="OAuth"';
    }
    // @codeCoverageIgnoreEnd
    return $headers;
  }

  public function getHttpStatusCode()
  {
    return $this->httpStatusCode;
  }
}
