<?php

namespace Drupal\upload_access_code_rest_api\Controller;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Routing\TrustedRedirectResponse;

class PermissionController
{
  public function checkUserAccess(AccountInterface $account)
  {
    if ($account->id() === '1' || $account->id() === 1) {
      return AccessResult::allowed();
    }
    return AccessResult::forbidden();
  }
}
