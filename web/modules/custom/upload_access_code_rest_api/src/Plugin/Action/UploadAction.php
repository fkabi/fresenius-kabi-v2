<?php

namespace Drupal\upload_access_code_rest_api\Plugin\Action;

use Drupal\Core\Action\ActionBase;
use Drupal\Core\Annotation\Action;
use Drupal\Core\Annotation\Translation;
use Drupal\Core\Session\AccountInterface;

/**
 * create custom action
 *
 * @Action(
 *   id = "node_upload_access_codes_action",
 *   label = @Translation("Upload Selected Access Codes"),
 *   type = "node"
 * )
 */
class UploadAction extends ActionBase {

  /**
   * {@inheritdoc}
   */
  public function execute($node = NULL) {
    if ($node) {
      // TODO: export your node here
      \Drupal::messenger()->addStatus('node is exported ');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function access($object, AccountInterface $account = NULL, $return_as_object = FALSE) {
    /** @var \Drupal\node\NodeInterface $object */
    // TODO: write here your permissions
    $result = $object->access('create', $account, TRUE);
    return $return_as_object ? $result : $result->isAllowed();
  }

}
