<?php

namespace Drupal\upload_access_code_rest_api\Plugin\rest\resource;

use Drupal\Core\Annotation\Translation;
use Drupal\Core\File\FileSystem;
use Drupal\Core\File\FileSystemInterface;
use Drupal\file\Entity\File;
use Drupal\node\Entity\Node;
use Drupal\rest\Annotation\RestResource;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Request;

use Drupal\media\Entity\Media;

/**
 * Provides Resource for the Upload Access Code functionality
 *
 * @RestResource(
 *   id = "upload_access_code",
 *   label = @Translation("UPLOAD_ACCESS_CODE"),
 *   uri_paths = {
 *     "canonical" = "api/json/access_codes/upload",
 *     "create" = "api/json/access_codes/upload"
 *   }
 * )
 */
class UploadAccessCode extends ResourceBase
{
  const HTTP_OK = 200;

  /**
   * Responds to entity POST requests.
   * @return \Drupal\rest\ResourceResponse
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function post(Request $request)
  {
    $content = json_decode($request->getContent(), true);
    if (pathinfo($content['filename'])['extension'] !== 'txt') {
      return new ResourceResponse([
        'success' => true,
        'status_code' => self::HTTP_OK,
        'message' => $this->t("INVALID_FILE_FORMAT"),
        'data' => [],
      ]);
    }

    $time = strtotime('now');
    $year = date('Y', $time);
    $month = date('m', $time);
    $path = 'public://' . $year . '-' . $month . '/' . $content['filename'];
    if (file_exists($path)) {
      $c = file_get_contents($path);
    } else if (file_exists('public://' . $content['filename'])) {
      $c = file_get_contents('public://' . $content['filename']);
    } else {
      return new ResourceResponse([
        'success' => true,
        'status_code' => self::HTTP_OK,
        'message' => $this->t("FILE_NOT_FOUND"),
        'data' => [],
      ]);
    }

    $d = preg_split('/\s+/', $c, -1, PREG_SPLIT_NO_EMPTY);
    $lang_code = strtoupper(\Drupal::languageManager()->getCurrentLanguage()->getId());

    $min_code_length = 3;
    $max_code_length = 32;

    $exist = [];

    foreach ($d as $e) {
      $code = $lang_code . "-" . $e;

      if (strlen($code) < ($min_code_length + strlen($lang_code) + 1) || (strlen($code) > ($max_code_length))) {
        continue;
      }

      preg_match('/^[a-zA-Z0-9\-]*$/', $code, $matches);
      if (empty($matches)) {
        continue;
      }

      $nids = \Drupal::entityTypeManager()
        ->getListBuilder('node')
        ->getStorage()
        ->loadByProperties([
          'type' => 'access_code',
          'field_access_code_code' => $code,
        ]);

      if (!$nids) {
        $node = Node::create([
          'type' => 'access_code',
          'title' => $e,
          'field_access_code_code' => $e,
        ]);
        $node->save();
      } else {
        $exist[] = $code;
      }
    }

    if (file_exists($path)) {
      unlink($path);
    } else {
      $path = 'public://' . $content['filename'];
      unlink($path);
    }

    return new ResourceResponse([
      'success' => true,
      'status_code' => self::HTTP_OK,
      'message' => $this->t("ACCESS_CODES_UPLOADED_SUCCESSFULLY"),
      'data' => [
        'exist' => $exist,
      ],
    ]);
  }
}
