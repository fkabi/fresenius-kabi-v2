<?php

namespace Drupal\forgot_password_rest_api\Plugin\rest\resource;

use Drupal\Core\Annotation\Translation;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\kabi\Validation\ApiValidation;
use Drupal\node\Entity\Node;
use Drupal\rest\Annotation\RestResource;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Provides Resource for the Forgot Password functionality
 *
 * @RestResource(
 *   id = "change_password_post",
 *   label = @Translation("CHANGE_PASSWORD_POST"),
 *   uri_paths = {
 *     "canonical" = "change-password",
 *     "create" = "change-password"
 *   }
 * )
 */
class ChangePasswordPost extends ResourceBase
{
  const HTTP_OK = 200;
  const HTTP_UNPROC = 422;

  /**
   * Responds to entity GET requests.
   * @return \Drupal\rest\ResourceResponse
   */
  public function post(Request $request)
  {
    $content = json_decode($request->getContent(), true);

    if ($content === null) {
      $response = [
        'success' => false,
        'status_code' => self::HTTP_UNPROC,
        'message' => $this->t("INVALID_DATA"),
        'data' => [
          'error_title' => $this->t('EMPTY_REQUEST'),
        ],
      ];

      return new ResourceResponse($response);
    }

    $errors = ApiValidation::validate($content, self::getRules(), 'user');

    if ($errors) {
      $response = [
        'success' => false,
        'status_code' => self::HTTP_UNPROC,
        'message' => $this->t("INVALID_DATA"),
        'data' => $errors,
      ];

      return new ResourceResponse($response);
    }

    if ($content['field_password'][0]['value'] !== $content['field_confirm_password'][0]['value']) {
      $response = [
        'success' => false,
        'status_code' => self::HTTP_UNPROC,
        'message' => $this->t("PASSWORDS_DO_NOT_MATCH"),
        'data' => $errors,
      ];

      return new ResourceResponse($response);
    }

    $email = $content['field_email'][0]['value'];
    $name = $email;

    if (!user_load_by_mail($email) && !user_load_by_name($name)) {
      $response = [
        'success' => false,
        'status_code' => self::HTTP_UNPROC,
        'message' => $this->t("INVALID_EMAIL"),
        'data' => [
          'content' => $this->t("USER_WITH_THE_PROVIDED_EMAIL_DOES_NOT_EXIST"),
        ],
      ];

      return new ResourceResponse($response);
    }

    try {
      $user = user_load_by_mail($email);
      if (!$user) {
        $user = user_load_by_name($name);
      }
      $user_id = $user->id();
    } catch (\Exception $e) {
      $payload = [
        'success' => false,
        'status_code' => self::HTTP_UNPROC,
        'message' => $this->t("USER_DOES_NOT_EXIST"),
        'data' => [
          'content' => $this->t("USER_DOES_NOT_EXIST"),
        ],
      ];

      return new ResourceResponse($payload);
    }

    try {
      $token = $content['field_token'][0]['value'];

      $result = \Drupal::entityQuery('node')
        ->condition('type', 'password_change_request')
        ->condition('field_password_change_token', $token)
        ->condition('field_password_change_user_ref', $user_id)
        ->condition('field_password_change_used', false)
        ->execute();

      if (empty($result)) {
        $payload = [
          'success' => false,
          'status_code' => self::HTTP_UNPROC,
          'message' => $this->t("Password reset link is expired"),
          'data' => [
            'content' => $this->t("Password reset link is expired"),
          ],
        ];

        return new ResourceResponse($payload);
      }

      $nids = \Drupal::entityTypeManager()->getStorage('node')->loadMultiple($result);
    } catch (\Exception $e) {
      $payload = [
        'success' => false,
        'status_code' => self::HTTP_UNPROC,
        'message' => $this->t("COULD_NOT_GET_PASSWORD_CHANGE_REQUEST_DATA_FROM_THE_SYSTEM"),
        'data' => [
          'content' => $this->t("COULD_NOT_GET_PASSWORD_CHANGE_REQUEST_DATA_FROM_THE_SYSTEM"),
        ],
      ];

      return new ResourceResponse($payload);
    }

    try {
      $password_change_request = array_pop(array_reverse($nids));

      $password_change_request->set('field_password_change_used', true);

      $password_change_request->save();
    } catch (\Exception $e) {
      $payload = [
        'success' => false,
        'status_code' => self::HTTP_UNPROC,
        'message' => $this->t("COULD_NOT_SET_PASSWORD_CHANGE_REQUEST_TO_USED"),
        'data' => [
          'content' => $this->t("COULD_NOT_SET_PASSWORD_CHANGE_REQUEST_TO_USED"),
        ],
      ];

      return new ResourceResponse($payload);
    }

    try {
      $password = $content['field_password'][0]['value'];

      $user->setPassword($password);

      $timestamp = \Drupal::time()->getRequestTime();
      $date = DrupalDateTime::createFromTimestamp($timestamp, 'UTC');
      $user->set('field_user_password_set', $date->format("Y-m-d\TH:i:s"));

      $user->save();
    } catch (\Exception $e) {
      $payload = [
        'success' => false,
        'status_code' => self::HTTP_UNPROC,
        'message' => $this->t("COULD_NOT_SET_PASSWORD"),
        'data' => [
          'content' => $this->t("COULD_NOT_SET_PASSWORD"),
        ],
      ];

      return new ResourceResponse($payload);
    }

    $payload = [
      'success' => true,
      'status_code' => self::HTTP_OK,
      'message' => $this->t("PASSWORD_CHANGED"),
      'data' => [
        'content' => $this->t("PASSWORD_CHANGED"),
      ],
    ];

    return new ResourceResponse($payload);
  }

  public static function getRules(): array
  {
    return [
      "field_email" => "required|max:60|pattern:/^([a-z0-9_\-\.]+)@([a-z0-9_\-\.]+)\.([a-z]{2,5})$/",
      "field_token" => "required|min:62|max:63",
      "field_password" => "required|min:6|max:60|pattern:/^.*(?=\S{10,})(?=\S*[a-z])(?=\S*[A-Z])(?=\S*[\d])[^\"'\s]*$/",
      "field_confirm_password" => "required|min:10|max:60|pattern:/^.*(?=\S{10,})(?=\S*[a-z])(?=\S*[A-Z])(?=\S*[\d])[^\"'\s]*$/",
    ];
  }
}
