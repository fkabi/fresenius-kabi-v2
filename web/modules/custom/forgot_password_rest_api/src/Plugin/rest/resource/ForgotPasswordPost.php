<?php

namespace Drupal\forgot_password_rest_api\Plugin\rest\resource;

use Drupal\Core\Annotation\Translation;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Site\Settings;
use Drupal\kabi\Validation\ApiValidation;
use Drupal\node\Entity\Node;
use Drupal\rest\Annotation\RestResource;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Provides Resource for the Forgot Password functionality
 *
 * @RestResource(
 *   id = "forgot_password_post",
 *   label = @Translation("FORGOT_PASSWORD_POST"),
 *   uri_paths = {
 *     "canonical" = "forgot-my-password",
 *     "create" = "forgot-my-password"
 *   }
 * )
 */
class ForgotPasswordPost extends ResourceBase
{
  const HTTP_OK = 200;
  const HTTP_UNPROC = 422;

  /**
   * Responds to entity GET requests.
   * @return \Drupal\rest\ResourceResponse
   */
  public function post(Request $request)
  {
    $content = json_decode($request->getContent(), true);

    if (
      $content === null ||
      !isset($content['field_email'][0]['value'])
    ) {
      $response = [
        'success' => false,
        'status_code' => self::HTTP_UNPROC,
        'message' => $this->t("INVALID_DATA"),
        'data' => [
          'error_title' => $this->t('EMPTY_REQUEST'),
        ],
      ];

      return new ResourceResponse($response);
    }

    $errors = ApiValidation::validate($content, self::getRules(), 'user');

    if ($errors) {
      $response = [
        'success' => false,
        'status_code' => self::HTTP_UNPROC,
        'message' => $this->t("INVALID_DATA"),
        'data' => $errors,
      ];

      return new ResourceResponse($response);
    }

    $email = $name = $content['field_email'][0]['value'];
    $user = user_load_by_mail($email);

    if (!$user) {
      $response = [
        'success' => false,
        'status_code' => self::HTTP_UNPROC,
        'message' => $this->t("INVALID_EMAIL"),
        'data' => [
          'content' => $this->t("USER_WITH_THE_PROVIDED_EMAIL_DOES_NOT_EXIST"),
        ],
      ];

      return new ResourceResponse($response);
    }

    try {
      $length = 63;
      $keyspace = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
      $pieces = [];
      $max = mb_strlen($keyspace, '8bit') - 1;

      for ($i = 0; $i < $length; ++$i) {
        $pieces[] = $keyspace[random_int(0, $max)];
      }

      $token = implode('', $pieces);

      $password_change = [
        'type' => 'password_change_request',
      ];

      $user = user_load_by_mail($email);
      if (!$user) {
        $user = user_load_by_name($name);
      }
      $user_id = $user->id();

      $password_change["title"] = $token;
      $password_change["field_password_change_token"] = $token;
      $password_change["field_password_change_user_ref"] = $user_id;

      $node = Node::create($password_change);
      $node->save();
    } catch (\Exception $e) {
      $payload = [
        'success' => false,
        'status_code' => self::HTTP_UNPROC,
        'message' => $this->t("CANNOT_GENERATE_TOKEN_FOR_PASSWORD_CHANGE"),
        'data' => [
          'content' => $this->t("CANNOT_GENERATE_TOKEN_FOR_PASSWORD_CHANGE"),
        ],
      ];

      return new ResourceResponse($payload);
    }

    try {
      $frontend_url = Settings::get('frontend_url');
      $mail_manager = \Drupal::service('plugin.manager.mail');
      $module = 'kabi';
      $key = 'forgot_password';
      $to = $email;
      $params['forgot_password_link'] = $frontend_url . "/change-password?_format=json&token=" . $token;
      $params['subject'] = "Password change request";
      $langcode = $user->getPreferredLangcode();
      $send = true;

      $result = $mail_manager->mail($module, $key, $to, $langcode, $params, NULL, $send);
    } catch (\Exception $e) {
      $payload = [
        'success' => false,
        'status_code' => self::HTTP_UNPROC,
        'message' => $this->t("COULD_NOT_SEND_PASSWORD_CHANGE_EMAIL"),
        'data' => [
          'content' => $this->t("COULD_NOT_SEND_PASSWORD_CHANGE_EMAIL"),
        ],
      ];

      return new ResourceResponse($payload);
    }

    $payload = [
      'success' => true,
      'status_code' => self::HTTP_OK,
      'message' => $this->t("EMAIL_SENT"),
      'data' => [
        'content' => $this->t("EMAIL_SENT"),
      ],
    ];

    return new ResourceResponse($payload);
  }

  public static function getRules(): array
  {
    return [
      "field_email" => "required|max:60|pattern:/^([a-z0-9_\-\.]+)@([a-z0-9_\-\.]+)\.([a-z]{2,5})$/",
    ];
  }
}
