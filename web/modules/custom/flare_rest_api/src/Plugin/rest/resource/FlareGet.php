<?php

namespace Drupal\flare_rest_api\Plugin\rest\resource;

use Drupal\Core\Annotation\Translation;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\rest\Annotation\RestResource;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Provides Resource for the Flare Up functionality
 *
 * @RestResource(
 *   id = "flare_get",
 *   label = @Translation("FLARE_GET"),
 *   uri_paths = {
 *     "canonical" = "/flare-rest-api/get-flares"
 *   }
 * )
 */
class FlareGet extends ResourceBase
{
  const HTTP_OK = 200;
  const HTTP_UNPROC = 422;

  /**
   * Responds to entity GET requests.
   * @param Request $request
   * @return \Drupal\rest\ResourceResponse
   */
  public function get(Request $request)
  {
    $interval = $request->query->get('interval');

    try {
      $user = \Drupal::currentUser();
      $userLangId = $user->getPreferredLangcode();

      $result = \Drupal::entityQuery('node')
        ->condition('type', 'flare')
        ->condition('field_flare_user_ref', $user->id());

      if ($interval !== null) {
        $start = new \DateTime('now');
        $end = new \DateTime('now');
        $end->modify('+1 day');
        $end->setTime(0, 0, 0);
        $end = DrupalDateTime::createFromDateTime($end);

        switch ($interval) {
          case 'day':
            $start->setTime(0, 0, 0);
            $start = DrupalDateTime::createFromDateTime($start);
            break;
          case 'week':
            $start->modify('-6 days');
            $start->setTime(0, 0, 0);
            $start = DrupalDateTime::createFromDateTime($start);

            break;
          case 'month':
          default:
            $start->modify('-30 days');
            $start->setTime(0, 0, 0);
            $start = DrupalDateTime::createFromDateTime($start);

            break;
        }

        $result->condition('field_flare_record_date', $start, '>=');
        $result->condition('field_flare_record_date', $end, '<=');
      }

      $result = $result->execute();
      $nids = \Drupal::entityTypeManager()->getStorage('node')->loadMultiple($result);

      $user_flares = [];

      foreach ($nids as $id => $node) {
        $user_flares[$id] = [
          'id' => $id,
          'note' => $node->field_flare_note->value,
          'recordDate' => $node->field_flare_record_date->value,
        ];
      }

      if (empty($user_flares)) {
        $payload = [
          'success' => false,
          'status_code' => self::HTTP_OK,
          'message' => $this->t("USER_DOES_NOT_HAVE_ANY_FLARE"),
          'data' => [
            'content' => [],
          ],
        ];
      } else {
        $payload = [
          'success' => true,
          'status_code' => self::HTTP_OK,
          'message' => $this->t("SUCCESSFUL"),
          'data' => [
            'content' => $user_flares,
          ],
        ];
      }

      $response = new ResourceResponse($payload);
      $response->getCacheableMetadata()->addCacheContexts(['user']);
      $response->addCacheableDependency(CacheableMetadata::createFromRenderArray([
        '#cache' => [
          'context' => ['user'],
          'tags' => ['user:3', 'languages:' . $userLangId],
          'max-age' => 0,
        ],
      ]));

      return $response;
    } catch (\Exception $e) {
      $response = [
        'success' => false,
        'status_code' => self::HTTP_UNPROC,
        'message' => $e->getMessage(),
        'data' => [],
      ];

      return new ResourceResponse($response);
    }
  }
}
