<?php

namespace Drupal\flare_rest_api\Plugin\rest\resource;

use Drupal\Core\Annotation\Translation;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\kabi\Validation\ApiValidation;
use Drupal\node\Entity\Node;
use Drupal\rest\Annotation\RestResource;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Provides Resource for the Symptom functionality
 *
 * @RestResource(
 *   id = "flare_create",
 *   label = @Translation("FLARE_CREATE"),
 *   uri_paths = {
 *     "canonical" = "/flare-rest-api/create-flare",
 *     "create" = "/flare-rest-api/create-flare"
 *   }
 * )
 */
class FlareCreate extends ResourceBase
{
  const HTTP_OK = 200;
  const HTTP_UNPROC = 422;

  /**
   * Responds to entity POST requests.
   * @param Request $request
   * @return \Drupal\rest\ResourceResponse
   */
  public function post(Request $request)
  {
    $content = json_decode($request->getContent(), true);

    if ($content === null) {
      $response = [
        'success' => false,
        'status_code' => self::HTTP_UNPROC,
        'message' => $this->t("INVALID_DATA"),
        'data' => [
          'error_title' => $this->t('EMPTY_REQUEST'),
        ],
      ];

      return new ResourceResponse($response);
    }

    $errors = ApiValidation::validate($content, self::getRules(), 'flare');

    if ($errors) {
      $response = [
        'success' => false,
        'status_code' => self::HTTP_UNPROC,
        'message' => $this->t("INVALID_DATA"),
        'data' => $errors,
      ];

      return new ResourceResponse($response);
    }

    try {
      $user = \Drupal::currentUser();
      $user_id = $user->id();
    } catch (\Exception $e) {
      $response = [
        'success' => false,
        'status_code' => self::HTTP_UNPROC,
        'message' => $this->t("USER_NOT_FOUND"),
        'data' => [],
      ];

      return new ResourceResponse($response);
    }

    $account = \Drupal\user\Entity\User::load($user_id);
    if ($account->field_used_access_code[0]->target_id === null) {
      $response = [
        'success' => false,
        'status_code' => self::HTTP_OK,
        'message' => $this->t("USER_CANNOT_CREATE_SYMPTOM"),
        'data' => [
          'has_order' => false,
          'content' => $this->t("USER_DOES_NOT_HAVE_A_VALID_ACCESS_CODE"),
          'can_order' => false,
        ],
      ];

      return new ResourceResponse($response);
    }

    try {
      $start = new \DateTime('now');
      $start->setTime(0, 0, 0);
      $start = DrupalDateTime::createFromDateTime($start);

      $end = new \DateTime('now');
      $end->modify('+1 day');
      $end->setTime(0, 0, 0);
      $end = DrupalDateTime::createFromDateTime($end);

      $result = \Drupal::entityQuery('node')
        ->condition('type', 'flare')
        ->condition('field_flare_user_ref', $user->id())
        ->condition('field_flare_record_date', $start, '>=')
        ->condition('field_flare_record_date', $end, '<=');

      $result = $result->execute();
      $nids = \Drupal::entityTypeManager()->getStorage('node')->loadMultiple($result);

      if (!empty($nids)) {
        $response = [
          'success' => false,
          'status_code' => self::HTTP_UNPROC,
          'message' => $this->t("FLARE_CANNOT_BE_CREATED_DUE_DAILY_LIMIT_REACHED"),
          'data' => [],
        ];

        return new ResourceResponse($response);
      }
    } catch (\Exception $e) {
      $response = [
        'success' => false,
        'status_code' => self::HTTP_UNPROC,
        'message' => $this->t("FLARE_CANNOT_BE_CREATED"),
        'data' => [],
      ];

      return new ResourceResponse($response);
    }

    if (
      $content['field_flare_note'][0]['value'] == '|' ||
      empty($content['field_flare_note'][0]['value'])
    ) {
      $response = [
        'success' => false,
        'status_code' => self::HTTP_UNPROC,
        'message' => $this->t("EMPTY_REQUEST"),
        'data' => [],
      ];
      return new ResourceResponse($response);     
    }

    try {
      $flare = [
        'type' => 'flare',
      ];

      $flare_title = $content['field_flare_note'][0]['value'];

      $flare["title"] = substr($flare_title, 0, 64);;
      $flare["field_flare_note"] = $flare_title;
      $flare["field_flare_user_ref"] = $user_id;

      $node = Node::create($flare);
      $node->save();
    } catch (\Exception $e) {
      $response = [
        'success' => false,
        'status_code' => self::HTTP_UNPROC,
        'message' => $this->t("FLARE_CANNOT_BE_CREATED"),
        'data' => [],
      ];

      return new ResourceResponse($response);
    }

    return new ResourceResponse([
      'success' => true,
      'status_code' => self::HTTP_OK,
      'message' => $this->t("FLARE_CREATED"),
      'data' => [
        'content' => [
          $node->id() => [
            'id' => $node->id(),
            'note' => $node->field_flare_note->value,
            'recordDate' => $node->field_flare_record_date->value,
          ],
        ],
      ],
    ]);
  }

  public static function getRules(): array
  {
    return [
      "field_flare_note" => "required|min:1|max:500",
    ];
  }
}
