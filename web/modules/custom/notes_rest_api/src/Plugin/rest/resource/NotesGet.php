<?php

namespace Drupal\notes_rest_api\Plugin\rest\resource;

use Drupal\Core\Annotation\Translation;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\rest\Annotation\RestResource;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Provides Resource for the Notes functionality
 *
 * @RestResource(
 *   id = "notes_get",
 *   label = @Translation("NOTES_GET"),
 *   uri_paths = {
 *     "canonical" = "/notes-rest-api/get-notes"
 *   }
 * )
 */
class NotesGet extends ResourceBase
{
  const HTTP_OK = 200;
  const HTTP_UNPROC = 422;

  /**
   * Responds to entity GET requests.
   * @return \Drupal\rest\ResourceResponse
   */
  public function get(Request $request)
  {
    $interval = $request->query->get('interval');
    $type = $request->query->get('type');

    if ($type === null || ($type !== 'notes' && $type !== 'appointments')) {
      $response = [
        'success' => false,
        'status_code' => self::HTTP_UNPROC,
        'message' => $this->t('INVALID_TYPE'),
        'data' => [],
      ];

      return new ResourceResponse($response);
    }

    try {
      $user = \Drupal::currentUser();
      $userLangId = $user->getPreferredLangcode();

      $result = \Drupal::entityQuery('node')
        ->condition('type', 'notes')
        ->condition('field_notes_user_ref', $user->id());

      if ($type === 'notes') {
        //if the type is notes, no interval is calculated
      } else if ($interval !== null) {
        $useInterval = true;
        $start = new \DateTime('now');
        $end = new \DateTime('now');

        switch ($interval) {
          case 'past-day':
            $start->modify('-1 day');
            $start->setTime(0, 0, 0);
            $start = DrupalDateTime::createFromDateTime($start);
            $end->setTime(0, 0, 0);
            $end = DrupalDateTime::createFromDateTime($end);

            break;
          case 'past-7-days':
            $start->modify('-6 days');
            $start->setTime(0, 0, 0);
            $start = DrupalDateTime::createFromDateTime($start);
            $end->modify('+1 day');
            $end->setTime(0, 0, 0);
            $end = DrupalDateTime::createFromDateTime($end);

            break;
          case 'past-30-days':
            $start->modify('-30 days');
            $start->setTime(0, 0, 0);
            $start = DrupalDateTime::createFromDateTime($start);
            $end->modify('+1 day');
            $end->setTime(0, 0, 0);
            $end = DrupalDateTime::createFromDateTime($end);

            break;
          case 'today':
            $start->setTime(0, 0, 0);
            $start = DrupalDateTime::createFromDateTime($start);
            $end->modify('+1 day');
            $end->setTime(0, 0, 0);
            $end = DrupalDateTime::createFromDateTime($end);

            break;
          case 'next-7-days':
            $start->setTime(0, 0, 0);
            $start = DrupalDateTime::createFromDateTime($start);
            $end->modify('+7 day');
            $end->setTime(0, 0, 0);
            $end = DrupalDateTime::createFromDateTime($end);

            break;
          case 'next-30-days':
            $start->setTime(0, 0, 0);
            $start = DrupalDateTime::createFromDateTime($start);
            $end->modify('+30 day');
            $end->setTime(0, 0, 0);
            $end = DrupalDateTime::createFromDateTime($end);

            break;
          case 'all':
          default:
            $useInterval = false;

            break;
        }

        if ($useInterval) {
          $result->condition('field_notes_due_date', $start, '>=');
          $result->condition('field_notes_due_date', $end, '<=');
        }
      }

      if ($type === 'appointments') {
        $result->sort('field_notes_due_date');
        $result->sort('changed', 'DESC');
      }

      $result = $result->execute();
      $nids = \Drupal::entityTypeManager()->getStorage('node')->loadMultiple($result);

      $user_notes = [];
      $counter = 0;

      foreach ($nids as $id => $node) {
        if ($type === 'notes') {
          if ($node->field_notes_due_date->value !== null) {
            continue;
          }
        } else {
          if ($node->field_notes_due_date->value === null) {
            continue;
          }

          if (strtotime($node->field_notes_due_date->value) < strtotime('2000-01-01 00:00:00')) {
            continue;
          }
        }

        $nids = \Drupal::entityTypeManager()
          ->getListBuilder('node')
          ->getStorage()
          ->loadByProperties([
            'type' => "note_item",
            'field_note_item_notes_ref' => $id,
          ]);

        $note_items = [];

        foreach ($nids as $note_item_id => $note_item) {
          $note_items[] = [
            'id' => $note_item->id(),
            'title' => $note_item->getTitle(),
            'completed' => ($note_item->field_note_item_completed->value === '0' ? false : true),
          ];
        }

        $created_date = $node->getCreatedTime();
        $created_date = \Drupal::service('date.formatter')->format($created_date, 'custom', 'Y-m-d H:i:s');

        $user_notes[$counter] = [
          'id' => $id,
          'title' => $node->title->value,
          'dueDate' => (strtotime($node->field_notes_due_date->value) > strtotime('2000-01-01 00:00:00') ? date('Y-m-d H:i:s', strtotime($node->field_notes_due_date->value)) : null),
          'body' => $node->body->value,
          'updatedAt' => date('Y-m-d H:i:s', $node->changed->value),
          'createdAt' => $created_date,
        ];

        if (!empty($note_items)) {
          $user_notes[$counter]['noteItems'] = $note_items;
        }
        $counter++;
      }

      if (empty($user_notes)) {
        $payload = [
          'success' => false,
          'status_code' => self::HTTP_OK,
          'message' => $this->t("USER_DOES_NOT_HAVE_ANY_NOTE"),
          'data' => [
            'content' => [],
          ],
        ];
      } else {
        if ($type === 'notes') {
          rsort($user_notes);
        }

        $payload = [
          'success' => true,
          'status_code' => self::HTTP_OK,
          'message' => $this->t("SUCCESSFUL"),
          'data' => [
            'content' => $user_notes,
          ],
        ];
      }

      $response = new ResourceResponse($payload);
      $response->getCacheableMetadata()->addCacheContexts(['user']);
      $response->addCacheableDependency(CacheableMetadata::createFromRenderArray([
        '#cache' => [
          'context' => ['user'],
          'tags' => ['user:3', 'languages:' . $userLangId],
          'max-age' => 0,
        ],
      ]));

      return $response;
    } catch (\Exception $e) {
      $response = [
        'success' => false,
        'status_code' => self::HTTP_UNPROC,
        'message' => $e->getMessage(),
        'data' => [],
      ];

      return new ResourceResponse($response);
    }
  }
}
