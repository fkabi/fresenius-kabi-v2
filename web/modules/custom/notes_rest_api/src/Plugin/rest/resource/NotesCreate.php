<?php

namespace Drupal\notes_rest_api\Plugin\rest\resource;

use Drupal\Core\Annotation\Translation;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\kabi\Validation\ApiValidation;
use Drupal\node\Entity\Node;
use Drupal\rest\Annotation\RestResource;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Provides Resource for the Notes functionality
 *
 * @RestResource(
 *   id = "notes_create",
 *   label = @Translation("NOTE_CREATE"),
 *   uri_paths = {
 *     "canonical" = "/notes-rest-api/create-note",
 *     "create" = "/notes-rest-api/create-note"
 *   }
 * )
 */
class NotesCreate extends ResourceBase
{
  const HTTP_OK = 200;
  const HTTP_UNPROC = 422;

  /**
   * Responds to entity POST requests.
   * @return \Drupal\rest\ResourceResponse
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function post(Request $request)
  {
    $content = json_decode($request->getContent(), true);

    try {
      $user = \Drupal::currentUser();
      $user_id = $user->id();
    } catch (\Exception $e) {
      $response = [
        'success' => false,
        'status_code' => self::HTTP_UNPROC,
        'message' => $this->t("USER_NOT_FOUND"),
        'data' => [],
      ];

      return new ResourceResponse($response);
    }

    $account = \Drupal\user\Entity\User::load($user_id);
    if ($account->field_used_access_code[0]->target_id === null) {
      $response = [
        'success' => false,
        'status_code' => self::HTTP_OK,
        'message' => $this->t("USER_CANNOT_CREATE_NOTE"),
        'data' => [
          'has_order' => false,
          'content' => $this->t("USER_DOES_NOT_HAVE_A_VALID_ACCESS_CODE"),
          'can_order' => false,
        ],
      ];

      return new ResourceResponse($response);
    }

    try {
      $note = [
        'type' => 'notes',
      ];
      $note_array = explode("\n", $content['field_body'][0]['value']);
      $note_name = reset($note_array);

      $time = strtotime($content['field_notes_due_date'][0]['value']);

      if (isset($content['field_notes_due_date'][0]['value']) && !$time) {
        $response = [
          'success' => false,
          'status_code' => self::HTTP_UNPROC,
          'message' => $this->t("INVALID_DATE"),
          'data' => [],
        ];

        return new ResourceResponse($response);
      }

      $body = htmlspecialchars($content['field_body'][0]['value'], ENT_QUOTES, 'UTF-8');
      $date = date('Y-m-d\TH:i:s', $time);

      $note["title"] = $content['field_title'][0]['value'];
      $note["body"] = $body;
      $note["field_notes_user_ref"] = $user_id;

      if ($time) {
        $note["title"] = $note_name;
        $note["field_notes_due_date"] = $date;
      }

      $node = Node::create($note);
      $node->save();
    } catch (\Exception $e) {
      $response = [
        'success' => false,
        'status_code' => self::HTTP_UNPROC,
        'message' => $this->t("NOTE_CANNOT_BE_CREATED"),
        'data' => [],
      ];

      return new ResourceResponse($response);
    }

    $created_date = $node->getCreatedTime();
    $created_date = \Drupal::service('date.formatter')->format($created_date, 'custom', 'Y-m-d H:i:s');

    return new ResourceResponse([
      'success' => true,
      'status_code' => self::HTTP_OK,
      'message' => $this->t("NOTE_CREATED"),
      'data' => [
        'content' => [
          $node->id() => [
            'id' => $node->id(),
            'title' => $note_name,
            'dueDate' => ($time ? $date : null),
            'body' => $body,
            'createdAt' => $created_date,
          ],
        ],
      ],
    ]);
  }
}
