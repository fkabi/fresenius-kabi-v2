<?php

namespace Drupal\notes_rest_api\Plugin\rest\resource;

use Drupal\Core\Annotation\Translation;
use Drupal\node\Entity\Node;
use Drupal\rest\Annotation\RestResource;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Provides Resource for the Notes functionality
 *
 * @RestResource(
 *   id = "notes_delete",
 *   label = @Translation("NOTE_DELETE"),
 *   uri_paths = {
 *     "canonical" = "/notes-rest-api/{note_id}/delete-note",
 *     "create" = "/notes-rest-api/{note_id}/delete-note"
 *   }
 * )
 */
class NotesDelete extends ResourceBase
{
  const HTTP_OK = 200;
  const HTTP_UNPROC = 422;

  /**
   * Responds to entity POST requests.
   * @return \Drupal\rest\ResourceResponse
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function post(Request $request, $note_id)
  {
    try {
      $user = \Drupal::currentUser();
      $user_id = $user->id();
    } catch (\Exception $e) {
      $response = [
        'success' => false,
        'status_code' => self::HTTP_UNPROC,
        'message' => $this->t("USER_NOT_FOUND"),
        'data' => [],
      ];

      return new ResourceResponse($response);
    }

    $account = \Drupal\user\Entity\User::load($user_id);
    if ($account->field_used_access_code[0]->target_id === null) {
      $response = [
        'success' => false,
        'status_code' => self::HTTP_OK,
        'message' => $this->t("USER_CANNOT_DELETE_NOTES"),
        'data' => [
          'content' => $this->t("USER_DOES_NOT_HAVE_A_VALID_ACCESS_CODE"),
        ],
      ];

      return new ResourceResponse($response);
    }

    try {
      $nids = \Drupal::entityTypeManager()
        ->getListBuilder('node')
        ->getStorage()
        ->loadByProperties([
          'type' => "notes",
          'nid' => $note_id,
          'field_notes_user_ref' => $user_id,
        ]);

      $keys = array_keys($nids);
      $node = null;

      if (isset($keys[0])) {
        $key = $keys[0];
        $node = $nids[$key];
      }

      if ($node === null) {
        $response = [
          'success' => false,
          'status_code' => self::HTTP_OK,
          'message' => $this->t("NOTE_DOES_NOT_EXIST_OR_DOES_NOT_BELONG_TO_USER"),
          'data' => [
            'content' => $this->t("NOTE_DOES_NOT_EXIST_OR_DOES_NOT_BELONG_TO_USER"),
          ],
        ];

        return new ResourceResponse($response);
      }

      $nids = \Drupal::entityTypeManager()
        ->getListBuilder('node')
        ->getStorage()
        ->loadByProperties([
          'type' => "note_item",
          'field_note_item_notes_ref' => $note_id,
        ]);
      foreach($nids as $note_item_id => $nid){
        $note_item = \Drupal\node\Entity\Node::load($note_item_id);

        $note_item->delete();
      }

      $node->delete();
    } catch (\Exception $e) {
      $response = [
        'success' => false,
        'status_code' => self::HTTP_UNPROC,
        'message' => $this->t("NOTE_CANNOT_BE_DELETED"),
        'data' => [],
      ];

      return new ResourceResponse($response);
    }

    return new ResourceResponse([
      'success' => true,
      'status_code' => self::HTTP_OK,
      'message' => $this->t("NOTE_DELETED"),
      'data' => [
        'content' => '',
      ],
    ]);
  }
}
