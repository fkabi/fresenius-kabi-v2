<?php

namespace Drupal\notes_rest_api\Plugin\rest\resource;

use Drupal\Core\Annotation\Translation;
use Drupal\kabi\Validation\ApiValidation;
use Drupal\node\Entity\Node;
use Drupal\rest\Annotation\RestResource;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Provides Resource for the Notes functionality
 *
 * @RestResource(
 *   id = "notes_modify",
 *   label = @Translation("NOTE_MODIFY"),
 *   uri_paths = {
 *     "canonical" = "/notes-rest-api/{note_id}/modify-note",
 *     "create" = "/notes-rest-api/{note_id}/modify-note"
 *   }
 * )
 */
class NotesModify extends ResourceBase
{
  const HTTP_OK = 200;
  const HTTP_UNPROC = 422;

  /**
   * Responds to entity POST requests.
   * @return \Drupal\rest\ResourceResponse
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function post(Request $request, $note_id)
  {
    $content = json_decode($request->getContent(), true);

    if ($content === null) {
      $response = [
        'success' => false,
        'status_code' => self::HTTP_UNPROC,
        'message' => $this->t("INVALID_DATA"),
        'data' => [
          'error_title' => $this->t('EMPTY_REQUEST'),
        ],
      ];

      return new ResourceResponse($response);
    }

    try {
      $user = \Drupal::currentUser();
      $user_id = $user->id();
    } catch (\Exception $e) {
      $response = [
        'success' => false,
        'status_code' => self::HTTP_UNPROC,
        'message' => $this->t("USER_NOT_FOUND"),
        'data' => [],
      ];

      return new ResourceResponse($response);
    }

    $account = \Drupal\user\Entity\User::load($user_id);
    if ($account->field_used_access_code[0]->target_id === null) {
      $response = [
        'success' => false,
        'status_code' => self::HTTP_OK,
        'message' => $this->t("USER_CANNOT_MODIFY_NOTES"),
        'data' => [
          'content' => $this->t("USER_DOES_NOT_HAVE_A_VALID_ACCESS_CODE"),
        ],
      ];

      return new ResourceResponse($response);
    }

    try {
      $nids = \Drupal::entityTypeManager()
        ->getListBuilder('node')
        ->getStorage()
        ->loadByProperties([
          'type' => "notes",
          'nid' => $note_id,
          'field_notes_user_ref' => $user_id,
        ]);
      $keys = array_keys($nids);
      $node = null;
      if (isset($keys[0])) {
        $key = $keys[0];
        $node = $nids[$key];
      }

      if ($node === null) {
        $response = [
          'success' => false,
          'status_code' => self::HTTP_OK,
          'message' => $this->t("NOTE_DOES_NOT_EXIST_OR_DOES_NOT_BELONG_TO_USER"),
          'data' => [
            'content' => $this->t("NOTE_DOES_NOT_EXIST_OR_DOES_NOT_BELONG_TO_USER"),
          ],
        ];

        return new ResourceResponse($response);
      }

      $shouldSave = false;

      if (isset($content['field_notes_due_date']) && isset($content['field_notes_due_date'][0]) && isset($content['field_notes_due_date'][0]['value']) && strtotime($content['field_notes_due_date'][0]['value']) !== false) {
        $time = strtotime($content['field_notes_due_date'][0]['value']);
        $node->set('field_notes_due_date', date('Y-m-d\TH:i:s', $time));
        $shouldSave = true;
      }

      if (isset($content['field_body']) && isset($content['field_body'][0]) && isset($content['field_body'][0]['value'])) {
        $node->set('body', $content['field_body'][0]['value']);

        $note_array = explode("\n", $content['field_body'][0]['value']);
        $note_name = reset($note_array);

        $node->set('title', $note_name);

        $shouldSave = true;
      }

      if ($shouldSave) {
        $node->save();
      }
    } catch (\Exception $e) {
      $response = [
        'success' => false,
        'status_code' => self::HTTP_UNPROC,
        'message' => $this->t("NOTE_CANNOT_BE_MODIFIED"),
        'data' => [],
      ];

      return new ResourceResponse($response);
    }

    return new ResourceResponse([
      'success' => true,
      'status_code' => self::HTTP_OK,
      'message' => $this->t("NOTE_MODIFIED"),
      'data' => [
        'content' => '',
      ],
    ]);
  }
}
