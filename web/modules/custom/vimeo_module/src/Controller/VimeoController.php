<?php

namespace Drupal\vimeo_module\Controller;

use Drupal\Core\Controller\ControllerBase;
use Vimeo\Exceptions\VimeoRequestException;
use Vimeo\Vimeo;

class VimeoController extends ControllerBase
{
  private $client_id;
  private $client_secret;
  private $access_token;
  private $auth_url;
  private $auth_token_url;
  private $token;

  public function __construct()
  {
    $this->client_id = "60493b982459475bb852b5c094a25ce6638498a5";
    $this->client_secret = "DPnXF4h7pUZQJqTKsNTYq8TNohM7fS3gBr8WVsm2r3HgJklVD446TAI4iToq6uf4eUQCKZHUXvnRDu6ARbSX7NqdewPUrRncL/gdAscLOzGr1zTk9V7LUv20CXwUK/Ur";
    $this->auth_url = "https://api.vimeo.com/oauth/authorize";
    $this->auth_token_url = "https://api.vimeo.com/oauth/access_token";
    $this->token = "e0e974db9849d77ab6972cc774c9559c";
  }

  public function getVideo($id)
  {
    $url = "https://api.vimeo.com/videos/{$id}";

    $curl = curl_init();

    curl_setopt_array($curl, [
      CURLOPT_URL => $url,
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => '',
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 0,
      CURLOPT_FOLLOWLOCATION => true,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => 'GET',
      CURLOPT_HTTPHEADER => [
        'Authorization: Bearer ' . $this->token,
      ],
    ]);

    $response = curl_exec($curl);

    if ($response) {
      curl_close($curl);

      return [
        'response' => $response,
      ];
    }

    $curl_info = curl_getinfo($curl);

    if ($curl_info['http_code'] < 1) {
      $curl_error = curl_error($curl);
      $curl_error = !empty($curl_error) ? ' [' . $curl_error . ']' : '';
      curl_close($curl);
      return [
        'error' => $curl_error,
        'has_error' => true,
      ];
    }

    curl_close($curl);

    return null;
  }

  public function isMyVideo($id)
  {
    $url = "https://api.vimeo.com/me/videos/{$id}";

    $curl = curl_init();

    curl_setopt_array($curl, [
      CURLOPT_URL => $url,
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => '',
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 0,
      CURLOPT_FOLLOWLOCATION => true,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => 'GET',
      CURLOPT_HTTPHEADER => [
        'Authorization: Bearer ' . $this->token,
      ],
    ]);

    $response = curl_exec($curl);

    $response = json_decode($response, true);

    if (isset($response['error'])) {
      return false;
    }

    if ($response) {
      curl_close($curl);

      return true;
    }

    $curl_info = curl_getinfo($curl);

    if ($curl_info['http_code'] < 1) {
      curl_close($curl);

      return false;
    }

    curl_close($curl);

    return null;
  }
}
